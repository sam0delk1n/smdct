
#ifndef __BOUNDBOX2D_HPP__
#define __BOUNDBOX2D_HPP__


////////////////////////////////////////////////////////////////////////////////
//
// BoundBox2D.hpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////


namespace smd {


/*******************************************************************************

 BoundBox2D

*******************************************************************************/
/*
class BoundBox2D : public BaseBox2D {
public:
	auto								operator=( const BoundBox2D& a ) ->BoundBox2D&;
	auto								operator=( BoundBox2D&& a ) noexcept ->BoundBox2D&;

	explicit							BoundBox2D( const Vector2f& size, const Vector2f& offset );
	explicit							BoundBox2D( const BoundBox2D& a );
	explicit							BoundBox2D( BoundBox2D&& a ) noexcept;
	virtual								~BoundBox2D( void );
};
*/

/*
================================================================================
BoundBox2D::operator=
================================================================================
*/
/*
inline auto BoundBox2D::operator=( const BoundBox2D& a ) ->BoundBox2D& {
	BaseBox2D::operator=( a );
	return *this;
}
*/

/*
================================================================================
BoundBox2D::operator=
================================================================================
*/
/*
inline auto BoundBox2D::operator=( BoundBox2D&& a ) noexcept ->BoundBox2D& {
	BaseBox2D::operator=( a );
	return *this;
}
*/

/*
================================================================================
BoundBox2D::BoundBox2D
================================================================================
*/
/*
inline BoundBox2D::BoundBox2D( const Vector2f& size, const Vector2f& offset ) :
BaseBox2D( size, offset ) {}
*/

/*
================================================================================
BoundBox2D::BoundBox2D
================================================================================
*/
/*
inline BoundBox2D::BoundBox2D( const BoundBox2D& a ) :
BaseBox2D( a ) {}
*/

/*
================================================================================
BoundBox2D::BoundBox2D
================================================================================
*/
/*
inline BoundBox2D::BoundBox2D( BoundBox2D&& a ) noexcept :
BaseBox2D( a ) {}
*/

/*
================================================================================
BoundBox2D::~BoundBox2D
================================================================================
*/
//inline BoundBox2D::~BoundBox2D( void ) {}

}/*namespace smd*/

#endif/*__BOUNDBOX2D_HPP___*/

