﻿////////////////////////////////////////////////////////////////////////////////
//
// Collision.cpp
//
// © Samodelkin, 2012
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include <cmath>

#include "../../inc/cpp/math/common.hpp"
#include "../../inc/cpp/math/vec2.hpp"
#include "../../inc/cpp/math/vec3.hpp"

#include "Collision.hpp"

/*
================================================================================
TrisWithLine
================================================================================
*/
/*
bool PYR_STDCALL smd::Math::TrisWithLine( Vector3f* l_pPoint, const Vector3f line[ 2 ], const Vector3f tris[ 3 ] ) {

	assert( l_pPoint );

	const float epsilon = 0.1f;

	// Вычисляем первый вектор плоскости.
	Vector3f planeVec0 = tris[ 1 ] - tris[ 0 ];

	// Вычисляем второй вектор плоскости.
	Vector3f planeVec1 = tris[ 2 ] - tris[ 0 ];

	// Вычисляем нормаль к плоскости.
	Vector3f planeNorm = planeVec0.Cross( planeVec1 );
	planeNorm.Normalize();

	// Вычисляем D.
	float valD = -( planeNorm.x * tris[ 0 ].x + planeNorm.y * tris[ 0 ].y + planeNorm.z * tris[ 0 ].z );

	// Считаем числитель для мю.
	float valMuUp = valD + planeNorm.x * line[ 0 ].x + planeNorm.y * line[ 0 ].y + planeNorm.z * line[ 0 ].z;

	// Считаем знаменятель для мю.
	float valMuDown = planeNorm.x * ( line[ 0 ].x - line[ 1 ].x ) + planeNorm.y * ( line[ 0 ].y - line[ 1 ].y ) + planeNorm.z * ( line[ 0 ].z - line[ 1 ].z );

	//FIXME Ввести эпсилон для поправки.
	if ( fabs( valMuDown ) < epsilon ) {
		
		// Пересечения нет.
		return false;
	}

	// Считаем мю.
	float valMu = valMuUp / valMuDown;

	if ( valMu > 1.0f || valMu < 0.0f ) {

		// Пересечения нет.
		return false;
	}

	// Находим точку пересечения отрезка с бесконечной плоскостью триса.
	Vector3f point = line[ 0 ] + ( line[ 1 ] - line[ 0 ] ) * valMu;

	// Считаем внутренние углы.
	Vector3f p0 = tris[ 0 ] - point;
	Vector3f p1 = tris[ 1 ] - point;
	Vector3f p2 = tris[ 2 ] - point;

	p0.Normalize();
	p1.Normalize();
	p2.Normalize();

	float a0 = static_cast< float >( ArcCosSeries64( static_cast< double >( p0 * p1 ), 64 ) );
	float a1 = static_cast< float >( ArcCosSeries64( static_cast< double >( p1 * p2 ), 64 ) );
	float a2 = static_cast< float >( ArcCosSeries64( static_cast< double >( p2 * p0 ), 64 ) );
	
	//float a0 = acos( p0 * p1 );
	//float a1 = acos( p1 * p2 );
	//float a2 = acos( p2 * p0 );
	

	// Проверяем условие.
	float temp = ( a0 + a1 + a2 ) - ( U_PI_F * 2.0f );

	if ( fabs( temp ) > epsilon ) {

		// Пересечения нет.
		return false;
	}

	// Пересечение есть.
	*l_pPoint = point;

	return true;
}
*/

/*
================================================================================
TrisWithRay
================================================================================
*/
/*
bool PYR_STDCALL smd::Math::TrisWithRay( smd::Vector3f* l_pPoint, const smd::Vector3f line[ 2 ], const smd::Vector3f tris[ 3 ] ) {

	assert( l_pPoint );

	//Vector3f E1 = tris[ 1 ] - tris[ 0 ];
	//Vector3f E2 = tris[ 2 ] - tris[ 0 ];
	//Vector3f T = line[ 0 ] - tris[ 0 ];
	//Vector3f D = line[ 1 ] - line[ 0 ];
	//Vector3f P = D.Cross( E2 );
	//Vector3f Q = T.Cross( E1 );

	//Vector3f secondPart( Q * E2, P * T, Q * D );
	//Vector3f result = secondPart * ( 1.0f / ( P * E1 ) );

	//TODO Нужно будет продолжить!

	return true;
}
*/

/*
================================================================================
TrisWithLine64
================================================================================
*/
/*
bool PYR_STDCALL smd::Math::TrisWithLine64( Vector3d* l_pPoint, const Vector3d line[ 2 ], const Vector3d tris[ 3 ] ) {

	assert( l_pPoint );

	const double epsilon = 0.01;

	// Вычисляем первый вектор плоскости.
	Vector3d planeVec0 = tris[ 1 ] - tris[ 0 ];

	// Вычисляем второй вектор плоскости.
	Vector3d planeVec1 = tris[ 2 ] - tris[ 0 ];

	// Вычисляем нормаль к плоскости.
	Vector3d planeNorm = planeVec0.Cross( planeVec1 );
	planeNorm.Normalize();

	// Вычисляем D.
	double valD = -( planeNorm.x * tris[ 0 ].x + planeNorm.y * tris[ 0 ].y + planeNorm.z * tris[ 0 ].z );

	// Считаем числитель для мю.
	double valMuUp = valD + planeNorm.x * line[ 0 ].x + planeNorm.y * line[ 0 ].y + planeNorm.z * line[ 0 ].z;

	// Считаем знаменятель для мю.
	double valMuDown = planeNorm.x * ( line[ 0 ].x - line[ 1 ].x ) + planeNorm.y * ( line[ 0 ].y - line[ 1 ].y ) + planeNorm.z * ( line[ 0 ].z - line[ 1 ].z );

	//FIXME Ввести эпсилон для поправки.
	if ( Dabs( valMuDown ) < epsilon ) {
		
		// Пересечения нет.
		return false;
	}

	// Считаем мю.
	double valMu = valMuUp / valMuDown;

	if ( valMu > 1.0 || valMu < 0.0 ) {

		// Пересечения нет.
		return false;
	}

	// Находим точку пересечения отрезка с бесконечной плоскостью триса.
	Vector3d point = line[ 0 ] + ( line[ 1 ] - line[ 0 ] ) * valMu;

	// Считаем внутренние углы.
	Vector3d p0 = tris[ 0 ] - point;
	Vector3d p1 = tris[ 1 ] - point;
	Vector3d p2 = tris[ 2 ] - point;

	p0.Normalize();
	p1.Normalize();
	p2.Normalize();

	//double a0 = ArcCosSeries64( p0 * p1, 64 );
	//double a1 = ArcCosSeries64( p1 * p2, 64 );
	//double a2 = ArcCosSeries64( p2 * p0, 64 );

	double a0 = acos( p0 * p1 );
	double a1 = acos( p1 * p2 );
	double a2 = acos( p2 * p0 );
	
	// Проверяем условие.
	double temp = ( a0 + a1 + a2 ) - ( U_PI_D * 2.0 );

	if ( Dabs( temp ) > epsilon ) {

		// Пересечения нет.
		return false;
	}

	// Пересечение есть.
	*l_pPoint = point;

	return true;
}
*/
