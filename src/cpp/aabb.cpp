
/*
================================================================================

aabb.cpp

© sam0delk1n, 2013-2015



================================================================================
*/

#include "../../inc/cpp/aabb.hpp"

using namespace smd;

/*
================================================================================
aabb_t::diagonalHalfLen
================================================================================
*/
auto aabb_t::diagonalHalfLen( const aabb_t& aabb ) noexcept ->float {
    return { sqrtf( aabb.radius.x * aabb.radius.x + aabb.radius.y * aabb.radius.y + aabb.radius.z * aabb.radius.z ) };
}

/*
================================================================================
aabb_t::testCollision
================================================================================
*/
auto aabb_t::testCollision( const aabb_t& a, const aabb_t& b ) noexcept ->bool {
    if ( fabsf( a.position.x - b.position.x ) > ( a.radius.x + b.radius.x ) ) return false;
    if ( fabsf( a.position.y - b.position.y ) > ( a.radius.y + b.radius.y ) ) return false;
    if ( fabsf( a.position.z - b.position.z ) > ( a.radius.z + b.radius.z ) ) return false;
    return true;
}

