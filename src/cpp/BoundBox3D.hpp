
#ifndef __BOUNDBOX3D_HPP__
#define __BOUNDBOX3D_HPP__


////////////////////////////////////////////////////////////////////////////////
//
// BoundBox3D.hpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////


namespace smd {


/*******************************************************************************

 BoundBox3D

*******************************************************************************/
/*
class BoundBox3D : public BaseBox3D {
public:
	auto								operator=( const BoundBox3D& a ) ->BoundBox3D&;
	auto								operator=( BoundBox3D&& a ) noexcept ->BoundBox3D&;

	explicit							BoundBox3D( const Vector3f& size, const Vector3f& offset );
	explicit							BoundBox3D( const BoundBox3D& a );
	explicit							BoundBox3D( BoundBox3D&& a ) noexcept;
	virtual								~BoundBox3D( void );
};
*/

/*
================================================================================
BoundBox3D::operator=
================================================================================
*/
/*
inline auto BoundBox3D::operator=( const BoundBox3D& a ) ->BoundBox3D& {
	BaseBox3D::operator=( a );
	return *this;
}
*/

/*
================================================================================
BoundBox3D::operator=
================================================================================
*/
/*
inline auto BoundBox3D::operator=( BoundBox3D&& a ) noexcept ->BoundBox3D& {
	BaseBox3D::operator=( a );
	return *this;
}
*/

/*
================================================================================
BoundBox3D::BoundBox3D
================================================================================
*/
/*
inline BoundBox3D::BoundBox3D( const Vector3f& size, const Vector3f& offset ) :
BaseBox3D( size, offset ) {}
*/

/*
================================================================================
BoundBox3D::BoundBox3D
================================================================================
*/
/*
inline BoundBox3D::BoundBox3D( const BoundBox3D& a ) :
BaseBox3D( a ) {}
*/

/*
================================================================================
BoundBox3D::BoundBox3D
================================================================================
*/
/*
inline BoundBox3D::BoundBox3D( BoundBox3D&& a ) noexcept :
BaseBox3D( a ) {}
*/

/*
================================================================================
BoundBox3D::~BoundBox3D
================================================================================
*/
//inline BoundBox3D::~BoundBox3D( void ) {}

}/*namespace smd*/

#endif/*__BOUNDBOX3D_HPP__*/

