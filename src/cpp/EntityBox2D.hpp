﻿
#ifndef __ENTITYBOX2D_HPP__
#define __ENTITYBOX2D_HPP__

/*
================================================================================

EntityBox2D.hpp

© sam0delk1n, 2013-2015



================================================================================
*/

namespace smd {

/*
================================================================================

EntityBox2D

================================================================================
*/
/*
class EntityBox2D : BoundBox2D {
    Vector2f*                           pPosition;

public:
    auto                                GetPosition( void ) const ->const Vector2f;

    //auto                              CheckLineCollision( const Vector3f& point, const Vector3f& vector ) const ->PYR_INT;
    //auto                              CheckBoundingBoxCollision( const BoundBox3D& a ) const ->PYR_INT;

    auto                                operator=( const EntityBox2D& a ) ->void;

    //explicit                          EntityBox2D( void );
    explicit                            EntityBox2D( Vector2f* l_pEntityCenter, const Vector2f& size, const Vector2f& offset = Vector2f( 0.0f, 0.0f ) );
    explicit                            EntityBox2D( Vector2f* l_pEntityCenter, float ocz, float sx, float sy, float ox = 0.0f, float oy = 0.0f );
                                        EntityBox2D( const EntityBox2D& a );
    virtual                             ~EntityBox2D( void );
};
*/

}/*namespace smd*/

#endif/*__ENTITYBOX2D_HPP__*/

