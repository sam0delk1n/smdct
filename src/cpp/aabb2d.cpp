/*
================================================================================

aabb2d.cpp

© sam0delk1n, 2013-2015



================================================================================
*/

#include "../../inc/cpp/aabb2d.hpp"
#include <cmath>

using namespace smd;

/*
================================================================================
aabb2d_t::diagonalHalfLen
================================================================================
*/
auto aabb2d_t::diagonalHalfLen( const aabb2d_t& aabb2d ) noexcept ->float {
    return { sqrtf( aabb2d.radius.x * aabb2d.radius.x + aabb2d.radius.y * aabb2d.radius.y ) };
}

/*
================================================================================
aabb2d_t::testCollision
================================================================================
*/
auto aabb2d_t::testCollision( const aabb2d_t& a, const aabb2d_t& b ) noexcept ->bool {
    if ( fabsf( a.position.x - b.position.x ) > ( a.radius.x + b.radius.x ) ) return false;
    if ( fabsf( a.position.y - b.position.y ) > ( a.radius.y + b.radius.y ) ) return false;
    return true;
}

