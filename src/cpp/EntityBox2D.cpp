﻿/*
================================================================================

EntityBox2D.cpp

© sam0delk1n, 2013-2015



================================================================================
*/

#include "BoundBox2D.hpp"
#include "EntityBox2D.hpp"

#include "../../inc/cpp/math/common.hpp"
#include "../../inc/cpp/math/vec2.hpp"

#include <cmath>

/*
================================================================================
EntityBox2D::GetPosition
================================================================================
*/
/*
auto smd::EntityBox2D::GetPosition( void ) const ->const Vector2f {
    if ( pPosition ) {
        return ( *pPosition ) + GetOffset();
    } else {
        return Vector2f( 0.0f, 0.0f );
    }
}
*/

/*
================================================================================
EntityBox2D::CheckLineCollision
================================================================================
*/
/*
auto smd::EntityBox3D::CheckLineCollision( const Vector3f& point, const Vector3f& vector ) const ->PYR_INT {
    //TODO
    return -1;
}
*/

/*
================================================================================
EntityBox2D::CheckBoundingBoxCollision
================================================================================
*/
/*
auto smd::EntityBox3D::CheckBoundingBoxCollision( const BoundBox3D& a ) const ->PYR_INT {
    if ( !pEntityCenter ) {
        return -1;
    }
    if ( !a.pEntityCenter ) {
        return -2;
    }

    float collisionDistanceX = ( size.x * 0.5f ) + ( a.size.x * 0.5f );
    float collisionDistanceY = ( size.y * 0.5f ) + ( a.size.y * 0.5f );
    float collisionDistanceZ = ( size.z * 0.5f ) + ( a.size.z * 0.5f );

    if ( ( fabs( ( pEntityCenter->x + offset.x ) - ( a.pEntityCenter->x + a.offset.x ) ) <= collisionDistanceX )
        && ( fabs( ( pEntityCenter->y + offset.y ) - ( a.pEntityCenter->y + a.offset.y ) ) <= collisionDistanceY )
        && ( fabs( ( pEntityCenter->z + offset.z ) - ( a.pEntityCenter->z + a.offset.z ) ) <= collisionDistanceZ ) ) {
        return 1; // Есть коллизия
    } else {
        return 0; // Нет коллизии
    }
}
*/

/*
================================================================================
EntityBox2D::operator=

FIXME Копировать так нельзя
================================================================================
*/
/*
auto smd::EntityBox2D::operator=( const EntityBox2D& a ) ->void {
    //objectCenter = a.objectCenter;
    //size = a.size;
    //offsetPosition = a.offsetPosition;
}
*/

/*
================================================================================
EntityBox2D::EntityBox2D

FIXME Такой конструктор запрещен
================================================================================
*/
/*
smd::EntityBox3D::EntityBox3D( void ) :
objectCenter( 0.0f, 0.0f, 0.0f ),
size( 1.0f, 1.0f, 1.0f ),
offsetPosition( 0.0f, 0.0f, 0.0f ) {}
*/

/*
================================================================================
EntityBox2D::EntityBox2D
================================================================================
*/
/*
smd::EntityBox2D::EntityBox2D( Vector2f* l_pEntityCenter, const Vector2f& size, const Vector2f& offsetPosition ) :
BoundBox2D( size, offsetPosition ),
pPosition( l_pEntityCenter ) {}
*/

/*
================================================================================
EntityBox2D::EntityBox2D
================================================================================
*/
/*
smd::EntityBox2D::EntityBox2D( Vector2f* l_pEntityCenter, float ocz, float sx, float sy, float ox, float oy ) :
BoundBox2D( Vector2f( sx, sy ), Vector2f( ox, oy ) ),
pPosition( l_pEntityCenter ) {}
*/

/*
================================================================================
EntityBox2D::EntityBox2D

FIXME Так копировать нельзя!
================================================================================
*/
/*
smd::BoundBox3D::BoundBox3D( const BoundingBox3D& a ) :
objectCenter( a.objectCenter ),
size( a.size ),
offsetPosition( a.offsetPosition ) {}
*/
/*
smd::EntityBox2D::EntityBox2D( const EntityBox2D& a ) :
BoundBox2D( a ) {}
*/

/*
================================================================================
EntityBox2D::~EntityBox2D
================================================================================
*/
//smd::EntityBox2D::~EntityBox2D( void ) {}

