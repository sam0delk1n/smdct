//==============================================================================================================================
//
// � 2015-2017 sam0delk1n
//
//==============================================================================================================================

#include "../../inc/cpp/algorithms.hpp"

constexpr float cIntersecEpsilon { 0.000001f };

//==============================================================================================================================
int32_t smd::math::intersecRayTri( const vec3f_t& orig, const vec3f_t& dir, const vec3f_t& vert0, const vec3f_t& vert1,
                                   const vec3f_t& vert2, float& t, float& u, float& v ) {
    vec3f_t edge1, edge2, tvec, pvec, qvec;
    float inv_det;

    // Find vectors for two edges sharing vert0.
    subtract( edge1, vert1, vert0 );
    subtract( edge2, vert2, vert0 );

    // Begin calculating determinant -- also used to calculate U parameter.
    cross( pvec, dir, edge2 );

    // If determinant is near zero, ray lies in plane of triangle.
    const float cDet = dot( edge1, pvec );

    // The non-culling branch.
    if ( cDet > -cIntersecEpsilon && cDet < cIntersecEpsilon ) {
        return 0;
    }
    inv_det = 1 / cDet;

    // Calculate distance from vert0 to ray origin.
    subtract( tvec, orig, vert0 );

    // Calculate U parameter and test bounds.
    // FIXME u = tvec * pvec * inv_det;
    u = dot( tvec, pvec );
    u *= inv_det;
    if ( u < 0.0f || u > 1.0f ) {
        return 0;
    }

    // Prepare to test V parameter.
    cross( qvec, tvec, edge1 );

    // Calculate V parameter and test bounds.
    // FIXME v = dir * qvec * inv_det;
    v = dot( dir, qvec );
    v *= inv_det;
    if ( v < 0.0f || u + v > 1.0f ) {
        return 0;
    }

    // Calculate t, ray intersects triangle.
    // FIXME t = edge2 * qvec * inv_det;
    t = dot( edge2, qvec );
    t *= inv_det;

    return 1;
}
