﻿
#ifndef __ENTITYBOX3D_HPP__
#define __ENTITYBOX3D_HPP__


////////////////////////////////////////////////////////////////////////////////
//
// EntityBox3D.hpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////


namespace smd {


/*******************************************************************************

 EntityBox3D

*******************************************************************************/
/*
class EntityBox3D : BoundBox3D {
	Vector3f*							pPosition;
	
public:
	auto								GetPosition( void ) const ->const Vector3f;
	
	//auto								CheckLineCollision( const Vector3f& point, const Vector3f& vector ) const ->PYR_INT;
	//auto								CheckBoundingBoxCollision( const BoundBox3D& a ) const ->PYR_INT;
	
	auto								operator=( const EntityBox3D& a ) ->void;
	
	//explicit							EntityBox3D( void );
	explicit							EntityBox3D( Vector3f* l_pEntityCenter, const Vector3f& size, const Vector3f& offset = Vector3f( 0.0f, 0.0f, 0.0f ) );
	explicit							EntityBox3D( Vector3f* l_pEntityCenter, float ocz, float sx, float sy, float sz, float ox = 0.0f, float oy = 0.0f, float oz = 0.0f );
										EntityBox3D( const EntityBox3D& a );
	virtual								~EntityBox3D( void );
};
*/
}/*namespace smd*/

#endif/*__ENTITYBOX3D_HPP__*/

