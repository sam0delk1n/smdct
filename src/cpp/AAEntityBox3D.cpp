﻿////////////////////////////////////////////////////////////////////////////////
//
// AAEntityBox3D.cpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include "AAEntityBox3D.hpp"

#include "../../inc/cpp/math/common.hpp"
#include "../../inc/cpp/math/vec3.hpp"
#include "../../inc/cpp/aabb.hpp"

#include <cstdint>
#include <cmath>

/*
================================================================================
AAEntityBox3D::TestCollisionWith
================================================================================
*/
/*
auto smd::AAEntityBox3D::TestCollisionWith( const AABoundBox3D& other ) const noexcept ->int32_t {
	
	if ( !pPosition ) {
		return -1;
	}
	
	Vector3f collisionDistance( GetSize() * 0.5f + other.GetSize() * 0.5f );
	
	if ( ( fabs( pPosition->x + GetOffset().x - other.GetOffset().x ) <= collisionDistance.x )
		 && ( fabs( pPosition->y + GetOffset().y - other.GetOffset().y ) <= collisionDistance.y )
		 && ( fabs( pPosition->y + GetOffset().y - other.GetOffset().y ) <= collisionDistance.z ) ) {
		
		// Есть коллизия.
		return 1;
	} else {
		
		// Нет коллизии.
		return 0;
	}
}
*/

/*
================================================================================
AAEntityBox3D::TestCollisionWith
================================================================================
*/
/*
auto smd::AAEntityBox3D::TestCollisionWith( const AAEntityBox3D& other ) const noexcept ->int32_t {
	
	if ( !pPosition || !other.pPosition ) {
		return -1;
	}
	
	Vector3f collisionDistance( GetSize() * 0.5f + other.GetSize() * 0.5f );
	
	if ( ( fabs( ( pPosition->x + GetOffset().x ) - ( other.pPosition->x + other.GetOffset().x ) ) <= collisionDistance.x )
		 && ( fabs( ( pPosition->y + GetOffset().y ) - ( other.pPosition->y + other.GetOffset().y ) ) <= collisionDistance.y )
		 && ( fabs( ( pPosition->y + GetOffset().y ) - ( other.pPosition->y + other.GetOffset().y ) ) <= collisionDistance.z ) ) {
		
		// Есть коллизия.
		return 1;
	} else {
		
		// Нет коллизии.
		return 0;
	}
}
*/

