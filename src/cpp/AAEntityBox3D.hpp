﻿
#ifndef __AAENTITYBOX3D_HPP__
#define __AAENTITYBOX3D_HPP__


////////////////////////////////////////////////////////////////////////////////
//
// AAEntityBox3D.hpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////


namespace smd {


/*******************************************************************************

 AAEntityBox3D

*******************************************************************************/
/*
class AAEntityBox3D : public AABoundBox3D {
	Vector3f*							pPosition;
	
public:
	virtual auto						TestCollisionWith( const AABoundBox3D& other ) const noexcept ->int32_t override;
	virtual auto						TestCollisionWith( const AAEntityBox3D& other ) const noexcept ->int32_t;
	
	auto								GetPosition( void ) const ->const Vector3f;
	
	auto								operator=( const AAEntityBox3D& a ) ->void;
	
	explicit							AAEntityBox3D( Vector3f* l_pPosition, const Vector3f& offset = Vector3f( 0.0f, 0.0f, 0.0f ) );
										AAEntityBox3D( const AAEntityBox3D& a );
	virtual 							~AAEntityBox3D( void );
};
*/

/*
================================================================================
AAEntityBox3D::GetPosition
================================================================================
*/
/*
inline auto AAEntityBox3D::GetPosition( void ) const ->const Vector3f {
	if ( pPosition ) {
		return ( *pPosition ) + GetOffset();
	} else {
		return Vector3f( 0.0f, 0.0f, 0.0f );
	}
}
*/

/*
================================================================================
AAEntityBox3D::operator=

FIXME Так копировать нелья - копировать нужно весь Entity целиком
================================================================================
*/
/*
inline auto AAEntityBox3D::operator=( const AAEntityBox3D& ) ->void {
	//objectCenter	= a.objectCenter;
	//size			= a.size;
	//offsetPosition	= a.offsetPosition;
}
*/

/*
================================================================================
AAEntityBox3D::AAEntityBox3D
================================================================================
*/
/*
inline AAEntityBox3D::AAEntityBox3D( Vector3f* l_pPosition, const Vector3f& offset ) :
AABoundBox3D( Vector3f( 1.0f, 1.0f, 1.0f ), offset ),
pPosition( l_pPosition ) {}
*/

/*
================================================================================
AAEntityBox3D::AAEntityBox3D
================================================================================
*/
/*
inline AAEntityBox3D::AAEntityBox3D( const AAEntityBox3D& a ) :
AABoundBox3D( a ) {}
*/

/*
================================================================================
AAEntityBox3D::~AAEntityBox3D
================================================================================
*/
//inline AAEntityBox3D::~AAEntityBox3D( void ) {}

}/*namespace smd*/

#endif/*__AAENTITYBOX3D_HPP__*/

