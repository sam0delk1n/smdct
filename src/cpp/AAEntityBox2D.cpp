﻿////////////////////////////////////////////////////////////////////////////////
//
// AAEntityBox2D.cpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include "AAEntityBox2D.hpp"

#include "../../inc/cpp/math/common.hpp"
#include "../../inc/cpp/math/vec2.hpp"
#include "../../inc/cpp/aabb2d.hpp"

#include <cstdint>
#include <cmath>

/*
================================================================================
AAEntityBox2D::TestCollisionWith
================================================================================
*/
/*
auto smd::AAEntityBox2D::TestCollisionWith( const AABoundBox2D& other ) const noexcept ->int32_t {
	
	if ( !pPosition ) {
		return -1;
	}
	
	Vector2f collisionDistance( GetSize() * 0.5f + other.GetSize() * 0.5f );
	
	if ( ( fabs( pPosition->x + GetOffset().x - other.GetOffset().x ) <= collisionDistance.x ) && ( fabs( pPosition->y + GetOffset().y - other.GetOffset().y ) <= collisionDistance.y ) ) {
		
		// Есть коллизия.
		return 1;
	} else {
		
		// Нет коллизии.
		return 0;
	}
}
*/

/*
================================================================================
AAEntityBox2D::TestCollisionWith
================================================================================
*/
/*
auto smd::AAEntityBox2D::TestCollisionWith( const AAEntityBox2D& other ) const noexcept ->int32_t {
	
	if ( !pPosition || !other.pPosition ) {
		return -1;
	}
	
	Vector2f collisionDistance( GetSize() * 0.5f + other.GetSize() * 0.5f );
	
	if ( ( fabs( ( pPosition->x + GetOffset().x ) - ( other.pPosition->x + other.GetOffset().x ) ) <= collisionDistance.x )
		 && ( fabs( ( pPosition->y + GetOffset().y ) - ( other.pPosition->y + other.GetOffset().y ) ) <= collisionDistance.y ) ) {
		
		// Есть коллизия.
		return 1;
	} else {
		
		// Нет коллизии.
		return 0;
	}
}
*/

