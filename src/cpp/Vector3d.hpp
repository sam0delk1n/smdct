﻿
#ifndef __VECTOR3D_HPP__
#define __VECTOR3D_HPP__


//TODO


namespace smd {


/**************************************************************

 Vector3d

 FIXME Пока сделаем виравнивание по 4, но возможно потом нужно изменить.

**************************************************************/

#pragma pack( push, 4 )
class Vector3d {
public:
	
	double								x, y, z;
	
										Vector3d( void );
										Vector3d( double a, double b, double c );
										Vector3d( const Vector3d& a );
	
	void								Set( double a, double b, double c );
	void								Zero( void );

	double								operator[]( int32_t index ) const;
	double&								operator[]( int32_t index );
	
	Vector3d&							operator=( const Vector3d& a ); // Написано что тут происходит какой то баг в msvs 6 и 7

	Vector3d							operator-( void ) const;
	Vector3d							operator+( const Vector3d& a ) const;
	Vector3d							operator-( const Vector3d& a ) const;
	Vector3d							operator*( double a ) const;
	double								operator*( const Vector3d& a ) const;
	Vector3d							operator/( double a ) const;

	Vector3d&							operator+=( const Vector3d& a );
	Vector3d&							operator-=( const Vector3d& a );
	Vector3d&							operator/=( const Vector3d& a );
	Vector3d&							operator/=( double a );
	Vector3d&							operator*=( double a );

	bool								Compare( const Vector3d& a ) const;
	bool								Compare( const Vector3d& a, double epsilon ) const;
	bool								operator==( const Vector3d& a ) const;
	bool								operator!=( const Vector3d& a ) const;

										//FIXME Следующие методы временно отключены, т. к. нету функций двойной точности для нахождения квадратного корня.
	double								Length( void ) const;
	double								LengthSqr( void ) const;
	void								Normalize( void ) throw( except );
	//double							NormalizeFast( void );

//TODO inline Vector3f&			Truncate( double length );
//TODO inline void					Clamp( const Vector3f& min, const Vector3f& max);
//TODO inline void					ClampMin( const double& minx, const double& miny, const double& minz );
//TODO inline void					Snap( void );
//TODO inline void					SnapInt( void );

	Vector3d							Cross( const Vector3d& a ) const;
	Vector3d&							Cross( const Vector3d& a, const Vector3d& b );
	
	int32_t								GetDimension( void ) const;
//TODO inline bool					IsZero( void ) const;

	const double*						ToDoublePtr( void ) const;
	double*								ToDoublePtr( void );
	//int32_t							ToString( char* strBuffer, const size_t strSize, int32_t precision = 2 ) const;
};
#pragma pack ( pop )

/*
================================================================================
Vector3d::Vector3d
================================================================================
*/
inline Vector3d::Vector3d( void ) {}

/*
================================================================================
Vector3d::Vector3d
================================================================================
*/
inline Vector3d::Vector3d( double a, double b, double c ) {
	x = a;
	y = b;
	z = c;
}

/*
================================================================================
Vector3d::Vector3d
================================================================================
*/
inline Vector3d::Vector3d( const Vector3d& a ) {
	x = a.x;
	y = a.y;
	z = a.z;
}

/*
================================================================================
Vector3d::Set
================================================================================
*/
inline void Vector3d::Set( double a, double b, double c ) {
	x = a;
	y = b;
	z = c;
}

/*
================================================================================
Vector3d::Zero
================================================================================
*/
inline void Vector3d::Zero( void ) {
	x = y = z = 0.0;
}

/*
================================================================================
Vector3d::operator[]
================================================================================
*/
inline double Vector3d::operator[]( int32_t index ) const {
	return ( &x )[ index ];
}

/*
================================================================================
Vector3d::operator[]
================================================================================
*/
inline double& Vector3d::operator[]( int32_t index ) {
	return ( &x )[ index ];
}

/*
================================================================================
Vector3d::operator=
================================================================================
*/
inline Vector3d& Vector3d::operator=( const Vector3d& a ) {
	x = a.x;
	y = a.y;
	z = a.z;

	return *this;
}

/*
================================================================================
Vector3d::operator-
================================================================================
*/
inline Vector3d Vector3d::operator-( void ) const {
	return Vector3d( -x, -y, -z );
}

/*
================================================================================
Vector3d::operator+
================================================================================
*/
inline Vector3d Vector3d::operator+( const Vector3d& a ) const {
	return Vector3d( x + a.x, y + a.y, z + a.z );
}

/*
================================================================================
Vector3d::operator-
================================================================================
*/
inline Vector3d Vector3d::operator-( const Vector3d& a ) const {
	return Vector3d( x - a.x, y - a.y, z - a.z );
}

/*
================================================================================
Vector3d::operator*
================================================================================
*/
inline Vector3d Vector3d::operator*( double a ) const {
	return Vector3d( x * a, y * a, z * a );
}

/*
================================================================================
Vector3d::operator*
================================================================================
*/
inline double Vector3d::operator*( const Vector3d& a ) const {
	return x * a.x + y * a.y + z * a.z;
}

/*
================================================================================
Vector3d::operator/
================================================================================
*/
inline Vector3d Vector3d::operator/( double a ) const {
	double inva = 1.0f / a;
	return Vector3d( x * inva, y * inva, z * inva );
}

/*
================================================================================
Vector3d::operator+=
================================================================================
*/
inline Vector3d& Vector3d::operator+=( const Vector3d& a ) {
	x += a.x;
	y += a.y;
	z += a.z;

	return *this;
}

/*
================================================================================
Vector3d::operator-=
================================================================================
*/
inline Vector3d& Vector3d::operator-=( const Vector3d& a ) {
	x -= a.x;
	y -= a.y;
	z -= a.z;

	return *this;
}

/*
================================================================================
Vector3d::operator/=
================================================================================
*/
inline Vector3d& Vector3d::operator/=( const Vector3d& a ) {
	x /= a.x;
	y /= a.y;
	z /= a.z;

	return *this;
}

/*
================================================================================
Vector3d::operator/=
================================================================================
*/
inline Vector3d& Vector3d::operator/=( double a ) {
	double inva = 1.0f / a;
	x *= inva;
	y *= inva;
	z *= inva;

	return *this;
}

/*
================================================================================
Vector3d::operator*=
================================================================================
*/
inline Vector3d& Vector3d::operator*=( double a ) {
	x *= a;
	y *= a;
	z *= a;

	return *this;
}

/*
================================================================================
Vector3d::Compare
================================================================================
*/
inline bool Vector3d::Compare( const Vector3d& a ) const {
	return ( ( x == a.x ) && ( y == a.y ) && ( z == a.z ) );
}

/*
================================================================================
Vector3d::Compare
================================================================================
*/
inline bool Vector3d::Compare( const Vector3d& a, double epsilon ) const {
	if( fabs( x - a.x ) > epsilon ) {
		return false;
	}
	if( fabs( y - a.y ) > epsilon ) {
		return false;
	}
	if( fabs( z - a.z ) > epsilon ) {
		return false;
	}
	
	return true;
}

/*
================================================================================
Vector3d::operator==
================================================================================
*/
inline bool Vector3d::operator==( const Vector3d& a ) const {
	return Compare( a );
}

/*
================================================================================
Vector3d::operator!=
================================================================================
*/
inline bool Vector3d::operator!=( const Vector3d& a ) const {
	return !Compare( a );
}

/*
================================================================================
Vector3d::Length
================================================================================
*/
inline double Vector3d::Length( void ) const {
	return sqrt( x * x + y * y + z * z );
}

/*
================================================================================
Vector3d::LengthSqr
================================================================================
*/
inline double Vector3d::LengthSqr( void ) const {
	return ( x * x + y * y + z * z );
}

/*
================================================================================
Vector3d::Normalize
================================================================================
*/
inline void Vector3d::Normalize( void ) throw( except ) {
	double lenInv = sqrt( x * x + y * y + z * z );
	if ( lenInv == 0.0f ) {
		throw except( "Vector3d::Normalize: lenInv == 0.0f" );
	}
	lenInv = 1.0f / lenInv;
	x *= lenInv;
	y *= lenInv;
	z *= lenInv;
}

/*
================================================================================
Vector3d::NormalizeFast
================================================================================
*/
/*
inline double Vector3d::NormalizeFast( void ) {
	double sqrLen = x * x + y * y + z * z;

	//FIXME Нужно заменить.
	double invLen = Math::InvSqrRootFast( sqrLen ); // Используем InvSqrRootFast, которая учитывает инверсию ( чтобы не делать ее два раза )
	x *= invLen;
	y *= invLen;
	z *= invLen;

	// Что здесь имеется ввиду
	return invLen * sqrLen;
}
*/

/*
================================================================================
Vector3d::Cross
================================================================================
*/
inline Vector3d Vector3d::Cross( const Vector3d& a ) const {
	return Vector3d( y * a.z - z * a.y, z * a.x - x * a.z, x * a.y - y * a.x );
}

/*
================================================================================
Vector3d::Cross
================================================================================
*/
inline Vector3d& Vector3d::Cross( const Vector3d& a, const Vector3d& b ) {
	
	x = a.y * b.z - a.z * b.y;
	y = a.z * b.x - a.x * b.z;
	z = a.x * b.y - a.y * b.x;

	return *this;
}

/*
================================================================================
Vector3d::GetDimension
================================================================================
*/
inline int32_t Vector3d::GetDimension( void ) const {
	return 3;
}

/*
================================================================================
Vector3d::ToDoublePtr
================================================================================
*/
inline const double* Vector3d::ToDoublePtr( void ) const {
	return &x;
}

/*
================================================================================
Vector3d::ToDoublePtr
================================================================================
*/
inline double* Vector3d::ToDoublePtr( void ) {
	return &x;
}

}/*namespace smd*/

#endif/*__VECTOR3D_HPP__*/

