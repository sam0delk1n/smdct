﻿////////////////////////////////////////////////////////////////////////////////
//
// EntityBox3D.cpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////

#include "BoundBox3D.hpp"
#include "EntityBox3D.hpp"

#include "../../inc/cpp/math/common.hpp"
#include "../../inc/cpp/math/vec3.hpp"

#include <cmath>

/*
================================================================================
EntityBox3D::GetPosition
================================================================================
*/
/*
auto smd::EntityBox3D::GetPosition( void ) const ->const Vector3f {
	if ( pPosition ) {
		return ( *pPosition ) + GetOffset();
	} else {
		return Vector3f( 0.0f, 0.0f, 0.0f );
	}
}
*/

/*
================================================================================
EntityBox3D::CheckLineCollision
================================================================================
*/
/*
auto smd::EntityBox3D::CheckLineCollision( const Vector3f& point, const Vector3f& vector ) const ->PYR_INT {
	//TODO
	return -1;
}
*/

/*
================================================================================
EntityBox3D::CheckBoundingBoxCollision
================================================================================
*/
/*
auto smd::EntityBox3D::CheckBoundingBoxCollision( const BoundBox3D& a ) const ->PYR_INT {

	if ( !pEntityCenter ) {
		return -1;
	}
	if ( !a.pEntityCenter ) {
		return -2;
	}

	float collisionDistanceX = ( size.x * 0.5f ) + ( a.size.x * 0.5f );
	float collisionDistanceY = ( size.y * 0.5f ) + ( a.size.y * 0.5f );
	float collisionDistanceZ = ( size.z * 0.5f ) + ( a.size.z * 0.5f );

	if ( ( fabs( ( pEntityCenter->x + offset.x ) - ( a.pEntityCenter->x + a.offset.x ) ) <= collisionDistanceX )
		&& ( fabs( ( pEntityCenter->y + offset.y ) - ( a.pEntityCenter->y + a.offset.y ) ) <= collisionDistanceY )
		&& ( fabs( ( pEntityCenter->z + offset.z ) - ( a.pEntityCenter->z + a.offset.z ) ) <= collisionDistanceZ ) ) {
		return 1; // Есть коллизия
	} else {
		return 0; // Нет коллизии
	}
}
*/

/*
================================================================================
EntityBox3D::operator=

FIXME Копировать так нельзя
================================================================================
*/
/*
auto smd::EntityBox3D::operator=( const EntityBox3D& ) ->void {
	//objectCenter	= a.objectCenter;
	//size			= a.size;
	//offsetPosition	= a.offsetPosition;
}
*/

/*
================================================================================
EntityBox3D::EntityBox3D

FIXME Такой конструктор запрещен
================================================================================
*/
/*
smd::EntityBox3D::EntityBox3D( void ) :
objectCenter( 0.0f, 0.0f, 0.0f ),
size( 1.0f, 1.0f, 1.0f ),
offsetPosition( 0.0f, 0.0f, 0.0f ) {}
*/

/*
================================================================================
EntityBox3D::EntityBox3D
================================================================================
*/
/*
smd::EntityBox3D::EntityBox3D( Vector3f* l_pEntityCenter, const Vector3f& size, const Vector3f& offsetPosition ) :
BoundBox3D( size, offsetPosition ),
pPosition( l_pEntityCenter ) {}
*/

/*
================================================================================
EntityBox3D::EntityBox3D
================================================================================
*/
/*
smd::EntityBox3D::EntityBox3D( Vector3f* l_pEntityCenter, float ocz, float sx, float sy, float sz, float ox, float oy, float oz ) :
BoundBox3D( Vector3f( sx, sy, sz ), Vector3f( ox, oy, oz ) ),
pPosition( l_pEntityCenter ) {}
*/

/*
================================================================================
EntityBox3D::EntityBox3D

FIXME Так копировать нельзя!
================================================================================
*/
/*
smd::BoundBox3D::BoundBox3D( const BoundingBox3D& a ) :
objectCenter( a.objectCenter ),
size( a.size ),
offsetPosition( a.offsetPosition ) {}
*/
/*
smd::EntityBox3D::EntityBox3D( const EntityBox3D& a ) :
BoundBox3D( a ) {}
*/

/*
================================================================================
EntityBox3D::~EntityBox3D
================================================================================
*/
//smd::EntityBox3D::~EntityBox3D( void ) {}

