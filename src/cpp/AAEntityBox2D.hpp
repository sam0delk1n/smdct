﻿
#ifndef __AAENTITYBOX2D_HPP__
#define __AAENTITYBOX2D_HPP__


////////////////////////////////////////////////////////////////////////////////
//
// AAEntityBox2D.hpp
//
// © Samodelkin, 2013
//
//
//
////////////////////////////////////////////////////////////////////////////////


namespace smd {


/*******************************************************************************

 AAEntityBox2D

*******************************************************************************/
/*
class AAEntityBox2D : public AABoundBox2D {
	Vector2f*							pPosition;
	
public:
	virtual auto						TestCollisionWith( const AABoundBox2D& other ) const noexcept ->int32_t override;
	virtual auto						TestCollisionWith( const AAEntityBox2D& other ) const noexcept ->int32_t;
	
	auto								GetPosition( void ) const ->const Vector2f;
	
	auto								operator=( const AAEntityBox2D& a ) ->void;
	
	explicit							AAEntityBox2D( Vector2f* l_pPosition, const Vector2f& offset = Vector2f( 0.0f, 0.0f ) );
										AAEntityBox2D( const AAEntityBox2D& a );
	virtual 							~AAEntityBox2D( void );
};
*/

/*
================================================================================
AAEntityBox2D::GetPosition
================================================================================
*/
/*
inline auto AAEntityBox2D::GetPosition( void ) const ->const Vector2f {
	if ( pPosition ) {
		return ( *pPosition ) + GetOffset();
	} else {
		return Vector2f( 0.0f, 0.0f );
	}
}
*/

/*
================================================================================
AAEntityBox2D::operator=

FIXME Так копировать нелья - копировать нужно весь Entity целиком
================================================================================
*/
/*
inline auto AAEntityBox2D::operator=( const AAEntityBox2D& ) ->void {
	//objectCenter	= a.objectCenter;
	//size			= a.size;
	//offsetPosition	= a.offsetPosition;
}
*/

/*
================================================================================
AAEntityBox2D::AAEntityBox2D
================================================================================
*/
/*
inline AAEntityBox2D::AAEntityBox2D( Vector2f* l_pPosition, const Vector2f& offset ) :
AABoundBox2D( Vector2f( 1.0f, 1.0f ), offset ),
pPosition( l_pPosition ) {}
*/

/*
================================================================================
AAEntityBox2D::AAEntityBox2D
================================================================================
*/
/*
inline AAEntityBox2D::AAEntityBox2D( const AAEntityBox2D& a ) :
AABoundBox2D( a ) {}
*/

/*
================================================================================
AAEntityBox2D::~AAEntityBox2D
================================================================================
*/
//inline AAEntityBox2D::~AAEntityBox2D( void ) {}

}/*namespace smd*/

#endif/*__AAENTITYBOX2D_HPP__*/

