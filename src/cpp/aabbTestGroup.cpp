﻿
#include "../../inc/cpp/aabbTestGroup.hpp"

using namespace smd;

/*
================================================================================
aabbTestGroup_c::get
================================================================================
*/
auto aabbTestGroup_c::get( const uint32_t index ) ->aabb_t& {
    return aabbGroup.at( index );
}

/*
================================================================================
aabbTestGroup_c::get
================================================================================
*/
auto aabbTestGroup_c::get( const uint32_t index ) const ->const aabb_t& {
    return aabbGroup.at( index );
}

/*
================================================================================
aabbTestGroup_c::add
================================================================================
*/
auto aabbTestGroup_c::add( const aabb_t& aabb ) ->size_t {
    aabbGroup.push_back( aabb );
    return { aabbGroup.size() - 1 };
}

/*
================================================================================
aabbTestGroup_c::remove
================================================================================
*/
auto aabbTestGroup_c::remove( const uint32_t index ) ->void {
    aabbGroup.erase( aabbGroup.begin() + index );
}

/*
================================================================================
aabbTestGroup_c::computeIterativeOffset
================================================================================
*/
auto aabbTestGroup_c::computeIterativeOffset( const uint32_t aabbCurrent, const math::vec3f_t& offset, const float deltaOffset ) ->int32_t {

    // РџСЂРѕРІРµСЂСЏРµРј РЅРѕРјРµСЂ Р±РѕРєСЃР°, РµСЃР»Рё РЅРѕРјРµСЂ РЅРµ РІРµСЂРЅС‹Р№, С„СѓРЅРєС†РёСЏ at() РєРёРЅРµС‚ РёСЃРєР»СЋС‡РµРЅРёРµ.
    // Р”Р°Р»РµРµ РІ С‚РµР»Рµ С„СѓРЅРєС†РёРё РІСЃРµ РѕР±СЂР°С‰РµРЅРёСЏ Р±СѓРґСѓС‚ С‡РµСЂРµР· РѕРїРµСЂР°С‚РѕСЂ [] Р±РµР· РїСЂРѕРІРµСЂРєРё РґРёР°РїР°Р·РѕРЅР°.
    aabbGroup.at( aabbCurrent );

    // Р—РЅР°С‡РµРЅРёРµ РґР»РёРЅС‹ Р·Р°РїРёСЃС‹РІР°РµРј РѕС‚РґРµР»СЊРЅРѕ, С‡С‚РѕР±С‹ РЅРµ СЃС‡РёС‚Р°С‚СЊ РєР°Р¶РґС‹Р№ СЂР°Р· sqrt ( РєРѕС‚РѕСЂС‹Р№ РІРЅСѓС‚СЂРё Length() ).
    const float cMoveLen = length( offset );

    // РџСЂРѕРІРµСЂСЏРµРј РІРµРєС‚РѕСЂ move, РµСЃР»Рё РµРіРѕ РґР»РёРЅР° РЅСѓР»РµРІР°СЏ,
    // С‚Рѕ Рё С‚РµСЃС‚РёСЂРѕРІР°С‚СЊ РЅРёС‡РµРіРѕ РЅРµ РЅР°РґРѕ - РѕР±СЉРµРєС‚ РІСЃРµСЂР°РІРЅРѕ РЅРёРєСѓРґР° РЅРµ СЃРѕР±РёСЂР°РµС‚СЃСЏ РїРµСЂРµРјРµС‰Р°С‚СЊСЃСЏ.
    if ( !cMoveLen ) {
        return -1;
    }

    // Р”Р»РёРЅР° РІРµРєС‚РѕСЂР° move Рё РЅРѕСЂРјР°Р»РёР·РѕРІР°РЅРЅР°СЏ РІРµСЂСЃРёСЏ move ( С‡С‚РѕР±С‹ РЅРµ СЃС‡РёС‚Р°С‚СЊ РёС… РµС‰Рµ СЂР°Р· РїРѕС‚РѕРј ).
    //FIXME Р”Р»РёРЅР° РІРµРєС‚РѕСЂР° move РЅРµ РґРѕР»Р¶РЅР° Р±С‹С‚СЊ РЅСѓР»РµРІРѕР№ ( РїСЂРµРґС‹РґСѓС‰РёР№ РєРѕРґ СЌС‚Рѕ РїСЂРѕРІРµСЂСЏРµС‚ ),
    // РїРѕС‚РѕРјСѓ С‡С‚Рѕ РЅРѕСЂРјР°Р»РёР·Р°С†РёСЏ РґРѕР»Р¶РЅР° РїСЂРёРјРµРЅСЏС‚СЊСЃСЏ Рє РЅРµРЅСѓР»РµРІРѕРјСѓ РІРµРєС‚РѕСЂСѓ.
    math::vec3f_t moveNorm( offset );
    normalize( moveNorm, moveNorm, math::cEpsilon );

    // РЎРѕР·РґР°РµРј РєРѕРЅС‚РµР№СЂРµРЅ СЃ СѓРєР°Р·Р°С‚РµР»СЏРјРё.
    std::vector< uint32_t > boxesPtrs;

    findPotentialCollision( aabbCurrent, aabbGroup, boxesPtrs, cMoveLen );

    // Р”РµР»СЊС‚Р° РїСЂРёСЂР°С‰РµРЅРёСЏ РїРѕ РґРІСѓРј РѕСЃСЏРј.
    math::vec3f_t delta;
    scale( delta, moveNorm, deltaOffset );

    // РљРѕР»РёС‡РµСЃС‚РІРѕ РёС‚РµСЂР°С†РёР№.
    const uint32_t iter { static_cast< uint32_t >( fabsf( cMoveLen / deltaOffset ) ) };
    const float rem { fmodf( cMoveLen, deltaOffset ) };

    // РљРѕРґ РІРѕР·РІСЂР°С‚Р°.
    int32_t retStat { 0 };

    // Р”Р°Р»РµРµ РёРґРµС‚ РїРѕС€Р°РіРѕРІР°СЏ РїСЂРѕРІРµСЂРєР°.
    for ( uint32_t i { 0 }; i < iter; ++i ) {
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, delta ) ) continue;
        retStat = 2;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { delta.x, 0.0f, delta.z } ) ) continue;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { delta.x, delta.y, 0.0f } ) ) continue;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, delta.y, delta.z } ) ) continue;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { delta.x, 0.0f, 0.0f } ) ) continue;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, 0.0f, delta.z } ) ) continue;
        if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, delta.y, 0.0f } ) ) continue;

        // Р’ СЌС‚РѕРј СЃР»СѓС‡Р°Рµ С†РµР»РµРІРѕР№ AABB РЅРµ СЃРјРѕРі РЅРёРєСѓРґР° СЃРґРІРёРЅСѓС‚СЊСЃСЏ,
        // С‚Р°Рє РєР°Рє РІСЃРµ РѕСЃС‚Р°Р»СЊРЅС‹Рµ AABB С‚РµСЃС‚ РіСЂСѓРїРїС‹ РЅР° РґР°РЅРЅРѕРј СЌС‚Р°РїРµ СЃС‡РёС‚Р°СЋС‚СЃСЏ СЃС‚Р°С‚РёС‡РЅС‹РјРё,
        // С‚Рѕ РґР°Р»СЊРЅРµР№С€РёР№ СЂР°СЃС‡РµС‚ С†РµР»РµРІРѕРіРѕ AABB РЅРµ РёРјРµРµС‚ СЃРјС‹СЃР»Р°.
        return 1;
    }

    // Р РѕСЃС‚Р°С‚РѕРє.
    math::vec3f_t remDelta;
    scale( remDelta, moveNorm, rem );
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, remDelta ) ) return retStat;
    retStat = 2;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { remDelta.x, 0.0f, remDelta.z } ) ) return retStat;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { remDelta.x, remDelta.y, 0.0f } ) ) return retStat;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, remDelta.y, remDelta.z } ) ) return retStat;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { remDelta.x, 0.0f, 0.0f } ) ) return retStat;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, 0.0f, remDelta.z } ) ) return retStat;
    if ( !computeDeltaOffset( aabbCurrent, aabbGroup, boxesPtrs, { 0.0f, remDelta.y, 0.0f } ) ) return retStat;

    // Р’ СЌС‚РѕРј СЃР»СѓС‡Р°Рµ С†РµР»РµРІРѕР№ AABB РЅРµ СЃРјРѕРі РЅРёРєСѓРґР° СЃРґРІРёРЅСѓС‚СЊСЃСЏ,
    // С‚Р°Рє РєР°Рє РІСЃРµ РѕСЃС‚Р°Р»СЊРЅС‹Рµ AABB С‚РµРєСЃС‚ РіСЂСѓРїРїС‹ РЅР° РґР°РЅРЅРѕРј СЌС‚Р°РїРµ СЃС‡РёС‚Р°СЋС‚СЃСЏ СЃС‚Р°С‚РёС‡РЅС‹РјРё,
    // С‚Рѕ РґР°Р»СЊРЅРµР№С€РёР№ СЂР°СЃС‡РµС‚ С†РµР»РµРІРѕРіРѕ AABB РЅРµ РёРјРµРµС‚ СЃРјС‹СЃР»Р°.
    return 1;
}

/*
================================================================================
aabbTestGroup_c::operator=
================================================================================
*/
auto aabbTestGroup_c::operator=( const aabbTestGroup_c& other ) ->aabbTestGroup_c& {
    aabbGroup = other.aabbGroup;
    return *this;
}

/*
================================================================================
aabbTestGroup_c::aabbTestGroup_c
================================================================================
*/
aabbTestGroup_c::aabbTestGroup_c( const aabbTestGroup_c& other ) :
aabbGroup( other.aabbGroup ) {}

/*
================================================================================
aabbTestGroup_c::findPotentialCollision
================================================================================
*/
auto aabbTestGroup_c::findPotentialCollision( const uint32_t current,
                                              const std::vector< aabb_t >& all,
                                              std::vector< uint32_t >& found,
                                              const float moveLen ) noexcept ->bool {
    bool collisionFound { false };
    for ( uint32_t i { 0 }; i < all.size(); ++i ) {
        if ( i == current ) continue;

        math::vec3f_t delta;
        subtract( delta, all[ i ].position, all[ current ].position );
        const float cLenBetween2aabb = length( delta );
        const float potentialCollisionLen { moveLen + aabb_t::diagonalHalfLen( all[ current ] ) + aabb_t::diagonalHalfLen( all[ i ] ) };
        if ( cLenBetween2aabb <= potentialCollisionLen ) {
            found.push_back( i );
            collisionFound = true;
        }
    }
    return collisionFound;
}

/*
================================================================================
aabbTestGroup_c::computeDeltaOffset
================================================================================
*/
auto aabbTestGroup_c::computeDeltaOffset( const uint32_t current,
                                          std::vector< aabb_t >& all,
                                          const std::vector< uint32_t >& found,
                                          const math::vec3f_t& deltaOffset ) noexcept ->bool {
    aabb_t aabb( all[ current ] );
    math::add( aabb.position, aabb.position, deltaOffset );
    for ( const auto x : found ) {
        if ( aabb_t::testCollision( aabb, all[ x ] ) ) return true;
    }
    all[ current ] = aabb;
    return false;
}

