//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/vec3f_t.h"

//==================================================================================================
void smdVec3f_set( smdVec3f_t* pOut, float x, float y, float z ) {
    pOut->x = x;
    pOut->y = y;
    pOut->z = z;
}

//==================================================================================================
void smdVec3f_negate( smdVec3f_t* pOut, const smdVec3f_t* cpIn ) {
    pOut->x = -cpIn->x;
    pOut->y = -cpIn->y;
    pOut->z = -cpIn->z;
}

//==================================================================================================
void smdVec3f_add( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 ) {
    pOut->x = cpIn1->x + cpIn2->x;
    pOut->y = cpIn1->y + cpIn2->y;
    pOut->z = cpIn1->z + cpIn2->z;
}

//==================================================================================================
void smdVec3f_subtract( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 ) {
    pOut->x = cpIn1->x - cpIn2->x;
    pOut->y = cpIn1->y - cpIn2->y;
    pOut->z = cpIn1->z - cpIn2->z;
}

//==================================================================================================
void smdVec3f_scale( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float scale ) {
    pOut->x = cpIn->x * scale;
    pOut->y = cpIn->y * scale;
    pOut->z = cpIn->z * scale;
}

//==================================================================================================
float smdVec3f_dot( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 ) {
    return cpIn1->x * cpIn2->x + cpIn1->y * cpIn2->y + cpIn1->z * cpIn2->z;
}

//==================================================================================================
bool smdVec3f_compare( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2, float eps ) {
    if ( fabsf( cpIn1->x - cpIn2->x ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->y - cpIn2->y ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->z - cpIn2->z ) > eps ) {
        return false;
    }
    return true;
}

//==================================================================================================
float smdVec3f_length( const smdVec3f_t* cpIn ) {
    return sqrtf( smdVec3f_dot( cpIn, cpIn ) );
}

//==================================================================================================
void smdVec3f_normalize( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float eps ) {
    float len = smdVec3f_length( cpIn );
    if ( len > eps ) {
        len = 1.0f / len;
        smdVec3f_scale( pOut, cpIn, len );
    } else {
        pOut->x = pOut->y = pOut->z = 0.0f;
    }
}

//==================================================================================================
float smdVec3f_distance( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 ) {
    smdVec3f_t subtracted;
    smdVec3f_subtract( &subtracted, cpIn2, cpIn1 );
    return smdVec3f_length( &subtracted );
}

//==================================================================================================
void smdVec3f_toDword( uint32_t* pOut, const smdVec3f_t* cpIn ) {
    *pOut = 0;
    *pOut |= ( ( uint32_t )( cpIn->x * 255.0f ) << 16 ) & 0x00ff0000;
    *pOut |= ( ( uint32_t )( cpIn->y * 255.0f ) <<  8 ) & 0x0000ff00;
    *pOut |=   ( uint32_t )( cpIn->z * 255.0f )         & 0x000000ff;
}

//==================================================================================================
void smdVec3f_fromDword( smdVec3f_t* pOut, uint32_t in ) {
    pOut->x = ( uint8_t )( in >> 16 ) / 255.0f;
    pOut->y = ( uint8_t )( in >>  8 ) / 255.0f;
    pOut->z = ( uint8_t )( in       ) / 255.0f;
}

//==================================================================================================
void smdVec3f_clamp( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float low, float high ) {
    *pOut = *cpIn;
    if      ( pOut->x < low  ) pOut->x = low;
    else if ( pOut->x > high ) pOut->x = high;
    if      ( pOut->y < low  ) pOut->y = low;
    else if ( pOut->y > high ) pOut->y = high;
    if      ( pOut->z < low  ) pOut->z = low;
    else if ( pOut->z > high ) pOut->z = high;
}

//==================================================================================================
void smdVec3f_saturate( smdVec3f_t* pOut, const smdVec3f_t* cpIn ) {
    smdVec3f_clamp( pOut, cpIn, 0.0f, 1.0f );
}

//==================================================================================================
// TODO It is different for the left-hand basis and for the right-hand basis.
void smdVec3f_cross( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 ) {
    smdVec3f_set( pOut, cpIn1->y * cpIn2->z - cpIn1->z * cpIn2->y,
                        cpIn1->z * cpIn2->x - cpIn1->x * cpIn2->z,
                        cpIn1->x * cpIn2->y - cpIn1->y * cpIn2->x );
}
