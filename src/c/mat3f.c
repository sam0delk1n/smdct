//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/mat3f_t.h"

//==================================================================================================
void smdMat3f_identity( smdMat3f_t* pOut ) {
    smdVec3f_set( &pOut->r1, 1, 0, 0 );
    smdVec3f_set( &pOut->r2, 0, 1, 0 );
    smdVec3f_set( &pOut->r3, 0, 0, 1 );
}

//==================================================================================================
void smdMat3f_set(
    smdMat3f_t* pOut,
    float m11, float m12, float m13,
    float m21, float m22, float m23,
    float m31, float m32, float m33
) {
    smdVec3f_set( &pOut->r1, m11, m12, m13 );
    smdVec3f_set( &pOut->r2, m21, m22, m23 );
    smdVec3f_set( &pOut->r3, m31, m32, m33 );
}

//==================================================================================================
bool smdMat3f_compare( const smdMat3f_t* cpIn1, const smdMat3f_t* cpIn2, float epsilon ) {
    if ( !smdVec3f_compare( &cpIn1->r1, &cpIn2->r1, epsilon ) ) return false;
    if ( !smdVec3f_compare( &cpIn1->r2, &cpIn2->r2, epsilon ) ) return false;
    if ( !smdVec3f_compare( &cpIn1->r3, &cpIn2->r3, epsilon ) ) return false;

    return true;
}

//==================================================================================================
float smdMat3f_determinant( const smdMat3f_t* cpIn ) {
    smdMat2f_t mat11,    mat12,    mat13;
    float      detMat11, detMat12, detMat13;

    smdMat2f_set( &mat11, cpIn->r2.y, cpIn->r2.z, cpIn->r3.y, cpIn->r3.z );
    smdMat2f_set( &mat12, cpIn->r2.x, cpIn->r2.z, cpIn->r3.x, cpIn->r3.z );
    smdMat2f_set( &mat13, cpIn->r2.x, cpIn->r2.y, cpIn->r3.x, cpIn->r3.y );

    detMat11 = smdMat2f_determinant( &mat11 );
    detMat12 = smdMat2f_determinant( &mat12 );
    detMat13 = smdMat2f_determinant( &mat13 );

    return cpIn->r1.x * detMat11 - cpIn->r1.y * detMat12 + cpIn->r1.z * detMat13;
}

//==================================================================================================
void smdMat3f_rotationX( smdMat3f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat3f_set(
        pOut,
        1, 0,    0,
        0, cCos, -cSin,
        0, cSin, cCos
    );
}

//==================================================================================================
void smdMat3f_rotationY( smdMat3f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat3f_set(
        pOut,
        cCos,  0, cSin,
        0,     1, 0,
        -cSin, 0, cCos
    );
}

//==================================================================================================
void smdMat3f_rotationZ( smdMat3f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat3f_set(
        pOut,
        cCos, -cSin, 0,
        cSin, cCos,  0,
        0,    0,     1
    );
}

//==================================================================================================
void smdMat3fProperEulerAnglesZXZ( smdMat3f_t* pOut, float z1, float x2, float z3 ) {
    smdMat3f_t mat;

    smdMat3f_rotationZ( pOut, z1 );
    smdMat3f_rotationX( &mat, x2 );
    smdMat3f_multiply( pOut, pOut, &mat );

    smdMat3f_rotationZ( &mat, z3 );
    smdMat3f_multiply( pOut, pOut, &mat );
}

//==================================================================================================
void smdMat3fProperEulerAnglesZXZFast( smdMat3f_t* pOut, float z1, float x2, float z3 ) {
    const float s1 = sinf( z1 );
    const float s2 = sinf( x2 );
    const float s3 = sinf( z3 );

    const float c1 = cosf( z1 );
    const float c2 = cosf( x2 );
    const float c3 = cosf( z3 );

    pOut->r1.x = c1 * c3 - s1 * c2 * s3;
    pOut->r1.y = -c1 * s3 - s1 * c2 * c3;
    pOut->r1.z = s1 * s2;

    pOut->r2.x = s1 * c3 + c1 * c2 * s3;
    pOut->r2.y = -s1 * s3 + c1 * c2 * c3;
    pOut->r2.z = -c1 * s2;

    pOut->r3.x = s2 * s3;
    pOut->r3.y = s2 * c3;
    pOut->r3.z = c2;
}

//==================================================================================================
void smdMat3fTaitBryanAnglesZXY( smdMat3f_t* pOut, float z, float x, float y ) {
    smdMat3f_t mat;

    smdMat3f_rotationZ( pOut, z );
    smdMat3f_rotationX( &mat, x );
    smdMat3f_multiply( pOut, pOut, &mat );

    smdMat3f_rotationZ( &mat, y );
    smdMat3f_multiply( pOut, pOut, &mat );
}

//==================================================================================================
void smdMat3f_multiplyVec( smdVec3f_t* pOut, const smdVec3f_t* cpVec, const smdMat3f_t* cpMat ) {
    smdMat3f_t transposed;
    smdMat3f_transpose( &transposed, cpMat );

    smdVec3f_set(
        pOut,
        smdVec3f_dot( cpVec, &transposed.r1 ),
        smdVec3f_dot( cpVec, &transposed.r2 ),
        smdVec3f_dot( cpVec, &transposed.r3 )
    );
}

//==================================================================================================
void smdMat3f_multiply( smdMat3f_t* pOut, const smdMat3f_t* cpIn1, const smdMat3f_t* cpIn2 ) {
    smdMat3f_t transposed;
    smdMat3f_transpose( &transposed, cpIn2 );

    smdVec3f_set(
        &pOut->r1,
        smdVec3f_dot( &cpIn1->r1, &transposed.r1 ),
        smdVec3f_dot( &cpIn1->r1, &transposed.r2 ),
        smdVec3f_dot( &cpIn1->r1, &transposed.r3 )
    );
    smdVec3f_set(
        &pOut->r2,
        smdVec3f_dot( &cpIn1->r2, &transposed.r1 ),
        smdVec3f_dot( &cpIn1->r2, &transposed.r2 ),
        smdVec3f_dot( &cpIn1->r2, &transposed.r3 )
    );
    smdVec3f_set(
        &pOut->r3,
        smdVec3f_dot( &cpIn1->r3, &transposed.r1 ),
        smdVec3f_dot( &cpIn1->r3, &transposed.r2 ),
        smdVec3f_dot( &cpIn1->r3, &transposed.r3 )
    );
}

//==================================================================================================
void smdMat3f_transpose( smdMat3f_t* pOut, const smdMat3f_t* cpIn ) {
    const float cIn_r1_y = cpIn->r1.y;
    const float cIn_r1_z = cpIn->r1.z;

    const float cIn_r2_z = cpIn->r2.z;

    pOut->r1.x = cpIn->r1.x;
    pOut->r1.y = cpIn->r2.x;
    pOut->r1.z = cpIn->r3.x;

    pOut->r2.x = cIn_r1_y;
    pOut->r2.y = cpIn->r2.y;
    pOut->r2.z = cpIn->r3.y;

    pOut->r3.x = cIn_r1_z;
    pOut->r3.y = cIn_r2_z;
    pOut->r3.z = cpIn->r3.z;
}

/*
//==================================================================================================
auto smd::Matrix3f::sInverse( Matrix3f& output, const Matrix3f& cMat ) noexcept ->bool {
    Matrix3f temp;
    float det, invDet;

    temp[ 0 ].x = cMat[ 1 ].y * cMat[ 2 ].z - cMat[ 1 ].z * cMat[ 2 ].y;
    temp[ 1 ].x = cMat[ 1 ].z * cMat[ 2 ].x - cMat[ 1 ].x * cMat[ 2 ].z;
    temp[ 2 ].x = cMat[ 1 ].x * cMat[ 2 ].y - cMat[ 1 ].y * cMat[ 2 ].x;

    det = cMat[ 0 ].x * temp[ 0 ].x + cMat[ 0 ].y * temp[ 1 ].x + cMat[ 0 ].z * temp[ 2 ].x;

    if ( fabsf( det ) < MATRIX_INVERSE_EPSILON ) return false;

    invDet = 1.0f / det;

    temp[ 0 ].y = cMat[ 0 ].z * cMat[ 2 ].y - cMat[ 0 ].y * cMat[ 2 ].z;
    temp[ 0 ].z = cMat[ 0 ].y * cMat[ 1 ].z - cMat[ 0 ].z * cMat[ 1 ].y;
    temp[ 1 ].y = cMat[ 0 ].x * cMat[ 2 ].z - cMat[ 0 ].z * cMat[ 2 ].x;
    temp[ 1 ].z = cMat[ 0 ].z * cMat[ 1 ].x - cMat[ 0 ].x * cMat[ 1 ].z;
    temp[ 2 ].y = cMat[ 0 ].y * cMat[ 2 ].x - cMat[ 0 ].x * cMat[ 2 ].y;
    temp[ 2 ].z = cMat[ 0 ].x * cMat[ 1 ].y - cMat[ 0 ].y * cMat[ 1 ].x;

    output[ 0 ].x = temp[ 0 ].x * invDet;
    output[ 0 ].y = temp[ 0 ].y * invDet;
    output[ 0 ].z = temp[ 0 ].z * invDet;

    output[ 1 ].x = temp[ 1 ].x * invDet;
    output[ 1 ].y = temp[ 1 ].y * invDet;
    output[ 1 ].z = temp[ 1 ].z * invDet;

    output[ 2 ].x = temp[ 2 ].x * invDet;
    output[ 2 ].y = temp[ 2 ].y * invDet;
    output[ 2 ].z = temp[ 2 ].z * invDet;

    return true;
}
*/

//==================================================================================================
bool smdMat3f_isSymmetric( const smdMat3f_t* cpIn, float epsilon ) {
    smdMat3f_t transposed;
    smdMat3f_transpose( &transposed, cpIn );

    if ( !smdMat3f_compare( &transposed, cpIn, epsilon ) ) return false;

    return true;
}

//==================================================================================================
bool smdMat3f_isOrthogonal( const smdMat3f_t* cpIn, float epsilon ) {
    smdMat3f_t identity;
    smdMat3f_identity( &identity );

    smdMat3f_t transposed;
    smdMat3f_transpose( &transposed, cpIn );

    smdMat3f_t mat1, mat2;
    smdMat3f_multiply( &mat1, cpIn, &transposed );
    smdMat3f_multiply( &mat2, &transposed, cpIn );

    // We must do three compares, because there is epsilon.
    if ( !smdMat3f_compare( &identity, &mat1, epsilon ) ) return false;
    if ( !smdMat3f_compare( &identity, &mat2, epsilon ) ) return false;
    if ( !smdMat3f_compare( &mat1, &mat2, epsilon ) )     return false;

    return true;
}
