//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 mr.DIMAS, sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-pool/pool-lite.h"

//==================================================================================================
// TODO Check the return pointer?
static stamp_t* smdCT_lpool_getStamp( smdCT_lpool_t* p, size_t i ) {
    return ( stamp_t* )( ( char* )p->records + i * p->record_size );
}

//==================================================================================================
int smdCT_lpool_init( smdCT_lpool_t* p, size_t capacity, size_t object_size ) {
    if ( !capacity ) {
        return 1;
    }
    if ( !object_size ) {
        return 1;
    }

    p->records        = NULL;
    p->record_size    = object_size + sizeof( stamp_t );
    p->capacity       = capacity;
    p->spawned_count  = 0;
    p->global_stamp   = 1;
    p->free_stack     = NULL;
    p->free_stack_top = 0;

    p->records = calloc( p->capacity, p->record_size );

    if ( !p->records ) {
        smdCT_lpool_clear( p );
        return 1;
    }

    // Add new free object indices to stack.
    p->free_stack = calloc( p->capacity, sizeof( size_t ) );

    if ( !p->free_stack ) {
        smdCT_lpool_clear( p );
        return 1;
    }

    for ( int64_t i = p->capacity - 1; i >= 0; --i ) {
        p->free_stack[ p->free_stack_top++ ] = i;
    }

    return 0;
}

//==================================================================================================
void smdCT_lpool_clear( smdCT_lpool_t* p ) {
    free( p->free_stack );
    free( p->records );

    p->records        = NULL;
    p->record_size    = 0;
    p->capacity       = 0;
    p->spawned_count  = 0;
    p->global_stamp   = 0;
    p->free_stack     = NULL;
    p->free_stack_top = 0;
}

//==================================================================================================
ref_t smdCT_lpool_spawn( smdCT_lpool_t* p ) {
    ref_t    ref = { 0 };
    stamp_t* stamp;

    if ( p->spawned_count >= p->capacity ) {
        return ref;
    }

    ref.index = p->free_stack[ --p->free_stack_top ];
    stamp     = smdCT_lpool_getStamp( p, ref.index );

    // Generate new stamp and share it between reference and object.
    ref.stamp = p->global_stamp++;
    *stamp = ref.stamp;

    ++p->spawned_count;

    return ref;
}

//==================================================================================================
int smdCT_lpool_isRef( smdCT_lpool_t* p, ref_t ref ) {
    if ( ( unsigned )ref.index >= p->capacity ) {
        return 0;
    }

    return *smdCT_lpool_getStamp( p, ref.index ) == ref.stamp;
}

//==================================================================================================
void smdCT_lpool_return( smdCT_lpool_t* p, ref_t ref ) {
    stamp_t* stamp;
    if(!smdCT_lpool_isRef(p, ref)) {
        return;
    }
    stamp = smdCT_lpool_getStamp(p, ref.index);
    if ( !*stamp ) {
        return;
    }
    *stamp = 0;
    --p->spawned_count;

    // Push index of returned object to free object indices stack.
    p->free_stack[ p->free_stack_top++ ] = ref.index;
}

//==================================================================================================
void* smdCT_lpool_getByIndex( smdCT_lpool_t* p, size_t i ) {
    stamp_t* stamp = smdCT_lpool_getStamp(p, i);
    if ( !*stamp ) {
        return NULL;
    }
    return (char*)stamp + sizeof(stamp_t);
}

//==================================================================================================
void* smdCT_lpool_get( smdCT_lpool_t* p, ref_t ref ) {
    return (char*)p->records + p->record_size * ref.index + sizeof(stamp_t);
}
