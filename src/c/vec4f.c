//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/vec4f_t.h"

//==================================================================================================
void smdVec4f_set( smdVec4f_t* pOut, float x, float y, float z, float w ) {
    pOut->x = x;
    pOut->y = y;
    pOut->z = z;
    pOut->w = w;
}

//==================================================================================================
void smdVec4f_negate( smdVec4f_t* pOut, const smdVec4f_t* cpIn ) {
    pOut->x = -cpIn->x;
    pOut->y = -cpIn->y;
    pOut->z = -cpIn->z;
    pOut->w = -cpIn->w;
}

//==================================================================================================
void smdVec4f_add( smdVec4f_t* pOut, const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 ) {
    pOut->x = cpIn1->x + cpIn2->x;
    pOut->y = cpIn1->y + cpIn2->y;
    pOut->z = cpIn1->z + cpIn2->z;
    pOut->w = cpIn1->w + cpIn2->w;
}

//==================================================================================================
void smdVec4f_subtract( smdVec4f_t* pOut, const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 ) {
    pOut->x = cpIn1->x - cpIn2->x;
    pOut->y = cpIn1->y - cpIn2->y;
    pOut->z = cpIn1->z - cpIn2->z;
    pOut->w = cpIn1->w - cpIn2->w;
}

//==================================================================================================
void smdVec4f_scale( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float scale ) {
    pOut->x = cpIn->x * scale;
    pOut->y = cpIn->y * scale;
    pOut->z = cpIn->z * scale;
    pOut->w = cpIn->w * scale;
}

//==================================================================================================
float smdVec4f_dot( const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 ) {
    return cpIn1->x * cpIn2->x + cpIn1->y * cpIn2->y + cpIn1->z * cpIn2->z + cpIn1->w * cpIn2->w;
}

//==================================================================================================
bool smdVec4f_compare( const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2, float eps ) {
    if ( fabsf( cpIn1->x - cpIn2->x ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->y - cpIn2->y ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->z - cpIn2->z ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->w - cpIn2->w ) > eps ) {
        return false;
    }
    return true;
}

//==================================================================================================
float smdVec4f_length( const smdVec4f_t* cpIn ) {
    return sqrtf( smdVec4f_dot( cpIn, cpIn ) );
}

//==================================================================================================
void smdVec4f_normalize( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float eps ) {
    float len = smdVec4f_length( cpIn );
    if ( len > eps ) {
        len = 1 / len;
        smdVec4f_scale( pOut, cpIn, len );
    } else {
        pOut->x = pOut->y = pOut->z = pOut->w = 0;
    }
}

//==================================================================================================
void smdVec4f_toDword( uint32_t* pOut, const smdVec4f_t* cpIn ) {
    *pOut = 0;
    *pOut |= ( ( uint32_t )( cpIn->x * 255.0f ) << 24 ) & 0xff000000;
    *pOut |= ( ( uint32_t )( cpIn->y * 255.0f ) << 16 ) & 0x00ff0000;
    *pOut |= ( ( uint32_t )( cpIn->z * 255.0f ) <<  8 ) & 0x0000ff00;
    *pOut |=   ( uint32_t )( cpIn->w * 255.0f )         & 0x000000ff;
}

//==================================================================================================
void smdVec4f_fromDword( smdVec4f_t* pOut, uint32_t in ) {
    pOut->x = ( uint8_t )( in >> 24 ) / 255.0f;
    pOut->y = ( uint8_t )( in >> 16 ) / 255.0f;
    pOut->z = ( uint8_t )( in >>  8 ) / 255.0f;
    pOut->w = ( uint8_t )( in       ) / 255.0f;
}

//==================================================================================================
void smdVec4f_clamp( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float low, float high ) {
    *pOut = *cpIn;

    if ( pOut->x < low ) {
        pOut->x = low;
    } else if ( pOut->x > high ) {
        pOut->x = high;
    }
    if ( pOut->y < low ) {
        pOut->y = low;
    } else if ( pOut->y > high ) {
        pOut->y = high;
    }
    if ( pOut->z < low ) {
        pOut->z = low;
    } else if ( pOut->z > high ) {
        pOut->z = high;
    }
    if ( pOut->w < low ) {
        pOut->w = low;
    } else if ( pOut->w > high ) {
        pOut->w = high;
    }
}

//==================================================================================================
void smdVec4f_saturate( smdVec4f_t* pOut, const smdVec4f_t* cpIn ) {
    smdVec4f_clamp( pOut, cpIn, 0.0f, 1.0f );
}
