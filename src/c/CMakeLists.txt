cmake_minimum_required(VERSION 3.20 FATAL_ERROR)

set(
    H_FILES
    ../../inc/c/smdCT.h
    ../../inc/c/smd-math.h
    ../../inc/c/smd-math/common.h
    ../../inc/c/smd-math/vec2f_t.h
    ../../inc/c/smd-math/vec3f_t.h
    ../../inc/c/smd-math/vec4f_t.h
    ../../inc/c/smd-math/mat2f_t.h
    ../../inc/c/smd-math/mat3f_t.h
    ../../inc/c/smd-math/mat4f_t.h
    ../../inc/c/smd-math/quatf_t.h
    ../../inc/c/smd-pool.h
    ../../inc/c/smd-pool/pool-common.h
    ../../inc/c/smd-pool/pool.h
    ../../inc/c/smd-pool/pool-lite.h
)

set(
    C_FILES
    vec2f.c
    vec3f.c
    vec4f.c
    mat2f.c
    mat3f.c
    mat4f.c
    quatf.c
    pool-lite.c
)

if(MSVC)
    add_library(${SMDCT_C_TARGET} STATIC ${H_FILES} ${C_FILES})
else()
    add_library(${SMDCT_C_TARGET} SHARED ${H_FILES} ${C_FILES})
endif()

target_include_directories(${SMDCT_C_TARGET} PUBLIC ../../inc/c)
if(MSVC)
else()
    target_link_libraries(${SMDCT_C_TARGET} m)
endif()
