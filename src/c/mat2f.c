//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/mat2f_t.h"

//==================================================================================================
void smdMat2f_identity( smdMat2f_t* pOut ) {
    smdVec2f_set( &pOut->r1, 1, 0 );
    smdVec2f_set( &pOut->r2, 0, 1 );
}

//==================================================================================================
void smdMat2f_set( smdMat2f_t* pOut, float m11, float m12, float m21, float m22 ) {
    smdVec2f_set( &pOut->r1, m11, m12 );
    smdVec2f_set( &pOut->r2, m21, m22 );
}

//==================================================================================================
bool smdMat2f_compare( const smdMat2f_t* cpIn1, const smdMat2f_t* cpIn2, float epsilon ) {
    if ( !smdVec2f_compare( &cpIn1->r1, &cpIn2->r1, epsilon ) ) return false;
    if ( !smdVec2f_compare( &cpIn1->r2, &cpIn2->r2, epsilon ) ) return false;

    return true;
}

//==================================================================================================
float smdMat2f_determinant( const smdMat2f_t* cpIn ) {
    return cpIn->r1.x * cpIn->r2.y - cpIn->r2.x * cpIn->r1.y;
}

//==================================================================================================
bool smdMat2f_inverse( smdMat2f_t* pOut, const smdMat2f_t* cpIn, float epsilon ) {
    const float cDet = smdMat2f_determinant( cpIn );
    if ( fabsf( cDet ) <= epsilon ) {
        return false;
    }
    smdMat2f_set( pOut, cpIn->r2.y / cDet, -cpIn->r1.y / cDet,
                       -cpIn->r2.x / cDet,  cpIn->r1.x / cDet );
    return true;
}

//==================================================================================================
void smdMat2f_transpose( smdMat2f_t* pOut, const smdMat2f_t* cpIn ) {
    const float cIn_r1_y = cpIn->r1.y;

    pOut->r1.x = cpIn->r1.x;
    pOut->r1.y = cpIn->r2.x;

    pOut->r2.x = cIn_r1_y;
    pOut->r2.y = cpIn->r2.y;
}

//==================================================================================================
void smdMat2f_multiplyVec( smdVec2f_t* pOut, const smdVec2f_t* cpVec, const smdMat2f_t* cpMat ) {
    smdMat2f_t transposed;
    smdMat2f_transpose( &transposed, cpMat );

    smdVec2f_set(
        pOut,
        smdVec2f_dot( cpVec, &transposed.r1 ),
        smdVec2f_dot( cpVec, &transposed.r2 )
    );
}

//==================================================================================================
void smdMat2f_multiply( smdMat2f_t* pOut, const smdMat2f_t* cpIn1, const smdMat2f_t* cpIn2 ) {
    smdMat2f_t transposed;
    smdMat2f_transpose( &transposed, cpIn2 );

    smdVec2f_set(
        &pOut->r1,
        smdVec2f_dot( &cpIn1->r1, &transposed.r1 ),
        smdVec2f_dot( &cpIn1->r1, &transposed.r2 )
    );
    smdVec2f_set(
        &pOut->r2,
        smdVec2f_dot( &cpIn1->r2, &transposed.r1 ),
        smdVec2f_dot( &cpIn1->r2, &transposed.r2 )
    );
}

//==================================================================================================
void smdMat2f_rotation( smdMat2f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );
    smdMat2f_set( pOut, cCos, cSin,
                       -cSin, cCos );
}

//==================================================================================================
bool smdMat2f_isSymmetric( const smdMat2f_t* cpIn, float epsilon ) {
    smdMat2f_t transposed;
    smdMat2f_transpose( &transposed, cpIn );

    if ( !smdMat2f_compare( &transposed, cpIn, epsilon ) ) return false;

    return true;
}

//==================================================================================================
bool smdMat2f_isOrthogonal( const smdMat2f_t* cpIn, float epsilon ) {
    smdMat2f_t identity;
    smdMat2f_identity( &identity );

    smdMat2f_t transposed;
    smdMat2f_transpose( &transposed, cpIn );

    smdMat2f_t mat1, mat2;
    smdMat2f_multiply( &mat1, cpIn, &transposed );
    smdMat2f_multiply( &mat2, &transposed, cpIn );

    // We must do three compares, because there is epsilon.
    if ( !smdMat2f_compare( &identity, &mat1, epsilon ) ) return false;
    if ( !smdMat2f_compare( &identity, &mat2, epsilon ) ) return false;
    if ( !smdMat2f_compare( &mat1, &mat2, epsilon ) )     return false;

    return true;
}
