//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/mat4f_t.h"

//==================================================================================================
void smdMat4f_identity( smdMat4f_t* pOut ) {
    smdVec4f_set( &pOut->r1, 1, 0, 0, 0 );
    smdVec4f_set( &pOut->r2, 0, 1, 0, 0 );
    smdVec4f_set( &pOut->r3, 0, 0, 1, 0 );
    smdVec4f_set( &pOut->r4, 0, 0, 0, 1 );
}

//==================================================================================================
void smdMat4f_set(
    smdMat4f_t* pOut,
    float m11, float m12, float m13, float m14,
    float m21, float m22, float m23, float m24,
    float m31, float m32, float m33, float m34,
    float m41, float m42, float m43, float m44
) {
    smdVec4f_set( &pOut->r1, m11, m12, m13, m14 );
    smdVec4f_set( &pOut->r2, m21, m22, m23, m24 );
    smdVec4f_set( &pOut->r3, m31, m32, m33, m34 );
    smdVec4f_set( &pOut->r4, m41, m42, m43, m44 );
}

//==================================================================================================
bool smdMat4f_compare( const smdMat4f_t* cpIn1, const smdMat4f_t* cpIn2, float epsilon ) {
    if ( !smdVec4f_compare( &cpIn1->r1, &cpIn2->r1, epsilon ) ) return false;
    if ( !smdVec4f_compare( &cpIn1->r2, &cpIn2->r2, epsilon ) ) return false;
    if ( !smdVec4f_compare( &cpIn1->r3, &cpIn2->r3, epsilon ) ) return false;
    if ( !smdVec4f_compare( &cpIn1->r4, &cpIn2->r4, epsilon ) ) return false;

    return true;
}

//==================================================================================================
void smdMat4f_translate( smdMat4f_t* pOut, const smdVec3f_t* cpTranslateAxis ) {
    smdMat4f_identity( pOut );
    pOut->r4.x = cpTranslateAxis->x;
    pOut->r4.y = cpTranslateAxis->y;
    pOut->r4.z = cpTranslateAxis->z;
}

//==================================================================================================
void smdMat4f_rotationX( smdMat4f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat4f_set(
        pOut,
        1, 0,    0,     0,
        0, cCos, -cSin, 0,
        0, cSin, cCos,  0,
        0, 0,    0,     1
    );
}

//==================================================================================================
void smdMat4f_rotationY( smdMat4f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat4f_set(
        pOut,
        cCos,  0, cSin, 0,
        0,     1, 0,    0,
        -cSin, 0, cCos, 0,
        0,     0, 0,    1
    );
}

//==================================================================================================
void smdMat4f_rotationZ( smdMat4f_t* pOut, float angle ) {
    const float cSin = sinf( angle );
    const float cCos = cosf( angle );

    smdMat4f_set(
        pOut,
        cCos, -cSin, 0, 0,
        cSin, cCos,  0, 0,
        0,    0,     1, 0,
        0,    0,     0, 1
    );
}

//==================================================================================================
void smdMat4fProperEulerAnglesZXZ( smdMat4f_t* pOut, float z1, float x2, float z3 ) {
    smdMat4f_t mat;

    smdMat4f_rotationZ( pOut, z1 );
    smdMat4f_rotationX( &mat, x2 );
    smdMat4f_multiply( pOut, pOut, &mat );

    smdMat4f_rotationZ( &mat, z3 );
    smdMat4f_multiply( pOut, pOut, &mat );
}

//==================================================================================================
void smdMat4fProperEulerAnglesZXZFast( smdMat4f_t* pOut, float z1, float x2, float z3 ) {
    const float s1 = sinf( z1 );
    const float s2 = sinf( x2 );
    const float s3 = sinf( z3 );

    const float c1 = cosf( z1 );
    const float c2 = cosf( x2 );
    const float c3 = cosf( z3 );

    pOut->r1.x = c1 * c3 - s1 * c2 * s3;
    pOut->r1.y = -c1 * s3 - s1 * c2 * c3;
    pOut->r1.z = s1 * s2;
    pOut->r1.w = 0;

    pOut->r2.x = s1 * c3 + c1 * c2 * s3;
    pOut->r2.y = -s1 * s3 + c1 * c2 * c3;
    pOut->r2.z = -c1 * s2;
    pOut->r2.w = 0;

    pOut->r3.x = s2 * s3;
    pOut->r3.y = s2 * c3;
    pOut->r3.z = c2;
    pOut->r3.w = 0;

    pOut->r4.x = 0;
    pOut->r4.y = 0;
    pOut->r4.z = 0;
    pOut->r4.w = 1;
}

//==================================================================================================
void smdMat4fTaitBryanAnglesZXY( smdMat4f_t* pOut, float z, float x, float y ) {
    smdMat4f_t mat;

    smdMat4f_rotationZ( pOut, z );
    smdMat4f_rotationX( &mat, x );
    smdMat4f_multiply( pOut, pOut, &mat );

    smdMat4f_rotationZ( &mat, y );
    smdMat4f_multiply( pOut, pOut, &mat );
}

//==================================================================================================
void smdMat4f_scale( smdMat4f_t* pOut, const smdVec3f_t* cpScaleAxis ) {
    memset( pOut, 0, sizeof( smdMat4f_t ) );
    pOut->r1.x = cpScaleAxis->x;
    pOut->r2.y = cpScaleAxis->y;
    pOut->r3.z = cpScaleAxis->z;
    pOut->r4.w = 1.0f;
}

//==================================================================================================
void smdMat4f_transpose( smdMat4f_t* pOut, const smdMat4f_t* cpIn ) {
    const float cIn_r1_y = cpIn->r1.y;
    const float cIn_r1_z = cpIn->r1.z;
    const float cIn_r1_w = cpIn->r1.w;

    const float cIn_r2_z = cpIn->r2.z;
    const float cIn_r2_w = cpIn->r2.w;

    const float cIn_r3_w = cpIn->r3.w;

    pOut->r1.x = cpIn->r1.x;
    pOut->r1.y = cpIn->r2.x;
    pOut->r1.z = cpIn->r3.x;
    pOut->r1.w = cpIn->r4.x;

    pOut->r2.x = cIn_r1_y;
    pOut->r2.y = cpIn->r2.y;
    pOut->r2.z = cpIn->r3.y;
    pOut->r2.w = cpIn->r4.y;

    pOut->r3.x = cIn_r1_z;
    pOut->r3.y = cIn_r2_z;
    pOut->r3.z = cpIn->r3.z;
    pOut->r3.w = cpIn->r4.z;

    pOut->r4.x = cIn_r1_w;
    pOut->r4.y = cIn_r2_w;
    pOut->r4.z = cIn_r3_w;
    pOut->r4.w = cpIn->r4.w;
}

//==================================================================================================
void smdMat4f_multiplyVec( smdVec4f_t* pOut, const smdVec4f_t* cpVec, const smdMat4f_t* cpMat ) {
    smdMat4f_t transposed;
    smdMat4f_transpose( &transposed, cpMat );

    smdVec4f_set(
        pOut,
        smdVec4f_dot( cpVec, &transposed.r1 ),
        smdVec4f_dot( cpVec, &transposed.r2 ),
        smdVec4f_dot( cpVec, &transposed.r3 ),
        smdVec4f_dot( cpVec, &transposed.r4 )
    );
}

//==================================================================================================
void smdMat4f_multiply( smdMat4f_t* pOut, const smdMat4f_t* cpIn1, const smdMat4f_t* cpIn2 ) {
    smdMat4f_t transposed;
    smdMat4f_transpose( &transposed, cpIn2 );

    smdVec4f_set(
        &pOut->r1,
        smdVec4f_dot( &cpIn1->r1, &transposed.r1 ),
        smdVec4f_dot( &cpIn1->r1, &transposed.r2 ),
        smdVec4f_dot( &cpIn1->r1, &transposed.r3 ),
        smdVec4f_dot( &cpIn1->r1, &transposed.r4 )
    );
    smdVec4f_set(
        &pOut->r2,
        smdVec4f_dot( &cpIn1->r2, &transposed.r1 ),
        smdVec4f_dot( &cpIn1->r2, &transposed.r2 ),
        smdVec4f_dot( &cpIn1->r2, &transposed.r3 ),
        smdVec4f_dot( &cpIn1->r2, &transposed.r4 )
    );
    smdVec4f_set(
        &pOut->r3,
        smdVec4f_dot( &cpIn1->r3, &transposed.r1 ),
        smdVec4f_dot( &cpIn1->r3, &transposed.r2 ),
        smdVec4f_dot( &cpIn1->r3, &transposed.r3 ),
        smdVec4f_dot( &cpIn1->r3, &transposed.r4 )
    );
    smdVec4f_set(
        &pOut->r4,
        smdVec4f_dot( &cpIn1->r4, &transposed.r1 ),
        smdVec4f_dot( &cpIn1->r4, &transposed.r2 ),
        smdVec4f_dot( &cpIn1->r4, &transposed.r3 ),
        smdVec4f_dot( &cpIn1->r4, &transposed.r4 )
    );
}

//==================================================================================================
void smdMat4f_minorMatrix( smdMat3f_t* pOut, const smdMat4f_t* cpIn, size_t row, size_t col ) {
    switch ( ( row << 4 ) + col ) {
    case 0x11:
        smdMat3f_set( pOut, cpIn->r2.y, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r3.y, cpIn->r3.z, cpIn->r3.w,
                            cpIn->r4.y, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x12:
        smdMat3f_set( pOut, cpIn->r2.x, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r3.x, cpIn->r3.z, cpIn->r3.w,
                            cpIn->r4.x, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x13:
        smdMat3f_set( pOut, cpIn->r2.x, cpIn->r2.y, cpIn->r2.w,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.w,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.w );
        break;
    case 0x14:
        smdMat3f_set( pOut, cpIn->r2.x, cpIn->r2.y, cpIn->r2.z,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.z,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.z );
        break;
    case 0x21:
        smdMat3f_set( pOut, cpIn->r1.y, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r3.y, cpIn->r3.z, cpIn->r3.w,
                            cpIn->r4.y, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x22:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r3.x, cpIn->r3.z, cpIn->r3.w,
                            cpIn->r4.x, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x23:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.w,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.w,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.w );
        break;
    case 0x24:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.z,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.z,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.z );
        break;
    case 0x31:
        smdMat3f_set( pOut, cpIn->r1.y, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r2.y, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r4.y, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x32:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r2.x, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r4.x, cpIn->r4.z, cpIn->r4.w );
        break;
    case 0x33:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.w,
                            cpIn->r2.x, cpIn->r2.y, cpIn->r2.w,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.w );
        break;
    case 0x34:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.z,
                            cpIn->r2.x, cpIn->r2.y, cpIn->r2.z,
                            cpIn->r4.x, cpIn->r4.y, cpIn->r4.z );
        break;
    case 0x41:
        smdMat3f_set( pOut, cpIn->r1.y, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r2.y, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r3.y, cpIn->r3.z, cpIn->r3.w );
        break;
    case 0x42:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.z, cpIn->r1.w,
                            cpIn->r2.x, cpIn->r2.z, cpIn->r2.w,
                            cpIn->r3.x, cpIn->r3.z, cpIn->r3.w );
        break;
    case 0x43:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.w,
                            cpIn->r2.x, cpIn->r2.y, cpIn->r2.w,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.w );
        break;
    case 0x44:
        smdMat3f_set( pOut, cpIn->r1.x, cpIn->r1.y, cpIn->r1.z,
                            cpIn->r2.x, cpIn->r2.y, cpIn->r2.z,
                            cpIn->r3.x, cpIn->r3.y, cpIn->r3.z );
        break;
    default:
        smdMat3f_identity( pOut );
    }
}

//==================================================================================================
void smdMat4f_determinant( float* pOut, const smdMat4f_t* cpIn ) {
    float      detMinor11, detMinor12, detMinor13, detMinor14;
    smdMat3f_t minor;

    smdMat4f_minorMatrix( &minor, cpIn, 0, 0 );
    detMinor11 = smdMat3f_determinant( &minor );

    smdMat4f_minorMatrix( &minor, cpIn, 0, 1 );
    detMinor12 = smdMat3f_determinant( &minor );

    smdMat4f_minorMatrix( &minor, cpIn, 0, 2 );
    detMinor13 = smdMat3f_determinant( &minor );

    smdMat4f_minorMatrix( &minor, cpIn, 0, 3 );
    detMinor14 = smdMat3f_determinant( &minor );

    *pOut = cpIn->r1.x * detMinor11 - cpIn->r1.y * detMinor12 + cpIn->r1.z * detMinor13 - cpIn->r1.w * detMinor14;
}

//==================================================================================================
bool smdMat4f_inverse( smdMat4f_t* pOut, const smdMat4f_t* cpIn, float epsilon ) {
    float      detMatInverse;
    smdMat3f_t minor;
    smdMat4f_t transposedMat;

    smdMat4f_determinant( &detMatInverse, cpIn );

    if ( fabsf( detMatInverse ) <= epsilon ) {
        return false;
    }

    detMatInverse = 1 / detMatInverse;
    smdMat4f_transpose( &transposedMat, cpIn );

    smdMat4f_minorMatrix( &minor, &transposedMat, 1, 1 ); pOut->r1.x =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 1, 2 ); pOut->r1.y = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 1, 3 ); pOut->r1.z =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 1, 4 ); pOut->r1.w = -smdMat3f_determinant( &minor ) * detMatInverse;

    smdMat4f_minorMatrix( &minor, &transposedMat, 2, 1 ); pOut->r2.x = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 2, 2 ); pOut->r2.y =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 2, 3 ); pOut->r2.z = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 2, 4 ); pOut->r2.w =  smdMat3f_determinant( &minor ) * detMatInverse;

    smdMat4f_minorMatrix( &minor, &transposedMat, 3, 1 ); pOut->r3.x =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 3, 2 ); pOut->r3.y = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 3, 3 ); pOut->r3.z =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 3, 4 ); pOut->r3.w = -smdMat3f_determinant( &minor ) * detMatInverse;

    smdMat4f_minorMatrix( &minor, &transposedMat, 4, 1 ); pOut->r4.x = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 4, 2 ); pOut->r4.y =  smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 4, 3 ); pOut->r4.z = -smdMat3f_determinant( &minor ) * detMatInverse;
    smdMat4f_minorMatrix( &minor, &transposedMat, 4, 4 ); pOut->r4.w =  smdMat3f_determinant( &minor ) * detMatInverse;

    return true;
}

/*
//==================================================================================================
void smd::inverseAlt( Matrix4f* pOut, const Matrix4f* cpMat ) noexcept {
    Matrix4f out;
    //const float* a = mat.f;
    //float* r = out.f;
    out.r1.x =  cpcMat->r2.y * cpcMat->r3.z * cpcMat->r4.w - a[5]*a[14]*a[11] - a[6]*a[9]*a[15] + a[6]*a[13]*a[11] + a[7]*a[9]*a[14] - a[7]*a[13]*a[10];
    out.r1.y = -cpcMat->r1.y * cpcMat->r3.z * cpcMat->r4.w + a[1]*a[14]*a[11] + a[2]*a[9]*a[15] - a[2]*a[13]*a[11] - a[3]*a[9]*a[14] + a[3]*a[13]*a[10];
    out.r1.z =  cpcMat->r1.y * cpcMat->r2.z * cpcMat->r4.w - a[1]*a[14]*a[7]  - a[2]*a[5]*a[15] + a[2]*a[13]*a[7]  + a[3]*a[5]*a[14] - a[3]*a[13]*a[6];
    out.r1.w = -cpcMat->r1.y * cpcMat->r2.z * cpcMat->r3.w + a[1]*a[10]*a[7]  + a[2]*a[5]*a[11] - a[2]*a[9]*a[7]   - a[3]*a[5]*a[10] + a[3]*a[9]*a[6];

    out.r2.x = -a[4]*a[10]*a[15] + a[4]*a[14]*a[11] + a[6]*a[8]*a[15] - a[6]*a[12]*a[11] - a[7]*a[8]*a[14] + a[7]*a[12]*a[10];
    out.r2.y =  a[0]*a[10]*a[15] - a[0]*a[14]*a[11] - a[2]*a[8]*a[15] + a[2]*a[12]*a[11] + a[3]*a[8]*a[14] - a[3]*a[12]*a[10];
    out.r2.z = -a[0]*a[6]*a[15]  + a[0]*a[14]*a[7]  + a[2]*a[4]*a[15] - a[2]*a[12]*a[7]  - a[3]*a[4]*a[14] + a[3]*a[12]*a[6];
    out.r2.w =  a[0]*a[6]*a[11]  - a[0]*a[10]*a[7]  - a[2]*a[4]*a[11] + a[2]*a[8]*a[7]   + a[3]*a[4]*a[10] - a[3]*a[8]*a[6];

    out.r3.x =  a[4]*a[9]*a[15]  - a[4]*a[13]*a[11] - a[5]*a[8]*a[15] + a[5]*a[12]*a[11] + a[7]*a[8]*a[13] - a[7]*a[12]*a[9];
    out.r3.y = -a[0]*a[9]*a[15]  + a[0]*a[13]*a[11] + a[1]*a[8]*a[15] - a[1]*a[12]*a[11] - a[3]*a[8]*a[13] + a[3]*a[12]*a[9];
    out.r3.z =  a[0]*a[5]*a[15]  - a[0]*a[13]*a[7]  - a[1]*a[4]*a[15] + a[1]*a[12]*a[7]  + a[3]*a[4]*a[13] - a[3]*a[12]*a[5];
    out.r3.w = -a[0]*a[5]*a[11]  + a[0]*a[9]*a[7]   + a[1]*a[4]*a[11] - a[1]*a[8]*a[7]   - a[3]*a[4]*a[9]  + a[3]*a[8]*a[5];

    out.r4.x = -a[4]*a[9]*a[14]  + a[4]*a[13]*a[10] + a[5]*a[8]*a[14] - a[5]*a[12]*a[10] - a[6]*a[8]*a[13] + a[6]*a[12]*a[9];
    out.r4.y =  a[0]*a[9]*a[14]  - a[0]*a[13]*a[10] - a[1]*a[8]*a[14] + a[1]*a[12]*a[10] + a[2]*a[8]*a[13] - a[2]*a[12]*a[9];
    out.r4.z = -a[0]*a[5]*a[14]  + a[0]*a[13]*a[6]  + a[1]*a[4]*a[14] - a[1]*a[12]*a[6]  - a[2]*a[4]*a[13] + a[2]*a[12]*a[5];
    out.r4.w =  a[0]*a[5]*a[10]  - a[0]*a[9]*a[6]   - a[1]*a[4]*a[10] + a[1]*a[8]*a[6]   + a[2]*a[4]*a[9]  - a[2]*a[8]*a[5];

    float det = a[0]*r[0] + a[4]*r[1] + a[8]*r[2] + a[12]*r[3];
    if( fabsf( det ) > 0.00001f ) {
        det = 1.0f / det;
    }
    for( int i = 0; i < 16; i++ ) {
        r[i] *= det;
    }
    return out;
}
*/

//==================================================================================================
bool smdMat4f_isSymmetric( const smdMat4f_t* cpIn, float epsilon ) {
    smdMat4f_t transposed;
    smdMat4f_transpose( &transposed, cpIn );

    if ( !smdMat4f_compare( &transposed, cpIn, epsilon ) ) return false;

    return true;
}

//==================================================================================================
bool smdMat4f_isOrthogonal( const smdMat4f_t* cpIn, float epsilon ) {
    smdMat4f_t identity;
    smdMat4f_identity( &identity );

    smdMat4f_t transposed;
    smdMat4f_transpose( &transposed, cpIn );

    smdMat4f_t mat1, mat2;
    smdMat4f_multiply( &mat1, cpIn, &transposed );
    smdMat4f_multiply( &mat2, &transposed, cpIn );

    // We must do three compares, because there is epsilon.
    if ( !smdMat4f_compare( &identity, &mat1, epsilon ) ) return false;
    if ( !smdMat4f_compare( &identity, &mat2, epsilon ) ) return false;
    if ( !smdMat4f_compare( &mat1, &mat2, epsilon ) )     return false;

    return true;
}
