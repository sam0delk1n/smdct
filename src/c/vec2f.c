//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/vec2f_t.h"

//==================================================================================================
void smdVec2f_set( smdVec2f_t* pOut, float x, float y ) {
    pOut->x = x;
    pOut->y = y;
}

//==================================================================================================
void smdVec2f_negate( smdVec2f_t* pOut, const smdVec2f_t* cpIn ) {
    pOut->x = -cpIn->x;
    pOut->y = -cpIn->y;
}

//==================================================================================================
void smdVec2f_add( smdVec2f_t* pOut, const smdVec2f_t* cpIn1, const smdVec2f_t* cpIn2 ) {
    pOut->x = cpIn1->x + cpIn2->x;
    pOut->y = cpIn1->y + cpIn2->y;
}

//==================================================================================================
void smdVec2f_subtract( smdVec2f_t* pOut, const smdVec2f_t* cpIn1, const smdVec2f_t* cpIn2 ) {
    pOut->x = cpIn1->x - cpIn2->x;
    pOut->y = cpIn1->y - cpIn2->y;
}

//==================================================================================================
void smdVec2f_scale( smdVec2f_t* pOut, const smdVec2f_t* cpIn, float scale ) {
    pOut->x = cpIn->x * scale;
    pOut->y = cpIn->y * scale;
}

//==================================================================================================
float smdVec2f_dot( const smdVec2f_t* cpIn1, const smdVec2f_t* cpIn2 ) {
    return cpIn1->x * cpIn2->x + cpIn1->y * cpIn2->y;
}

//==================================================================================================
bool smdVec2f_compare( const smdVec2f_t* cpIn1, const smdVec2f_t* cpIn2, float eps ) {
    if ( fabsf( cpIn1->x - cpIn2->x ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->y - cpIn2->y ) > eps ) {
        return false;
    }
    return true;
}

//==================================================================================================
float smdVec2f_length( const smdVec2f_t* cpIn ) {
    return sqrtf( smdVec2f_dot( cpIn, cpIn ) );
}

//==================================================================================================
void smdVec2f_normalize( smdVec2f_t* pOut, const smdVec2f_t* cpIn, float eps ) {
    float len = smdVec2f_length( cpIn );
    if ( len > eps ) {
        len = 1 / len;
        smdVec2f_scale( pOut, cpIn, len );
    } else {
        pOut->x = pOut->y = 0;
    }
}

//==================================================================================================
float smdVec2f_distance( const smdVec2f_t* cpIn1, const smdVec2f_t* cpIn2 ) {
    smdVec2f_t subtracted;
    smdVec2f_subtract( &subtracted, cpIn1, cpIn2 );
    return smdVec2f_length( &subtracted );
}

//==================================================================================================
void smdVec2f_clamp( smdVec2f_t* pOut, const smdVec2f_t* cpIn, float low, float high ) {
    *pOut = *cpIn;

    if ( pOut->x < low ) {
        pOut->x = low;
    } else if ( pOut->x > high ) {
        pOut->x = high;
    }

    if ( pOut->y < low ) {
        pOut->y = low;
    } else if ( pOut->y > high ) {
        pOut->y = high;
    }
}
