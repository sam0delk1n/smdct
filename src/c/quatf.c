//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../inc/c/smd-math/quatf_t.h"

//==================================================================================================
void smdQuatf_set( smdQuatf_t* pOut, float x, float y, float z, float w ) {
    pOut->x = x;
    pOut->y = y;
    pOut->z = z;
    pOut->w = w;
}

//==================================================================================================
void smdQuatf_fromAxisAngle( smdQuatf_t* pOut, const smdVec3f_t* cpAxis, float angle ) {
    const float cHalfAngle = 0.5f * angle;
    const float cHalfSin   = sinf( cHalfAngle );

    pOut->x = cpAxis->x * cHalfSin;
    pOut->y = cpAxis->y * cHalfSin;
    pOut->z = cpAxis->z * cHalfSin;
    pOut->w = cosf( cHalfAngle );
}

//==================================================================================================
void smdQuatf_fromYawPitchRoll( smdQuatf_t* pOut, const smdVec3f_t* cpYpr ) {
    const float cHalfYaw   = cpYpr->x * 0.5f;
    const float cHalfPitch = cpYpr->y * 0.5f;
    const float cHalfRoll  = cpYpr->z * 0.5f;

    const float cCosYaw   = cosf( cHalfYaw );
    const float cSinYaw   = sinf( cHalfYaw );
    const float cCosPitch = cosf( cHalfPitch );
    const float cSinPitch = sinf( cHalfPitch );
    const float cCosRoll  = cosf( cHalfRoll );
    const float cSinRoll  = sinf( cHalfRoll );

    pOut->x = cCosRoll * cSinPitch * cCosYaw + cSinRoll * cCosPitch * cSinYaw;
    pOut->y = cCosRoll * cCosPitch * cSinYaw - cSinRoll * cSinPitch * cCosYaw;
    pOut->z = cSinRoll * cCosPitch * cCosYaw - cCosRoll * cSinPitch * cSinYaw;
    pOut->w = cCosRoll * cCosPitch * cCosYaw + cSinRoll * cSinPitch * cSinYaw;
}

//==================================================================================================
void smdQuatf_add( smdQuatf_t* pOut, const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 ) {
    pOut->x = cpIn1->x + cpIn2->x;
    pOut->y = cpIn1->y + cpIn2->y;
    pOut->z = cpIn1->z + cpIn2->z;
    pOut->w = cpIn1->w + cpIn2->w;
}

//==================================================================================================
void smdQuatf_scale( smdQuatf_t* pOut, const smdQuatf_t* cpIn, float scale ) {
    pOut->x = cpIn->x * scale;
    pOut->y = cpIn->y * scale;
    pOut->z = cpIn->z * scale;
    pOut->w = cpIn->w * scale;
}

//==================================================================================================
float smdQuatf_dot( const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 ) {
    return cpIn1->x * cpIn2->x + cpIn1->y * cpIn2->y + cpIn1->z * cpIn2->z + cpIn1->w * cpIn2->w;
}

//==================================================================================================
bool smdQuatf_compare( const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2, float eps ) {
    if ( fabsf( cpIn1->x - cpIn2->x ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->y - cpIn2->y ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->z - cpIn2->z ) > eps ) {
        return false;
    }
    if ( fabsf( cpIn1->w - cpIn2->w ) > eps ) {
        return false;
    }
    return true;
}

//==================================================================================================
float smdQuatf_norm( const smdQuatf_t* cpIn ) {
    return cpIn->x * cpIn->x + cpIn->y * cpIn->y + cpIn->z * cpIn->z + cpIn->w * cpIn->w;
}

//==================================================================================================
float smdQuatf_magnitude( const smdQuatf_t* cpIn ) {
    return sqrtf( smdQuatf_norm( cpIn ) );
}

//==================================================================================================
void smdQuatf_normalize( smdQuatf_t* pOut, const smdQuatf_t* cpIn, float eps ) {
    float len = smdQuatf_magnitude( cpIn );

    if ( len > eps ) {
        len = 1 / len;
        smdQuatf_scale( pOut, cpIn, len );
    } else {
        pOut->x = pOut->y = pOut->z = pOut->w = 0;
    }
}

//==================================================================================================
void smdQuatf_multiply( smdQuatf_t* pOut, const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 ) {
    smdVec3f_t vec0, vec1, vec2;

    smdVec3f_set( &vec1, cpIn1->x, cpIn1->y, cpIn1->z );
    smdVec3f_set( &vec2, cpIn2->x, cpIn2->y, cpIn2->z );

    const float cDot = smdVec3f_dot( &vec1, &vec2 );
    smdVec3f_cross( &vec0, &vec1, &vec2 );
    smdVec3f_scale( &vec1, &vec1, cpIn2->w );
    smdVec3f_scale( &vec2, &vec2, cpIn1->w );
    smdVec3f_add( &vec0, &vec0, &vec1 );
    smdVec3f_add( &vec0, &vec0, &vec2 );

    pOut->x = vec0.x;
    pOut->y = vec0.y;
    pOut->z = vec0.z;
    pOut->w = cpIn1->w * cpIn2->w - cDot;
}

//==================================================================================================
static void smdQuatf_toMatT( smdMat3f_t* pOut, const smdQuatf_t* cpIn, float s ) {
    const float x2 = cpIn->x * s, y2 = cpIn->y * s, z2 = cpIn->z * s;

    const float xx = cpIn->x * x2, xy = cpIn->x * y2, xz = cpIn->x * z2;
    const float yy = cpIn->y * y2, yz = cpIn->y * z2, zz = cpIn->z * z2;
    const float wx = cpIn->w * x2, wy = cpIn->w * y2, wz = cpIn->w * z2;

    smdMat3f_set( pOut, 1 - ( yy + zz ), xy + wz,         xz - wy,
                        xy - wz,         1 - ( xx + zz ), yz + wx,
                        xz + wy,         yz - wx,         1 - ( xx + yy ) );
}

//==================================================================================================
void smdQuatf_unitToMat3f( smdMat3f_t* pOut, const smdQuatf_t* cpIn ) {
    smdQuatf_toMatT( pOut, cpIn, 2 );
}

//==================================================================================================
void smdQuatf_unitToMat4f( smdMat4f_t* pOut, const smdQuatf_t* cpIn ) {
    smdMat3f_t mat;

    smdQuatf_toMatT( &mat, cpIn, 2 );

    smdMat4f_set( pOut, mat.r1.x, mat.r1.y, mat.r1.z, 0,
                        mat.r2.x, mat.r2.y, mat.r2.z, 0,
                        mat.r3.x, mat.r3.y, mat.r3.z, 0,
                        0,        0,        0,        1 );
}

//==================================================================================================
void smdQuatf_toMat3f( smdMat3f_t* pOut, const smdQuatf_t* cpIn ) {
    smdQuatf_toMatT( pOut, cpIn, 2 / smdQuatf_norm( cpIn ) );
}

//==================================================================================================
void smdQuatf_toMat4f( smdMat4f_t* pOut, const smdQuatf_t* cpIn ) {
    smdMat3f_t mat;

    smdQuatf_toMatT( &mat, cpIn, 2 / smdQuatf_norm( cpIn ) );

    smdMat4f_set( pOut, mat.r1.x, mat.r1.y, mat.r1.z, 0,
                        mat.r2.x, mat.r2.y, mat.r2.z, 0,
                        mat.r3.x, mat.r3.y, mat.r3.z, 0,
                        0,        0,        0,        1 );
}

//==================================================================================================
static void smdQuatf_unitFromMatT( smdQuatf_t* pOut, const smdVec3f_t* cpRow1,
                                                     const smdVec3f_t* cpRow2,
                                                     const smdVec3f_t* cpRow3 ) {
    const float tr = cpRow1->x + cpRow2->y + cpRow3->z;

    if ( tr > 0 ) {
        smdQuatf_set( pOut, cpRow2->z - cpRow3->y, cpRow3->x - cpRow1->z,
                            cpRow1->y - cpRow2->x, tr + 1 );
        smdQuatf_scale( pOut, pOut, 0.5f / sqrtf( pOut->w ) );

    } else if ( cpRow1->x > cpRow2->y && cpRow1->x > cpRow3->z ) {
        smdQuatf_set( pOut, 1 + cpRow1->x - cpRow2->y - cpRow3->z, cpRow2->x + cpRow1->y,
                            cpRow3->x + cpRow1->z,                 cpRow2->z - cpRow3->y );
        smdQuatf_scale( pOut, pOut, 0.5f / sqrtf( pOut->x ) );

    } else if ( cpRow2->y > cpRow3->z ) {
        smdQuatf_set( pOut, cpRow2->x + cpRow1->y, 1 + cpRow2->y - cpRow1->x - cpRow3->z,
                            cpRow3->y + cpRow2->z, cpRow3->x - cpRow1->z );
        smdQuatf_scale( pOut, pOut, 0.5f / sqrtf( pOut->y ) );

    } else {
        smdQuatf_set( pOut, cpRow3->x + cpRow1->z,                 cpRow3->y + cpRow2->z,
                            1 + cpRow3->z - cpRow1->x - cpRow2->y, cpRow1->y - cpRow2->x );
        smdQuatf_scale( pOut, pOut, 0.5f / sqrtf( pOut->z ) );
    }
}

//==================================================================================================
void smdQuatf_unitFromMat3f( smdQuatf_t* pOut, const smdMat3f_t* cpIn ) {
    smdQuatf_unitFromMatT( pOut, &cpIn->r1, &cpIn->r2, &cpIn->r3 );
}

//==================================================================================================
void smdQuatf_unitFromMat4f( smdQuatf_t* pOut, const smdMat4f_t* cpIn ) {
    smdQuatf_unitFromMatT( pOut, ( const smdVec3f_t* )&cpIn->r1,
                                 ( const smdVec3f_t* )&cpIn->r2,
                                 ( const smdVec3f_t* )&cpIn->r3 );
}

//==================================================================================================
static void smdQuatf_fromMatT( smdQuatf_t* pOut, const smdVec3f_t* cpRow1,
                                                 const smdVec3f_t* cpRow2,
                                                 const smdVec3f_t* cpRow3 ) {
    const float tr = cpRow1->x + cpRow2->y + cpRow3->z;

    if ( tr > 0 ) {
        smdQuatf_set( pOut, cpRow2->z - cpRow3->y, cpRow3->x - cpRow1->z,
                            cpRow1->y - cpRow2->x, tr + 1 );

    } else if ( cpRow1->x > cpRow2->y && cpRow1->x > cpRow3->z ) {
        smdQuatf_set( pOut, 1 + cpRow1->x - cpRow2->y - cpRow3->z, cpRow2->x + cpRow1->y,
                            cpRow3->x + cpRow1->z,                 cpRow2->z - cpRow3->y );

    } else if ( cpRow2->y > cpRow3->z ) {
        smdQuatf_set( pOut, cpRow2->x + cpRow1->y, 1 + cpRow2->y - cpRow1->x - cpRow3->z,
                            cpRow3->y + cpRow2->z, cpRow3->x - cpRow1->z );

    } else {
        smdQuatf_set( pOut, cpRow3->x + cpRow1->z,                 cpRow3->y + cpRow2->z,
                            1 + cpRow3->z - cpRow1->x - cpRow2->y, cpRow1->y - cpRow2->x );
    }
}

//==================================================================================================
void smdQuatf_fromMat3f( smdQuatf_t* pOut, const smdMat3f_t* cpIn ) {
    smdQuatf_fromMatT( pOut, &cpIn->r1, &cpIn->r2, &cpIn->r3 );
}

//==================================================================================================
void smdQuatf_fromMat4f( smdQuatf_t* pOut, const smdMat4f_t* cpIn ) {
    smdQuatf_fromMatT( pOut, ( const smdVec3f_t* )&cpIn->r1,
                             ( const smdVec3f_t* )&cpIn->r2,
                             ( const smdVec3f_t* )&cpIn->r3 );
}
