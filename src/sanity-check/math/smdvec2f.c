//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../../inc/c/smdCT.h"

#include <stdbool.h>
#include <stdio.h>

//==============================================================================================================================
static bool check_smdVec2f_set( void ) {
    smdVec2f_t out;

    // Sanity check.
    smdVec2f_set( &out, 1, 2 );

    if ( !( 1 == out.x && 2 == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_negate( void ) {
    smdVec2f_t out, in;

    // Sanity check.
    smdVec2f_set( &in, 1, 2 );

    smdVec2f_negate( &out, &in );

    if ( !( -1 == out.x && -2 == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 1, 2 );

    smdVec2f_negate( &out, &out );

    if ( !( -1 == out.x && -2 == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_add( void ) {
    smdVec2f_t out, in1, in2;

    // Sanity check.
    smdVec2f_set( &in1, 1, 2 );
    smdVec2f_set( &in2, 3, 4 );

    smdVec2f_add( &out, &in1, &in2 );

    if ( !( 4 == out.x && 6 == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 1, 2 );

    smdVec2f_add( &out, &out, &out );

    if ( !( 2 == out.x && 4 == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_subtract( void ) {
    smdVec2f_t out, in1, in2;

    // Sanity check.
    smdVec2f_set( &in1, 1, 2 );
    smdVec2f_set( &in2, 3, 5 );

    smdVec2f_subtract( &out, &in1, &in2 );

    if ( !( -2 == out.x && -3 == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 1, 2 );

    smdVec2f_subtract( &out, &out, &out );

    if ( !( 0 == out.x && 0 == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_scale( void ) {
    smdVec2f_t out, in;

    // Sanity check.
    smdVec2f_set( &in, 1, 2 );

    smdVec2f_scale( &out, &in, 0.5f );

    if ( !( 0.5f == out.x && 1 == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 1, 2 );

    smdVec2f_scale( &out, &out, 0.5f );

    if ( !( 0.5f == out.x && 1 == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_dot( void ) {
    smdVec2f_t in1, in2;

    // Sanity check.
    smdVec2f_set( &in1, 1, 2 );
    smdVec2f_set( &in2, 3, 4 );

    if ( !( 11 == smdVec2f_dot( &in1, &in2 ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_compare( void ) {
    smdVec2f_t in1, in2;

    // Sanity check.
    smdVec2f_set( &in1, 1, 2 );
    smdVec2f_set( &in2, 1, 2 );

    if ( !smdVec2f_compare( &in1, &in2, 0 ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_length( void ) {
    smdVec2f_t in;

    // Sanity check.
    smdVec2f_set( &in, 3, 4 );

    if ( !( 5 == smdVec2f_length( &in ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_normalize( void ) {
    smdVec2f_t out, in;

    // Sanity check.
    smdVec2f_set( &in, 3, 4 );

    smdVec2f_normalize( &out, &in, 0 );

    if ( !( 0.6f == out.x && 0.8f == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 3, 4 );

    smdVec2f_normalize( &out, &out, 0 );

    if ( !( 0.6f == out.x && 0.8f == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_distance( void ) {
    smdVec2f_t in1, in2;

    // Sanity check.
    smdVec2f_set( &in1, 1, 2 );
    smdVec2f_set( &in2, 4, 6 );

    if ( !( 5 == smdVec2f_distance( &in1, &in2 ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec2f_clamp( void ) {
    smdVec2f_t out, in;

    // Sanity check.
    smdVec2f_set( &in, 0.1f, 1 );

    smdVec2f_clamp( &out, &in, 0.25f, 0.5f );

    if ( !( 0.25f == out.x && 0.5f == out.y ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &out, 0.1f, 1 );

    smdVec2f_clamp( &out, &out, 0.25f, 0.5f );

    if ( !( 0.25f == out.x && 0.5f == out.y ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
void test_smdVec2f( void ) {
    #define TEST( x ) if ( x() ) { printf( #x": passed.\n" ); } else { printf( #x": failed.\n" ); }

    TEST( check_smdVec2f_set );
    TEST( check_smdVec2f_negate );
    TEST( check_smdVec2f_add );
    TEST( check_smdVec2f_subtract );
    TEST( check_smdVec2f_scale );
    TEST( check_smdVec2f_dot );
    TEST( check_smdVec2f_compare );
    TEST( check_smdVec2f_length );
    TEST( check_smdVec2f_normalize );
    TEST( check_smdVec2f_distance );
    TEST( check_smdVec2f_clamp );

    // TODO TEST( check_smdVec2fArray_negate );
    // TODO TEST( check_smdVec2fArray_add );
    // TODO TEST( check_smdVec2fArray_subtract );
    // TODO TEST( check_smdVec2fArray_scale );
    // TODO TEST( check_smdVec2fArray_dot );
    // TODO TEST( check_smdVec2fArray_compare );
    // TODO TEST( check_smdVec2fArray_length );
    // TODO TEST( check_smdVec2fArray_normalize );
    // TODO TEST( check_smdVec2fArray_distance );
    // TODO TEST( check_smdVec2fArray_clamp );
}
