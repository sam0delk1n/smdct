//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../../inc/c/smdCT.h"
#include <stdio.h>

//==================================================================================================
static bool check_smdMat2f_identity() {
    smdMat2f_t out;
    smdMat2f_t correct;

    // Sanity check.
    smdMat2f_set( &correct, 1, 0,
                            0, 1 );

    smdMat2f_identity( &out );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_set() {
    smdMat2f_t out;

    // Sanity check.
    smdMat2f_set( &out, 1, 2,
                        3, 4 );

    if ( !( 1 == out.r1.x && 2 == out.r1.y &&
            3 == out.r2.x && 4 == out.r2.y ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_compare() {
    smdMat2f_t in1;
    smdMat2f_t in2;

    // Sanity check.
    smdMat2f_set( &in1, 1, 2,
                        3, 4 );
    smdMat2f_set( &in2, 1, 2,
                        3, 4 );

    if ( !smdMat2f_compare( &in1, &in2, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_determinant() {
    smdMat2f_t in;

    // Sanity check.
    smdMat2f_set( &in, 1, 2,
                       3, 4 );

    if ( !( -2 == smdMat2f_determinant( &in ) ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_inverse() {
    smdMat2f_t out;
    smdMat2f_t in;
    smdMat2f_t correct;

    // Sanity check.
    smdMat2f_set( &in, 1, 2,
                       3, 4 );
    smdMat2f_set( &correct, -2,     1,
                             1.5f, -0.5f );

    smdMat2f_inverse( &out, &in, ( float )SMDCT_INVERSEMATEPSILON );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    // Cross pointers check.
    smdMat2f_set( &out, 1, 2,
                        3, 4 );

    smdMat2f_inverse( &out, &out, ( float )SMDCT_INVERSEMATEPSILON );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_transpose() {
    smdMat2f_t out;
    smdMat2f_t in;
    smdMat2f_t correct;

    // Sanity check.
    smdMat2f_set( &in,      1, 2,
                            3, 4 );
    smdMat2f_set( &correct, 1, 3,
                            2, 4 );

    smdMat2f_transpose( &out, &in );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    // Cross pointers check.
    smdMat2f_set( &out, 1, 2,
                        3, 4 );

    smdMat2f_transpose( &out, &out );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_multiplyVec() {
    smdMat2f_t in;

    smdVec2f_t vecOut;
    smdVec2f_t vecIn1;
    smdVec2f_t vecCorrect;

    // Sanity check.
    smdVec2f_set( &vecIn1,     1, 2  );
    smdVec2f_set( &vecCorrect, 7, 10 );

    smdMat2f_set( &in, 1, 2,
                       3, 4 );

    smdMat2f_multiplyVec( &vecOut, &vecIn1, &in );

    if ( !smdVec2f_compare( &vecCorrect, &vecOut, 0 ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec2f_set( &vecOut, 1, 2 );

    smdMat2f_multiplyVec( &vecOut, &vecOut, &in );

    if ( !smdVec2f_compare( &vecCorrect, &vecOut, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_multiply() {
    smdMat2f_t out;
    smdMat2f_t in1;
    smdMat2f_t in2;
    smdMat2f_t correct;

    // Sanity check.
    smdMat2f_set( &in1,     1, 2,
                            3, 4 );
    smdMat2f_set( &in2,     5, 6,
                            7, 8 );
    smdMat2f_set( &correct, 19, 22,
                            43, 50 );

    smdMat2f_multiply( &out, &in1, &in2 );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    // Cross pointers check.
    smdMat2f_set( &out, 1, 2,
                        3, 4 );
    smdMat2f_set( &correct,  7, 10,
                            15, 22 );

    smdMat2f_multiply( &out, &out, &out );

    if ( !smdMat2f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat2f_rotation() {
    // Sanity check.
    {
        smdMat2f_t out;
        smdMat2f_t correct;

        // angle=0
        smdMat2f_set( &correct, 1, 0,
                                0, 1 );

        smdMat2f_rotation( &out, 0 );

        if ( !smdMat2f_compare( &correct, &out, ( float )SMDCT_EPSILON ) ) {
            return false;
        }

        // angle=PI
        smdMat2f_set( &correct, -1,  0,
                                 0, -1 );

        smdMat2f_rotation( &out, ( float )SMDCT_PI );

        if ( !smdMat2f_compare( &correct, &out, ( float )SMDCT_EPSILON ) ) {
            return false;
        }

        // angle=PI/2
        smdMat2f_set( &correct, 0, 1,
                               -1, 0 );

        smdMat2f_rotation( &out, ( float )SMDCT_PI_2 );

        if ( !smdMat2f_compare( &correct, &out, ( float )SMDCT_EPSILON ) ) {
            return false;
        }
    }

    return true;
}

//==================================================================================================
void test_smdMat2f() {
    #define TEST( x ) if ( x() ) { printf( #x": passed.\n" ); } else { printf( #x": failed.\n" ); }

    TEST( check_smdMat2f_set );
    TEST( check_smdMat2f_compare );
    TEST( check_smdMat2f_identity );
    TEST( check_smdMat2f_determinant );
    TEST( check_smdMat2f_inverse );
    TEST( check_smdMat2f_transpose );
    TEST( check_smdMat2f_multiplyVec );
    TEST( check_smdMat2f_multiply );
    TEST( check_smdMat2f_rotation );
}
