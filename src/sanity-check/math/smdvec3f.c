//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../../inc/c/smdCT.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

//==============================================================================================================================
static bool check_smdVec3f_set( void ) {
    smdVec3f_t out;

    // Sanity check.
    smdVec3f_set( &out, 1, 2, 3 );

    if ( !( 1 == out.x && 2 == out.y && 3 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_negate( void ) {
    smdVec3f_t out, in;

    // Sanity check.
    smdVec3f_set( &in, 1, 2, 3 );

    smdVec3f_negate( &out, &in );

    if ( !( -1 == out.x && -2 == out.y && -3 == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_negate( &out, &out );

    if ( !( -1 == out.x && -2 == out.y && -3 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_add( void ) {
    smdVec3f_t out, in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 4, 5, 6 );

    smdVec3f_add( &out, &in1, &in2 );

    if ( !( 5 == out.x && 7 == out.y && 9 == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_add( &out, &out, &out );

    if ( !( 2 == out.x && 4 == out.y && 6 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_subtract( void ) {
    smdVec3f_t out, in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 4, 6, 8 );

    smdVec3f_subtract( &out, &in1, &in2 );

    if ( !( -3 == out.x && -4 == out.y && -5 == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_subtract( &out, &out, &out );

    if ( !( 0 == out.x && 0 == out.y && 0 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_scale( void ) {
    smdVec3f_t out, in;

    // Sanity check.
    smdVec3f_set( &in, 1, 2, 3 );

    smdVec3f_scale( &out, &in, 0.5f );

    if ( !( 0.5f == out.x && 1 == out.y && 1.5f == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_scale( &out, &out, 0.5f );

    if ( !( 0.5f == out.x && 1 == out.y && 1.5f == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_dot( void ) {
    smdVec3f_t in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 4, 5, 6 );

    if ( !( 32 == smdVec3f_dot( &in1, &in2 ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_compare( void ) {
    smdVec3f_t in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 1, 2, 3 );

    if ( !smdVec3f_compare( &in1, &in2, 0 ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_length( void ) {
    smdVec3f_t in;

    // Sanity check.
    smdVec3f_set( &in, 3, 4, 12 );

    if ( !( 13 == smdVec3f_length( &in ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_normalize( void ) {
    smdVec3f_t correct, out, in;

    // Sanity check.
    smdVec3f_set( &correct, 3 / sqrtf( 50 ), 4 / sqrtf( 50 ), 5 / sqrtf( 50 ) ); // TODO Find more simple numbers.
    smdVec3f_set( &in, 3, 4, 5 );

    smdVec3f_normalize( &out, &in, 0 );

    if ( !smdVec3f_compare( &correct, &out, ( float )SMDCT_EPSILON ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 3, 4, 5 );

    smdVec3f_normalize( &out, &out, 0 );

    if ( !smdVec3f_compare( &correct, &out, ( float )SMDCT_EPSILON ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_distance( void ) {
    smdVec3f_t in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 4, 6, 15 );

    if ( !( 13 == smdVec3f_distance( &in1, &in2 ) ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_toDword( void ) {
    uint32_t   out;
    smdVec3f_t in;

    // Sanity check.
    smdVec3f_set( &in, 0, 0.5f, 1 );

    smdVec3f_toDword( &out, &in );

    if ( !( 0x00007fff == out ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_fromDword( void ) {
    smdVec3f_t out;

    // Sanity check.
    smdVec3f_fromDword( &out, 0x000033ff );

    if ( !( 0 == out.x && 0.2f == out.y && 1 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_clamp( void ) {
    smdVec3f_t out, in;

    // Sanity check.
    smdVec3f_set( &in, 0.1f, 0.3f, 1 );

    smdVec3f_clamp( &out, &in, 0.25f, 0.5f );

    if ( !( 0.25f == out.x && 0.3f == out.y && 0.5f == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, 0.1f, 0.3f, 1 );

    smdVec3f_clamp( &out, &out, 0.25f, 0.5f );

    if ( !( 0.25f == out.x && 0.3f == out.y && 0.5f == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_saturate( void ) {
    smdVec3f_t out, in;

    // Sanity check.
    smdVec3f_set( &in, -1, 0.5f, 2 );

    smdVec3f_saturate( &out, &in );

    if ( !( 0 == out.x && 0.5f == out.y && 1 == out.z ) ) {
        return false;
    }

    // Cross pointers check.
    smdVec3f_set( &out, -1, 0.5f, 2 );

    smdVec3f_saturate( &out, &out );

    if ( !( 0 == out.x && 0.5f == out.y && 1 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
static bool check_smdVec3f_cross( void ) {
    smdVec3f_t out, in1, in2;

    // Sanity check.
    smdVec3f_set( &in1, 1, 2, 3 );
    smdVec3f_set( &in2, 4, 5, 6 );

    smdVec3f_cross( &out, &in1, &in2 );

    if ( !( -3 == out.x && 6 == out.y && -3 == out.z ) ) {
        return false;
    }

    // Cross pointers check [1/3].
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_cross( &out, &out, &in2 );

    if ( !( -3 == out.x && 6 == out.y && -3 == out.z ) ) {
        return false;
    }

    // Cross pointers check [2/3].
    smdVec3f_set( &out, 4, 5, 6 );

    smdVec3f_cross( &out, &in1, &out );

    if ( !( -3 == out.x && 6 == out.y && -3 == out.z ) ) {
        return false;
    }

    // Cross pointers check [3/3].
    smdVec3f_set( &out, 1, 2, 3 );

    smdVec3f_cross( &out, &out, &out );

    if ( !( 0 == out.x && 0 == out.y && 0 == out.z ) ) {
        return false;
    }

    return true;
}

//==============================================================================================================================
void test_smdVec3f( void ) {
    #define TEST( x ) if ( x() ) { printf( #x": passed.\n" ); } else { printf( #x": failed.\n" ); }

    TEST( check_smdVec3f_set );
    TEST( check_smdVec3f_negate );
    TEST( check_smdVec3f_add );
    TEST( check_smdVec3f_subtract );
    TEST( check_smdVec3f_scale );
    TEST( check_smdVec3f_dot );
    TEST( check_smdVec3f_compare );
    TEST( check_smdVec3f_length );
    TEST( check_smdVec3f_normalize );
    TEST( check_smdVec3f_distance );
    TEST( check_smdVec3f_toDword );   // TODO Add 'toDwordARGB' and 'toDwordRGBA'.
    TEST( check_smdVec3f_fromDword ); // TODO Add 'fromDwordARGB' and 'fromDwordRGBA'.
    TEST( check_smdVec3f_clamp );
    TEST( check_smdVec3f_saturate );
    TEST( check_smdVec3f_cross );

    // TODO TEST( check_smdVec3fArray_negate );
    // TODO TEST( check_smdVec3fArray_add );
    // TODO TEST( check_smdVec3fArray_subtract );
    // TODO TEST( check_smdVec3fArray_scale );
    // TODO TEST( check_smdVec3fArray_dot );
    // TODO TEST( check_smdVec3fArray_compare );
    // TODO TEST( check_smdVec3fArray_length );
    // TODO TEST( check_smdVec3fArray_normalize );
    // TODO TEST( check_smdVec3fArray_distance );
    // TODO TEST( check_smdVec3fArray_toDword );
    // TODO TEST( check_smdVec3fArray_fromDword );
    // TODO TEST( check_smdVec3fArray_clamp );
    // TODO TEST( check_smdVec3fArray_saturate );
    // TODO TEST( check_smdVec3fArray_cross );
}
