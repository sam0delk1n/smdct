//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#include "../../../inc/c/smdCT.h"
#include <stdio.h>

//==================================================================================================
static bool check_smdMat3f_set() {
    smdMat3f_t out;

    // Sanity check.
    smdMat3f_set(
        &out,
        1, 2, 3,
        4, 5, 6,
        7, 8, 9
    );

    if ( !(
        1 == out.r1.x && 2 == out.r1.y && 3 == out.r1.z &&
        4 == out.r2.x && 5 == out.r2.y && 6 == out.r2.z &&
        7 == out.r3.x && 8 == out.r3.y && 9 == out.r3.z
    ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat3f_compare() {
    smdMat3f_t in1;
    smdMat3f_t in2;

    // Sanity check.
    smdMat3f_set(
        &in1,
        1, 2, 3,
        4, 5, 6,
        7, 8, 9
    );
    smdMat3f_set(
        &in2,
        1, 2, 3,
        4, 5, 6,
        7, 8, 9
    );

    if ( !smdMat3f_compare( &in1, &in2, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
static bool check_smdMat3f_identity() {
    smdMat3f_t out;
    smdMat3f_t correct;

    // Sanity check.
    smdMat3f_set(
        &correct,
        1, 0, 0,
        0, 1, 0,
        0, 0, 1
    );

    smdMat3f_identity( &out );

    if ( !smdMat3f_compare( &correct, &out, 0 ) ) {
        return false;
    }

    return true;
}

/*
//==================================================================================================
static bool check_smdMat3fProperEulerAnglesZXZ() {
}

//==================================================================================================
static bool check_smdMat3fProperEulerAnglesZXZFast() {
}
*/

//==================================================================================================
static bool check_smdMat3fProperEulerAnglesZXZTest() {
    smdMat3f_t out1, out2;

    smdMat3fProperEulerAnglesZXZ( &out1, 0.5, 1, 1.5 );
    smdMat3fProperEulerAnglesZXZFast( &out2, 0.5, 1, 1.5 );

    if ( !smdMat3f_compare( &out1, &out2, 0 ) ) {
        return false;
    }

    return true;
}

//==================================================================================================
void test_smdMat3f() {
    #define TEST( x ) if ( x() ) { printf( #x": passed.\n" ); } else { printf( #x": failed.\n" ); }

    TEST( check_smdMat3f_set );
    TEST( check_smdMat3f_compare );
    TEST( check_smdMat3f_identity );
    //TEST( check_smdMat3f_determinant );
    //TEST( check_smdMat3f_inverse );
    //TEST( check_smdMat3f_transpose );
    //TEST( check_smdMat3f_multiplyVec );
    //TEST( check_smdMat3f_multiply );
    //TEST( check_smdMat3f_rotation );
    TEST( check_smdMat3fProperEulerAnglesZXZTest );
}
