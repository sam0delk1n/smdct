//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

/**
@file
@brief Sanity check for mr.DIMAS's pool.h/pool.c.
*/

#define SMDCT_IMPLEMENTATION

#include "../../inc/c/smdCT.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// MinGW doesn't support the printf %llu and %zu, so define sz_t as unsigned long and use it instead size_t in the printf args.
// e. g. { size_t size; printf( "%lu", ( sz_t )size ); }. Of course unsigned long is only a 32 bit type -- be careful.
typedef unsigned long sz_t;

typedef int   ( *poolInit_t )( void* p, size_t capacity, size_t object_size );
typedef void  ( *poolClear_t )( void* p );
typedef ref_t ( *poolSpawn_t )( void* p );
typedef void  ( *poolReturn_t )( void* p, ref_t h );
typedef int   ( *poolIsRef_t )( void* p, ref_t h );
typedef void* ( *poolGet_t )( void* p, ref_t h );
typedef void* ( *poolGetByIndex_t )( void* p, size_t i );

typedef struct pool_i {
    poolInit_t       init;
    poolClear_t      clear;
    poolSpawn_t      spawn;
    poolReturn_t     release;
    poolIsRef_t      isRef;
    poolGet_t        get;
    poolGetByIndex_t getByIndex;
} pool_i;

//==============================================================================================================================
static pool_i getPoolIface( void ) {
    pool_i iface;

    iface.init       = ( poolInit_t )&pool_init;
    iface.clear      = ( poolClear_t )&pool_clear;
    iface.spawn      = ( poolSpawn_t )&pool_spawn;
    iface.release    = ( poolReturn_t )&pool_return;
    iface.isRef      = ( poolIsRef_t )&pool_is_ref;
    iface.get        = ( poolGet_t )&pool_get;
    iface.getByIndex = ( poolGetByIndex_t )&pool_get_by_index;

    return iface;
}

//==============================================================================================================================
static pool_i getLitePoolIface( void ) {
    pool_i iface;

    iface.init       = ( poolInit_t )&smdCT_lpool_init;
    iface.clear      = ( poolClear_t )&smdCT_lpool_clear;
    iface.spawn      = ( poolSpawn_t )&smdCT_lpool_spawn;
    iface.release    = ( poolReturn_t )&smdCT_lpool_return;
    iface.isRef      = ( poolIsRef_t )&smdCT_lpool_isRef;
    iface.get        = ( poolGet_t )&smdCT_lpool_get;
    iface.getByIndex = ( poolGetByIndex_t )&smdCT_lpool_getByIndex;

    return iface;
}

//==============================================================================================================================
/**
@brief The core function with all algorithms for pools testing.

@param [in,out] pPool    Pool for testing.
@param [in,out] pRefs    Buffer for entity references.
@param [in]     poolSize Number of entities in the pool.

@return 0 if all tests are completed, otherwise will be if something is failed.
*/
int testCore( void* pPool, pool_i* pIface, ref_t* pRefs, size_t poolSize ) {
    assert( pPool );
    assert( pIface );
    assert( pRefs );

    // Spawn all entities in the pool and check its references.
    for ( size_t iEntity = 0; iEntity < poolSize; ++iEntity ) {
        const ref_t cRef = pIface->spawn( pPool );

        if ( !pIface->isRef( pPool, cRef ) ) {
            return 1;
        }

        pRefs[ iEntity ] = cRef;
    }

    // Bypass the pool to get entities by reference.
    for ( size_t iEntity = 0; iEntity < poolSize; ++iEntity ) {
        const void* cpEntity = pIface->get( pPool, pRefs[ iEntity ] );

        if ( !cpEntity ) {
            return 1;
        }
    }

    // Bypass the pool to get entities by index.
    for ( size_t iEntity = 0; iEntity < poolSize; ++iEntity ) {
        const void* cpEntity = pIface->getByIndex( pPool, iEntity );

        if ( !cpEntity ) {
            return 1;
        }
    }

    // Return all entities.
    for ( size_t iEntity = 0; iEntity < poolSize; ++iEntity ) {
        pIface->release( pPool, pRefs[ iEntity ] );
    }

    // TODO Random test.

    return 0;
}

//==============================================================================================================================
/**
@brief Prepares and controls throughout test process. Invokes testCore().

@param [in] poolSize   Number of entities in the pool.
@param [in] entitySize Size of each entity in the pool.

@return Returns 0 on success and something else when fails.
*/
int testPool( void* pPool, pool_i* pIface, size_t poolSize, size_t entitySize ) {
    assert( pPool );
    assert( pIface );

    int ret = 0;

    ref_t* pRefs = malloc( poolSize * sizeof( ref_t ) );
    memset( pRefs, 0, poolSize * sizeof( ref_t ) );

    if ( pIface->init( pPool, poolSize, entitySize ) ) {
        // FIXME Lite pools are not support zero-sized initializations, thus it's not an error.
        free( pRefs );
        return ret;
    }

    if ( testCore( pPool, pIface, pRefs, poolSize ) ) {
        goto except;
    }

    // Check pool reinitialization.
    memset( pRefs, 0, poolSize * sizeof( ref_t ) );
    pIface->clear( pPool );
    if ( pIface->init( pPool, poolSize, entitySize ) ) {
        goto except;
    }

    if ( testCore( pPool, pIface, pRefs, poolSize ) ) {
        goto except;
    }

exit:
    pIface->clear( pPool );
    free( pRefs );
    return ret;

except:
    ret = 1;
    goto exit;
}

//==============================================================================================================================
/**
@brief Do one test pass on specific interval of pool size.

@param [in] begin      Number of entities in the pool at the begin of test process.
@param [in] end        Number of entities (inclusive) in the pool at the end of test process.
@param [in] factor     1 means that begin and end values are bytes, 1024 - kilobytes, 1024 * 1024 - megabytes, etc.
@param [in] entitySize Size of each entity in the pool.

@return Returns 0 on success and something else when fails.
*/
int testPass( size_t begin, size_t end, size_t factor, size_t entitySize ) {
    for ( size_t i = begin; i <= end; ++i ) {
        const size_t cPoolSize = factor * i;

        pool_i iface;

        pool_t pool;
        iface = getPoolIface();

        // TODO Add test which starts with zero pool and pool will grow.
        // TODO Add test for pool overflow.
        if ( testPool( &pool, &iface, cPoolSize, entitySize ) ) {
            return 1;
        }

        printf( "Origin: [%lu/%lu] (factor=%lu, entity_size=%lu, pool_size=%lu bytes)\n",
                ( sz_t )i, ( sz_t )end, ( sz_t )factor, ( sz_t )entitySize, ( sz_t )( cPoolSize * entitySize ) );

        smdCT_lpool_t litePool;
        iface = getLitePoolIface();

        // TODO Add test for pool overflow.
        if ( testPool( &litePool, &iface, cPoolSize, entitySize ) ) {
            return 1;
        }

        printf( "Lite: [%lu/%lu] (factor=%lu, entity_size=%lu, pool_size=%lu bytes)\n",
                ( sz_t )i, ( sz_t )end, ( sz_t )factor, ( sz_t )entitySize, ( sz_t )( cPoolSize * entitySize ) );
    }

    return 0;
}

//==============================================================================================================================
/**
@brief The main function.
*/
int main( void ) {
    const size_t cMaxOne  = 1024; // TODO Make it as args.
    const size_t cMaxKilo = 1024;
    const size_t cMaxMega = 1;

    const size_t cMaxEntitySize = 1;

    const size_t cMaxMemAvailable = ( size_t )1024 * 1024 * 2048;

    for ( size_t iEntitySize = 1; iEntitySize <= cMaxEntitySize; iEntitySize *= 2 ) {

        // Test the pool with size from 0 to 'cMaxOne' bytes inclusive, step size is 1 byte.
        if ( cMaxOne * iEntitySize <= cMaxMemAvailable ) {
            testPass( 0, cMaxOne, 1, iEntitySize );
        } else {
            printf( "Not enough memory (request_size=%lu). Skip pass.\n", ( sz_t )( cMaxOne * iEntitySize ) );
        }

        // Test the pool with size from 0 to 'cMaxKilo' kilobytes inclusive, step size is 1 kilobyte (1024 bytes).
        if ( cMaxKilo * 1024 * iEntitySize <= cMaxMemAvailable ) {
            testPass( 0, cMaxKilo, 1024, iEntitySize );
        } else {
            printf( "Not enough memory (request_size=%lu). Skip pass.\n", ( sz_t )( cMaxKilo * 1024 * iEntitySize ) );
        }

        // Test the pool with size from 0 to 'cMaxMega' kilobytes inclusive, step size is 1 megabyte (1048576 bytes).
        if ( cMaxMega * 1024 * 1024 * iEntitySize <= cMaxMemAvailable ) {
            testPass( 0, cMaxMega, 1024 * 1024, iEntitySize );
        } else {
            printf( "Not enough memory (request_size=%lu). Skip pass.\n", ( sz_t )( cMaxMega * 1024 * 1024 * iEntitySize ) );
        }
    }

    return 0;
}
