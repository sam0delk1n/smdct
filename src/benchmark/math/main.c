//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#define SMDCT_IMPLEMENTATION

#include "../../../inc/c/smdCT.h"
#include <stdio.h>
#include <time.h>

//==================================================================================================
int main() {
    printf( "Hello, Benchmark!\n" );

    srand( time( NULL ) );

    const size_t cTaskSize = 5000000;

    smdMat4f_t* mat0 = malloc( cTaskSize * sizeof( smdMat4f_t ) );
    assert( mat0 );

    smdMat4f_t* mat1 = malloc( cTaskSize * sizeof( smdMat4f_t ) );
    assert( mat1 );

    smdMat4f_t* mat2 = malloc( cTaskSize * sizeof( smdMat4f_t ) );
    assert( mat2 );

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdMat4f_set(
            &mat1[ i ],
            1,  2,  3,  rand() % 100,
            5,  6,  7,  rand() % 100,
            9,  10, 11, rand() % 100,
            13, 14, 15, rand() % 100
        );

        smdMat4f_set(
            &mat2[ i ],
            17, 18, 19, 20,
            21, 22, 23, 24,
            25, 26, 27, 28,
            29, 30, 31, 32
        );
    }

    clock_t startTime = clock();

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdMat4f_transpose( &mat0[ i ], &mat1[ i ] );
    }

    double cpuTime = ( ( double )( clock() - startTime ) ) / CLOCKS_PER_SEC;
    printf( "smdMat4f_transpose: %f seconds\n", cpuTime );

    startTime = clock();

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdMat4f_multiply( &mat0[ i ], &mat1[ i ], &mat2[ i ] );
    }

    cpuTime = ( ( double )( clock() - startTime ) ) / CLOCKS_PER_SEC;
    printf( "smdMat4f_multiply: %f seconds\n", cpuTime );

    //void smdMat4f_minorMatrix( smdMat3f_t*, const smdMat4f_t*, size_t, size_t );
    //void smdMat4f_determinant( float*, const smdMat4f_t* );

    startTime = clock();

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        // TODO Check epsilon.
        smdMat4f_inverse( &mat0[ i ], &mat1[ i ], 0 );
    }

    cpuTime = ( ( double )( clock() - startTime ) ) / CLOCKS_PER_SEC;
    printf( "smdMat4f_inverse: %f seconds\n", cpuTime );

    free( mat2 );
    free( mat1 );
    free( mat0 );

    // FIXME Part 2
    mat0 = malloc( cTaskSize * sizeof( smdMat4f_t ) );
    assert( mat0 );

    smdVec3f_t* angles = malloc( cTaskSize * sizeof( smdVec3f_t ) );
    assert( angles );

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdVec3f_set( &angles[ i ], rand(), rand(), rand() );
    }

    startTime = clock();

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdMat4fProperEulerAnglesZXZ( &mat0[ i ], angles[ i ].x, angles[ i ].y, angles[ i ].z );
    }

    cpuTime = ( ( double )( clock() - startTime ) ) / CLOCKS_PER_SEC;
    printf( "smdMat4fProperEulerAnglesZXZ: %f seconds\n", cpuTime );

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdVec3f_set( &angles[ i ], rand(), rand(), rand() );
    }

    startTime = clock();

    for ( size_t i = 0; i < cTaskSize; ++i ) {
        smdMat4fProperEulerAnglesZXZFast( &mat0[ i ], angles[ i ].x, angles[ i ].y, angles[ i ].z );
    }

    cpuTime = ( ( double )( clock() - startTime ) ) / CLOCKS_PER_SEC;
    printf( "smdMat4fProperEulerAnglesZXZFast: %f seconds\n", cpuTime );

    free( angles );
    free( mat0 );

    return 0;
}
