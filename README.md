# smdCT

**sam0delk1n's Conventional Tools (smdCT)** -- it's a software development kit which contains
standard tools and a code library to develop performance-critical programs like 3d applications and
games.

## How to build and run Sanity-check

### Clone the repo

Clone from `Bitbucket`.

```bash
git clone https://bitbucket.org/sam0delk1n/smdct.git
cd smdct
```

### Linux

You need to have compilers `gcc-7` and `g++-7`. Run the build script.

```bash
./build
```

All files will be in the `bin` folder. Open it.

```bash
cd bin
```

And then you can run the tests.

```bash
./math-check
./benchmark
./benchmark-fast
./sanity-check
./fbt-check
./hash-check
```

- *math-check* -- Validates vectors, matrices, quaternions and other math stuff.
- *benchmark* -- CPU math performance test.
- *benchmark-fast* -- CPU math performance test compiled with the optimization keys.
- *sanity-check* -- Validates the pools.
- *fbt-check* -- Fluent Behaviour Tree sanity check.
- *hash-check* -- SHA-256 sanity check.
