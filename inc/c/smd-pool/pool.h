//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 mr.DIMAS, sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

/** @file */

#pragma once

#include "pool-common.h"

#include <string.h>

typedef struct pool_t {
    void*   records;        /**< @brief Pointer to objects, that aligned as continuous array of bytes. Each pool record
                                        starts from "stamp" field. So structure of "records" will be something like:
                                        ||stamp+object|...|stamp+object| */
    size_t  record_size;    /**< @brief Size of object + size of stamp */
    size_t  capacity;       /**< @brief Total capacity of the pool */
    size_t  spawned_count;  /**< @brief Count of spawned objects */
    stamp_t global_stamp;   /**< @brief Unique stamp counter */
    size_t* free_stack;     /**< @brief Stack of free object indices */
    size_t  free_stack_top; /**< @brief Count of free object indices */
    int     zero_memory;    /**< @brief Should pool zero memory for spawned objects? Default is true */

    /** @brief Pointer to a function that will be used as destructor when object is returned to the pool,
               or when "pool_clear" is called. By default, no destructor specified */
    void ( *destructor )( void* );
} pool_t;

#ifdef SMDCT_IMPLEMENTATION

//==================================================================================================
static void pool_call_destructor_for_each_element( pool_t* p ) {
    size_t i;
    stamp_t* stamp;

    if(!p->destructor) {
        return;
    }
    for(i = 0; i < p->capacity; ++i) {
        stamp = (stamp_t*)((char*)p->records + p->record_size * i);
        /* Ignore free elements */
        if ( !*stamp ) {
            continue;
        }
        p->destructor((char*)stamp + sizeof(stamp_t));
    }
}

//==================================================================================================
void pool_clear( pool_t* p ) {
    assert( p );

    pool_call_destructor_for_each_element(p);

    free(p->records);
    free(p->free_stack);
    p->free_stack_top = 0;
    p->records = NULL;
    p->capacity = 0;
    p->spawned_count = 0;
    p->global_stamp = 1;
}

//==================================================================================================
/* Will push new index of free object on stack */
static void pool_stack_push( pool_t* p, size_t index ) {
    if ( p->free_stack_top < p->capacity ) {
        p->free_stack[ p->free_stack_top++ ] = index;
        return;
    }
    assert( 0 && "Stack overflow" );
}

//==================================================================================================
static int pool_realloc_memory( pool_t* p, size_t old_capacity ) {
    void* records = realloc( p->records, p->capacity * p->record_size );
    if ( !records ) {
        return 1;
    }
    p->records = records;

    // Zero out new piece of memory.
    char*  ptr  = ( char* )p->records + p->record_size * old_capacity;
    size_t size = p->record_size * ( p->capacity - old_capacity );
    memset( ptr, 0, size );

    // Add new free object indices to stack.
    void* free_stack = realloc( p->free_stack, p->capacity * sizeof( size_t ) );
    if ( !free_stack ) {
        return 1;
    }
    p->free_stack = free_stack;

    for ( int64_t i = p->capacity - 1; i >= ( int )old_capacity; --i ) {
        pool_stack_push( p, i );
    }

    return 0;
}

//==================================================================================================
int pool_init( pool_t* p, size_t capacity, size_t object_size ) {
    assert( p );

    p->records        = NULL;
    p->capacity       = capacity;
    p->spawned_count  = 0;
    p->record_size    = object_size + sizeof(stamp_t);
    p->global_stamp   = 1;
    p->free_stack     = NULL;
    p->free_stack_top = 0;
    p->destructor     = NULL;
    p->zero_memory    = 1;

    return pool_realloc_memory( p, 0 );
}

//==================================================================================================
static size_t pool_stack_pop( pool_t* p ) {
    if ( p->free_stack_top > 0 ) {
        return p->free_stack[ --p->free_stack_top ];
    }
    assert( 0 && "Stack underflow" );
    return 0;
}

//==================================================================================================
static stamp_t* pool_get_stamp( pool_t* p, size_t i ) {
    return ( stamp_t* )( ( char* )p->records + i * p->record_size );
}

//==================================================================================================
/* Returns old capacity */
static size_t pool_increase_capacity( pool_t* p ) {
    size_t old_capacity = p->capacity;
    if(p->capacity == 0) {
        p->capacity = 1;
    } else {
        p->capacity *= 2;
    }
    return old_capacity;
}

//==================================================================================================
ref_t pool_spawn( pool_t* p ) {
    assert( p );

    // Grow pool if needed.
    if ( p->spawned_count >= p->capacity ) {
        if ( pool_realloc_memory( p, pool_increase_capacity( p ) ) ) {
            const ref_t cNullRef = { 0 };
            return cNullRef;
        }
    }

    ref_t ref;
    ref.index = pool_stack_pop( p );

    stamp_t* stamp = pool_get_stamp( p, ref.index );

    if ( p->zero_memory ) {
        // Zero memory of spawned object.
        memset( ( char* )stamp + sizeof( stamp_t ), 0, p->record_size - sizeof( stamp_t ) );
    }

    // Generate new stamp and share it between reference and object.
    ref.stamp = p->global_stamp++;
    *stamp = ref.stamp;

    ++p->spawned_count;

    return ref;
}

//==================================================================================================
int pool_is_ref( pool_t* p, ref_t ref ) {
    assert( p );

    if ( ref.index >= p->capacity ) {
        return 0;
    }

    return *pool_get_stamp( p, ref.index ) == ref.stamp;
}

//==================================================================================================
void pool_return( pool_t* p, ref_t ref ) {
    stamp_t* stamp;
    assert(p);
    if(!pool_is_ref(p, ref)) {
        return;
    }
    stamp = pool_get_stamp(p, ref.index);
    if ( !*stamp ) {
        return;
    }
    /* Call destructor for returnable object */
    if(p->destructor) {
        /* Object is located right of stamp */
        p->destructor((char*)stamp + sizeof(stamp_t));
    }
    *stamp = 0;
    --p->spawned_count;

    /* Push index of returned object to free object indices stack */
    pool_stack_push(p, ref.index);
}

//==================================================================================================
void* pool_get_by_index( pool_t* p, size_t i ) {
    stamp_t* stamp = pool_get_stamp(p, i);
    if ( !*stamp ) {
        return NULL;
    }
    return (char*)stamp + sizeof(stamp_t);
}

//==================================================================================================
void* pool_get( pool_t* p, ref_t ref ) {
    return ( char* )p->records + p->record_size * ref.index + sizeof( stamp_t );
}

#endif // SMDCT_IMPLEMENTATION

int pool_init( pool_t* p, size_t capacity, size_t object_size );

/** @brief Invalidates all handles, frees memory and clean up the state */
void pool_clear( pool_t* p );

/** @brief Spawns the new object inside of the pool. Returns reference to the object
           Note: Allocated memory is filled with zeros, so no need to call memset */
ref_t pool_spawn( pool_t* p );

/* Checks reference validity, use this function to ensure for correctness of a
 * reference */
int pool_is_ref( pool_t* p, ref_t h );

/** @brief Returns object in the pool, all handles to this object becomes invalid */
void pool_return( pool_t* p, ref_t h );

/* Returns pointer to object by reference */
void* pool_get( pool_t* p, ref_t h );

/* Return pointer to object by its index.
 * Note: If index points to free object, NULL will be returned */
void* pool_get_by_index( pool_t* p, size_t i );
