//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2018-2019 mr.DIMAS, sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "pool-common.h"

typedef struct smdCT_lpool_t {
    void*   records;        // Pointer to objects, that aligned as continuous array of bytes.
                            // Each pool record starts from "stamp" field. So structure of
                            // "records" will be something like: ||stamp+object|...|stamp+object||.
    size_t  record_size;    // Size of object + size of stamp.
    size_t  capacity;       // Total capacity of the pool.
    size_t  spawned_count;  // Count of spawned objects.
    stamp_t global_stamp;   // Unique stamp counter.
    size_t* free_stack;     // Stack of free object indices.
    size_t  free_stack_top; // Count of free object indices.
} smdCT_lpool_t;

int smdCT_lpool_init( smdCT_lpool_t* p, size_t capacity, size_t object_size );

// Invalidates all handles, frees memory and clean up the state.
void smdCT_lpool_clear( smdCT_lpool_t* p );

// Spawns the new object inside of the pool. Returns reference to the object.
ref_t smdCT_lpool_spawn( smdCT_lpool_t* p );

// Checks reference validity, use this function to ensure for correctness of a reference.
int smdCT_lpool_isRef( smdCT_lpool_t* p, ref_t h );

// Returns object in the pool, all handles to this object becomes invalid.
void smdCT_lpool_return( smdCT_lpool_t* p, ref_t h );

// Returns pointer to object by reference.
void* smdCT_lpool_get( smdCT_lpool_t* p, ref_t h );

// Return pointer to object by its index.
// Note: If the index points to a free object, NULL will be returned.
void* smdCT_lpool_getByIndex( smdCT_lpool_t* p, size_t i );
