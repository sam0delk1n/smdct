﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "common.h"
#include "vec2f_t.h"

//==================================================================================================
typedef struct smdMat2f_t {
    smdVec2f_t r1, r2;
} smdMat2f_t;

void  smdMat2f_identity( smdMat2f_t* pOut );
void  smdMat2f_set( smdMat2f_t* pOut, float m11, float m12, float m21, float m22 );
bool  smdMat2f_compare( const smdMat2f_t* cpIn1, const smdMat2f_t* cpIn2, float epsilon );
float smdMat2f_determinant( const smdMat2f_t* cpIn );
bool  smdMat2f_inverse( smdMat2f_t* pOut, const smdMat2f_t* cpIn, float epsilon );
void  smdMat2f_transpose( smdMat2f_t* pOut, const smdMat2f_t* cpIn );
void  smdMat2f_multiplyVec( smdVec2f_t* pOut, const smdVec2f_t* cpVec, const smdMat2f_t* cpMat );
void  smdMat2f_multiply( smdMat2f_t* pOut, const smdMat2f_t* cpIn1, const smdMat2f_t* cpIn2 );
void  smdMat2f_rotation( smdMat2f_t* pOut, float angle );
bool  smdMat2f_isSymmetric( const smdMat2f_t* cpIn, float epsilon );
bool  smdMat2f_isOrthogonal( const smdMat2f_t* cpIn, float epsilon );
