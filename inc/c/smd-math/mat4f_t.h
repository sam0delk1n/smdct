﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "mat3f_t.h"
#include "vec4f_t.h"
#include "vec3f_t.h"
#include "common.h"

//==================================================================================================
typedef struct smdMat4f_t {
    smdVec4f_t r1, r2, r3, r4;
} smdMat4f_t;

void smdMat4f_identity( smdMat4f_t* pOut );
void smdMat4f_set(
    smdMat4f_t* pOut,
    float m11, float m12, float m13, float m14,
    float m21, float m22, float m23, float m24,
    float m31, float m32, float m33, float m34,
    float m41, float m42, float m43, float m44
);
bool smdMat4f_compare( const smdMat4f_t* cpIn1, const smdMat4f_t* cpIn2, float epsilon );
void smdMat4f_translate( smdMat4f_t* pOut, const smdVec3f_t* cpTranslateAxis );
void smdMat4f_rotationX( smdMat4f_t* pOut, float angle );
void smdMat4f_rotationY( smdMat4f_t* pOut, float angle );
void smdMat4f_rotationZ( smdMat4f_t* pOut, float angle );

void smdMat4fProperEulerAnglesZXZ( smdMat4f_t* pOut, float z1, float x2, float z3 );
void smdMat4fProperEulerAnglesZXZFast( smdMat4f_t* pOut, float z1, float x2, float z3 );
void smdMat4fTaitBryanAnglesZXY( smdMat4f_t* pOut, float z, float x, float y );

void smdMat4f_scale( smdMat4f_t* pOut, const smdVec3f_t* cpScaleAxis );
void smdMat4f_transpose( smdMat4f_t* pOut, const smdMat4f_t* cpIn );
void smdMat4f_multiplyVec( smdVec4f_t* pOut, const smdVec4f_t* cpVec, const smdMat4f_t* cpMat );
void smdMat4f_multiply( smdMat4f_t* pOut, const smdMat4f_t* cpIn1, const smdMat4f_t* cpIn2 );
void smdMat4f_minorMatrix( smdMat3f_t* pOut, const smdMat4f_t* cpIn, size_t row, size_t col );
void smdMat4f_determinant( float* pOut, const smdMat4f_t* cpIn );
bool smdMat4f_inverse( smdMat4f_t* pOut, const smdMat4f_t* cpIn, float epsilon );
// FIXME void inverseAlt( Matrix4f* const, const Matrix4f* const ) noexcept;
bool smdMat4f_isSymmetric( const smdMat4f_t* cpIn, float epsilon );
bool smdMat4f_isOrthogonal( const smdMat4f_t* cpIn, float epsilon );
