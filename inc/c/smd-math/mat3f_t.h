﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "mat2f_t.h"
#include "vec3f_t.h"
#include "common.h"

//==================================================================================================
typedef struct smdMat3f_t {
    smdVec3f_t r1, r2, r3;
} smdMat3f_t;

void  smdMat3f_identity( smdMat3f_t* pOut );
void  smdMat3f_set(
    smdMat3f_t* pOut,
    float m11, float m12, float m13,
    float m21, float m22, float m23,
    float m31, float m32, float m33
);
bool  smdMat3f_compare( const smdMat3f_t* cpIn1, const smdMat3f_t* cpIn2, float epsilon );
float smdMat3f_determinant( const smdMat3f_t* cpIn );
void  smdMat3f_rotationX( smdMat3f_t* pOut, float angle );
void  smdMat3f_rotationY( smdMat3f_t* pOut, float angle );
void  smdMat3f_rotationZ( smdMat3f_t* pOut, float angle );
void  smdMat3fProperEulerAnglesZXZ( smdMat3f_t* pOut, float z1, float x2, float z3 );
void  smdMat3fProperEulerAnglesZXZFast( smdMat3f_t* pOut, float z1, float x2, float z3 );
void  smdMat3fTaitBryanAnglesZXY( smdMat3f_t* pOut, float z, float x, float y );
void  smdMat3f_multiplyVec( smdVec3f_t* pOut, const smdVec3f_t* cpVec, const smdMat3f_t* cpMat );
void  smdMat3f_multiply( smdMat3f_t* pOut, const smdMat3f_t* cpIn1, const smdMat3f_t* cpIn2 );
void  smdMat3f_transpose( smdMat3f_t* pOut, const smdMat3f_t* cpIn );
// TODO Inverse
bool  smdMat3f_isSymmetric( const smdMat3f_t* cpIn, float epsilon );
bool  smdMat3f_isOrthogonal( const smdMat3f_t* cpIn, float epsilon );
