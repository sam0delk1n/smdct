﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "mat4f_t.h"
#include "vec3f_t.h"
#include "common.h"

//==================================================================================================
//
// TODO
//
// * q_lerper
// * q_slerper
// * shortest_arc
//
// * норма
// * модуль
// * сопряжение
// * инверсный
//
//==================================================================================================

//==================================================================================================
typedef struct smdQuatf_t {
    float x, y, z, w;
} smdQuatf_t;

void  smdQuatf_set( smdQuatf_t* pOut, float x, float y, float z, float w );
void  smdQuatf_fromAxisAngle( smdQuatf_t* pOut, const smdVec3f_t* cpAxis, float angle );
void  smdQuatf_fromYawPitchRoll( smdQuatf_t* pOut, const smdVec3f_t* cpYpr );
void  smdQuatf_add( smdQuatf_t* pOut, const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 );
void  smdQuatf_scale( smdQuatf_t* pOut, const smdQuatf_t* cpIn, float scale );
float smdQuatf_dot( const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 );
bool  smdQuatf_compare( const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2, float eps );
float smdQuatf_norm( const smdQuatf_t* cpIn );
float smdQuatf_magnitude( const smdQuatf_t* cpIn );
void  smdQuatf_normalize( smdQuatf_t* pOut, const smdQuatf_t* cpIn, float eps );
void  smdQuatf_multiply( smdQuatf_t* pOut, const smdQuatf_t* cpIn1, const smdQuatf_t* cpIn2 );
void  smdQuatf_unitToMat3f( smdMat3f_t* pOut, const smdQuatf_t* cpIn );
void  smdQuatf_unitToMat4f( smdMat4f_t* pOut, const smdQuatf_t* cpIn );
void  smdQuatf_toMat3f( smdMat3f_t* pOut, const smdQuatf_t* cpIn );
void  smdQuatf_toMat4f( smdMat4f_t* pOut, const smdQuatf_t* cpIn );
void  smdQuatf_unitFromMat3f( smdQuatf_t* pOut, const smdMat3f_t* cpIn );
void  smdQuatf_unitFromMat4f( smdQuatf_t* pOut, const smdMat4f_t* cpIn );
void  smdQuatf_fromMat3f( smdQuatf_t* pOut, const smdMat3f_t* cpIn );
void  smdQuatf_fromMat4f( smdQuatf_t* pOut, const smdMat4f_t* cpIn );
// TODO Quat     Inverse(void) const;
// TODO Vector3f ToAngularVelocity(void) const;
// TODO Quat&    Slerp(const Quat& from, const Quat& to, float t);
