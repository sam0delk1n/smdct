﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "common.h"

//==================================================================================================
typedef struct smdVec4f_t {
    float x, y, z, w;
} smdVec4f_t;

void  smdVec4f_set( smdVec4f_t* pOut, float x, float y, float z, float w );
void  smdVec4f_negate( smdVec4f_t* pOut, const smdVec4f_t* cpIn );
void  smdVec4f_add( smdVec4f_t* pOut, const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 );
void  smdVec4f_subtract( smdVec4f_t* pOut, const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 );
void  smdVec4f_scale( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float scale );
float smdVec4f_dot( const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2 );
bool  smdVec4f_compare( const smdVec4f_t* cpIn1, const smdVec4f_t* cpIn2, float eps );
float smdVec4f_length( const smdVec4f_t* cpIn );
void  smdVec4f_normalize( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float eps );
void  smdVec4f_toDword( uint32_t* pOut, const smdVec4f_t* cpIn );
void  smdVec4f_fromDword( smdVec4f_t* pOut, uint32_t in );
void  smdVec4f_clamp( smdVec4f_t* pOut, const smdVec4f_t* cpIn, float low, float high );
void  smdVec4f_saturate( smdVec4f_t* pOut, const smdVec4f_t* cpIn );
