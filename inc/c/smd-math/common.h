﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define SMDCT_EPSILON           1e-6
#define SMDCT_INVERSEMATEPSILON 1e-14

#define SMDCT_E        2.71828182845904523536
#define SMDCT_LOG2E    1.44269504088896340736
#define SMDCT_LOG10E   0.434294481903251827651
#define SMDCT_LN2      0.693147180559945309417
#define SMDCT_LN10     2.30258509299404568402
#define SMDCT_PI       3.14159265358979323846
#define SMDCT_PI_2     1.57079632679489661923
#define SMDCT_PI_4     0.785398163397448309616
#define SMDCT_1_PI     0.318309886183790671538
#define SMDCT_2_PI     0.636619772367581343076
#define SMDCT_2_SQRTPI 1.12837916709551257390
#define SMDCT_SQRT2    1.41421356237309504880
#define SMDCT_SQRT1_2  0.707106781186547524401
