﻿//==================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2019 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==================================================================================================

#pragma once

#include "common.h"

//==================================================================================================
typedef struct smdVec3f_t {
    float x, y, z;
} smdVec3f_t;

void  smdVec3f_set( smdVec3f_t* pOut, float x, float y, float z );
void  smdVec3f_negate( smdVec3f_t* pOut, const smdVec3f_t* cpIn );
void  smdVec3f_add( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 );
void  smdVec3f_subtract( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 );
void  smdVec3f_scale( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float scale );
float smdVec3f_dot( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 );
bool  smdVec3f_compare( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2, float eps );
float smdVec3f_length( const smdVec3f_t* cpIn );
void  smdVec3f_normalize( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float eps );
float smdVec3f_distance( const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 );
void  smdVec3f_toDword( uint32_t* pOut, const smdVec3f_t* cpIn );
void  smdVec3f_fromDword( smdVec3f_t* pOut, uint32_t in );
void  smdVec3f_clamp( smdVec3f_t* pOut, const smdVec3f_t* cpIn, float low, float high );
void  smdVec3f_saturate( smdVec3f_t* pOut, const smdVec3f_t* cpIn );
void  smdVec3f_cross( smdVec3f_t* pOut, const smdVec3f_t* cpIn1, const smdVec3f_t* cpIn2 );
// TODO Vector3f& Truncate( float length );
// TODO void      Snap( void );
// TODO void      SnapInt( void );
