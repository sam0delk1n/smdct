/*
================================================================================

aabb.hpp

© sam0delk1n, 2013-2015



================================================================================
*/

#pragma once

#include "math/vec3.hpp"

namespace smd {

/*
================================================================================

aabb_t

================================================================================
*/
struct aabb_t final {
    math::vec3f_t                       position;
    math::vec3f_t                       radius;

    static auto                         diagonalHalfLen( const aabb_t& aabb ) noexcept ->float;

    static auto                         testCollision( const aabb_t& a, const aabb_t& b ) noexcept ->bool;
    //auto                              CheckLineCollision( const Vector3f& point, const Vector3f& vector ) const ->int32_t;
};

} // namespace smd
