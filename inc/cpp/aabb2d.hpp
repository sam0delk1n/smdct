
#ifndef __AABB2D_HPP__
#define __AABB2D_HPP__

/*
================================================================================

aabb2d.hpp

© sam0delk1n, 2013-2015



================================================================================
*/

#include "math/vec2.hpp"

namespace smd {

/*
================================================================================

aabb2d_t

================================================================================
*/
struct aabb2d_t final {
    math::vec2f_t                       position;
    math::vec2f_t                       radius;

    static auto                         diagonalHalfLen( const aabb2d_t& aabb2d ) noexcept ->float;

    static auto                         testCollision( const aabb2d_t& a, const aabb2d_t& b ) noexcept ->bool;
    //auto                              CheckLineCollision( const Vector2f& point, const Vector2f& vector ) const ->int32_t;
};

//FIXME
//static_assert( std::is_pod< aabb2d_t >::value, "struct aabb2d_t is not a POD" );

}/*namespace smd*/

#endif/*__AABB2D_HPP__*/

