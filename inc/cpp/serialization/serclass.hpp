#pragma once

//#include <functional>
//#include <map>

namespace smd {

//==============================================================================================================================
class archTexIn_c final {
public:
    //container& mCont;
    //size_t     mPos;

    //inline archTexIn_c( container& cont, const size_t cPos );
             archTexIn_c( void ) = default;

    //inline void                           mReset( void );
    //template< class T > inline void       mRawRead( T beginPos, const size_t cLen );

private:
    //template< class T > inline void mDeserial( T& val );
    //template< class T > inline void deserialize_impl( T& val, );
};

/*
//==============================================================================================================================
inline archTexIn_c::archTexIn_c( container& cont, const size_t cPos ) :
mCont( cont ), mPos( cPos ) {}
*/

/*
//==============================================================================================================================
inline void archTexIn_c::mReset( void ) {
    resetPos();
    mCont.clear();
}
*/

/*
//==============================================================================================================================
template< class T > inline void archTexIn_c::mRawRead( T beginPos, const size_t cLen ) {
    HASSERT( static_cast< size_t >( mCont.size() - mPos ) >= cLen );

    iterator pos = mCont.begin() + mPos;
    iterator endPos = pos + cLen;
    std::copy( pos, endPos, beginPos );
    mPos = mPos + cLen;
}
*/

/*
//==============================================================================================================================
template< class T > inline void archTexIn_c::mDeserial( T& val ) {
    mRawRead( reinterpret_cast< char* >( &val ), sizeof( T ) );
}
*/

/*
//==============================================================================================================================
template< class T > class serial_c {
public:
    virtual inline void mSerialize( T& cont );
    virtual inline void mDeserialize( T& cont );

    virtual ~serial_c( void ) = default;

protected:
    typedef std::function< void( const std::string&, T& ) > func_t;

    struct pair_t {
        func_t serializer;
        func_t deserializer;
    };

    inline char mAdd( const std::string& key, func_t serializer, func_t deserializer );

private:
    std::map< std::string, pair_t > mSerializers;
};

//==============================================================================================================================
template< class T > inline char serial_c< T >::mAdd( const std::string& key, func_t serializer, func_t deserializer ) {
    pair_t& pair { mSerializers[ key ] };
    pair.serializer   = serializer;
    pair.deserializer = deserializer;
    return 0;
}

//==============================================================================================================================
template< class T > inline void serial_c< T >::mSerialize( T& cont ) {
    for ( auto& ser : mSerializers ) {
        ser.second.serializer( ser.first, cont );
    }
}

//==============================================================================================================================
template< class T > inline void serial_c< T >::mDeserialize( T& cont ) {
    for ( auto& ser : mSerializers ) {
        ser.second.deserializer( ser.first, cont );
    }
}

#define serialize( x ) char UNNAMED = Add( #x, \
                                     [ this ]( const std::string& key, ClearType< decltype( ContainerInf ) >::Type& cont ) {}, \
                                     [ this ]( const std::string& key, ClearType< decltype( ContainerInf ) >::Type& cont ) {} )
*/

} // namespace smd
