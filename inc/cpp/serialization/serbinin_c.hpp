//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2017 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "../math.hpp"

#include <cstdint>
#include <fstream>
#include <string>

namespace smd {

//==============================================================================================================================
class serBinIn_c final {
public:
    serBinIn_c& operator=( const serBinIn_c& )  = delete;
    inline      serBinIn_c( const std::string& cFileName );
                serBinIn_c( const serBinIn_c& ) = delete;
                ~serBinIn_c( void )             = default;

    template< class T > inline serBinIn_c& operator&( T& val );
    inline bool                            mcGood( void ) const;

private:
    std::ifstream mIn;
};

//==============================================================================================================================
inline serBinIn_c::serBinIn_c( const std::string& cFileName ) :
mIn( cFileName, std::ios_base::binary ) {}

//==============================================================================================================================
#define READ_TSPEC( T )                                         \
template<> inline serBinIn_c& serBinIn_c::operator&( T& val ) { \
    mIn.read( reinterpret_cast< char* >( &val ), sizeof( T ) ); \
    return *this;                                               \
}

READ_TSPEC( uint32_t )
READ_TSPEC( float )
READ_TSPEC( math::vec3f_t )
READ_TSPEC( math::vec4f_t )
READ_TSPEC( math::quatf_t )

//==============================================================================================================================
template<> inline serBinIn_c& serBinIn_c::operator&( std::string& val ) {
    getline( mIn, val, mIn.widen( '\0' ) );
    return *this;
}

//==============================================================================================================================
template< class T > inline serBinIn_c& serBinIn_c::operator&( T& val ) {
    val.mSerial( *this, 1 );
    return *this;
}

//==============================================================================================================================
inline bool serBinIn_c::mcGood( void ) const {
    return mIn.good();
}

} // namespace smd
