//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2017 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "../math.hpp"

#include <cstdint>
#include <fstream>
#include <string>

namespace smd {

//==============================================================================================================================
class serTexOut_c final {
public:
    serTexOut_c& operator=( const serTexOut_c& )   = delete;
    inline       serTexOut_c( const std::string& cFileName );
                 serTexOut_c( const serTexOut_c& ) = delete;
                 ~serTexOut_c( void )              = default;

    template< class T > inline serTexOut_c& operator&( T& val );

private:
    std::ofstream mOut;
};

//==============================================================================================================================
inline serTexOut_c::serTexOut_c( const std::string& cFileName ) :
mOut( cFileName ) {}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( uint32_t& val ) {
    mOut << "uint32_t(" << val << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const uint32_t& val ) {
    mOut << "uint32_t(" << val << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( float& val ) {
    mOut << "float("<< val << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const float& val ) {
    mOut << "float("<< val << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( math::vec3f_t& val ) {
    mOut << "vec3f_t(" << val.x << "," << val.y << "," << val.z << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const math::vec3f_t& val ) {
    mOut << "vec3f_t(" << val.x << "," << val.y << "," << val.z << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( math::vec4f_t& val ) {
    mOut << "vec4f_t(" << val.x << "," << val.y << "," << val.z << "," << val.w << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const math::vec4f_t& val ) {
    mOut << "vec4f_t(" << val.x << "," << val.y << "," << val.z << "," << val.w << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( math::quatf_t& val ) {
    mOut << "quatf_t(" << val.x << "," << val.y << "," << val.z << "," << val.w << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const math::quatf_t& val ) {
    mOut << "quatf_t(" << val.x << "," << val.y << "," << val.z << "," << val.w << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( std::string& val ) {
    mOut << "string(" << val << ")\n";
    return *this;
}

//==============================================================================================================================
template<> inline serTexOut_c& serTexOut_c::operator&( const std::string& val ) {
    mOut << "string(" << val << ")\n";
    return *this;
}

//==============================================================================================================================
template< class T > inline serTexOut_c& serTexOut_c::operator&( T& val ) {
    val.mSerial( *this, 1 );
    return *this;
}

} // namespace smd
