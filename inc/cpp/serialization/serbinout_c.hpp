//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2017 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "../math.hpp"

#include <cstdint>
#include <fstream>
#include <string>

namespace smd {

//==============================================================================================================================
class serBinOut_c final {
public:
    serBinOut_c& operator=( const serBinOut_c& )   = delete;
    inline       serBinOut_c( const std::string& cFileName );
                 serBinOut_c( const serBinOut_c& ) = delete;
                 ~serBinOut_c( void )              = default;

    template< class T > inline serBinOut_c& operator&( T& val );

private:
    std::ofstream mOut;
};

//==============================================================================================================================
inline serBinOut_c::serBinOut_c( const std::string& cFileName ) :
mOut( cFileName, std::ios_base::binary ) {}

//==============================================================================================================================
#define WRITE_TSPEC( T )                                                \
template<> inline serBinOut_c& serBinOut_c::operator&( T& val ) {       \
    mOut.write( reinterpret_cast< const char* >( &val ), sizeof( T ) ); \
    return *this;                                                       \
}

WRITE_TSPEC( uint32_t )
WRITE_TSPEC( const uint32_t )
WRITE_TSPEC( float )
WRITE_TSPEC( const float )
WRITE_TSPEC( math::vec3f_t )
WRITE_TSPEC( const math::vec3f_t )
WRITE_TSPEC( math::vec4f_t )
WRITE_TSPEC( const math::vec4f_t )
WRITE_TSPEC( math::quatf_t )
WRITE_TSPEC( const math::quatf_t )

//==============================================================================================================================
template<> inline serBinOut_c& serBinOut_c::operator&( std::string& val ) {
    mOut.write( val.c_str(), val.size() + 1 );
    return *this;
}

//==============================================================================================================================
template<> inline serBinOut_c& serBinOut_c::operator&( const std::string& val ) {
    mOut.write( val.c_str(), val.size() + 1 );
    return *this;
}

//==============================================================================================================================
template< class T > inline serBinOut_c& serBinOut_c::operator&( T& val ) {
    val.mSerial( *this, 1 );
    return *this;
}

} // namespace smd
