
#ifndef __AABBTESTGROUP_HPP__
#define __AABBTESTGROUP_HPP__

/*
================================================================================

aabbTestGroup.hpp

© sam0delk1n, 2015



================================================================================
*/

#include "aabb.hpp"

#include <cstddef>
#include <cstdint>
#include <vector>

namespace smd {

/*
================================================================================

aabbTestGroup_c

================================================================================
*/
class aabbTestGroup_c final {
public:
    auto                                get( const uint32_t index ) ->aabb_t&;
    auto                                get( const uint32_t index ) const ->const aabb_t&;

    auto                                add( const aabb_t& aabb ) ->size_t;
    auto                                remove( const uint32_t index ) ->void;

    auto                                computeIterativeOffset( const uint32_t aabbCurrent, const math::vec3f_t& offset, const float deltaOffset ) ->int32_t;

    auto                                operator=( const aabbTestGroup_c& other ) ->aabbTestGroup_c&;
                                        aabbTestGroup_c( void ) = default;
                                        aabbTestGroup_c( const aabbTestGroup_c& other );
                                        ~aabbTestGroup_c( void ) = default;

private:
    std::vector< aabb_t >               aabbGroup;

                                        // If potential collision was found, the function will return true,
                                        // and in the 'found' vector will be written indexes of AABBs, which was found.
    static auto                         findPotentialCollision( const uint32_t current,
                                                                const std::vector< aabb_t >& all,
                                                                std::vector< uint32_t >& found,
                                                                const float moveLen ) noexcept ->bool;

                                        // If a 'current' AABB might be moved with 'deltaOffset' value,
                                        // and collision will not occur, the function changes the AABB position,
                                        // and returns 'false'.
    static auto                         computeDeltaOffset( const uint32_t current,
                                                            std::vector< aabb_t >& all,
                                                            const std::vector< uint32_t >& found,
                                                            const math::vec3f_t& deltaOffset ) noexcept ->bool;
};

}/*namespace smd*/

#endif/*__AABBTESTGROUP_HPP__*/

