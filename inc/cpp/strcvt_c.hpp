//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2016-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include <codecvt>
#include <string>

namespace smd {

//==============================================================================================================================
class strcvt_c final {
public:
    static inline std::wstring str2wstr( const std::string& in );
    static inline std::string  wstr2str( const std::wstring& in );

    strcvt_c& operator=( const strcvt_c& ) = delete;
    strcvt_c& operator=( strcvt_c&& )      noexcept = delete;
              strcvt_c( void )             = delete;
              strcvt_c( const strcvt_c& )  = delete;
              strcvt_c( strcvt_c&& )       noexcept = delete;
              ~strcvt_c( void )            = delete;
};

//==============================================================================================================================
inline std::wstring strcvt_c::str2wstr( const std::string& in ) {
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > cvt;
    return cvt.from_bytes( in );

    // FIXME Windows official implementation.
    // mbstowcs( const_cast< wstring::value_type* >( out.data() ), in.data(), out.size() );
}

//==============================================================================================================================
inline std::string strcvt_c::wstr2str( const std::wstring& in ) {
    std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > cvt;
    return cvt.to_bytes( in );

    // FIXME Windows official implementation.
    // wcstombs( const_cast< string::value_type* >( out.data() ), in.data(), out.size() );
}

} // namespace smd
