//==============================================================================================================================
//
// � 2015-2017 sam0delk1n
//
//==============================================================================================================================

#pragma once

#include "math/vec3.hpp"

namespace smd {
namespace math {

int32_t intersecRayTri( const vec3f_t& orig, const vec3f_t& dir, const vec3f_t& vert0, const vec3f_t& vert1,
                        const vec3f_t& vert2, float& t, float& u, float& v );

} // namespace math
} // namespace smd
