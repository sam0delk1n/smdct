#ifndef __TESTGROUP2D_HPP__
#define __TESTGROUP2D_HPP__

/*
================================================================================

TestGroup2D.hpp

© sam0delk1n, 2013-2015



================================================================================
*/

#include "aabb2d.hpp"

#include "math/vec2.hpp"

#include <cstdint>
#include <vector>

namespace smd {

/*
================================================================================

TestGroup2D

================================================================================
*/
class TestGroup2D {
    std::vector< aabb2d_t >             boxes;

public:
    auto                                Get( uint32_t num ) ->aabb2d_t&;
    auto                                Get( uint32_t num ) const ->const aabb2d_t&;

    auto                                Add( const aabb2d_t& aabb ) ->size_t;
    auto                                Delete( uint32_t num ) ->void;

                                        // Итеративно перемещает и проверяет целевой AABB на пересечения с другими AABB из тестовой группы.
                                        // Результат перемещения сохраняется в значении offset целевого AABB.
                                        //
                                        // Аргументы:
                                        // num - номер в списке тест группы целевого AABB.
                                        // move - вектор приращения координат ( куда надо сдвинуть AABB ОТНОСИТЕЛЬНО его текущей позиции ).
                                        // deltaDist - на отрезки какой максимальной длины нужно поделить путь перемещения.
                                        //
                                        // Функция возвращает:
                                        // 0 - если небыло коллизий ( итоговый_offset = предыдущий_offset + move );
                                        // 1 - если была коллизия и объект целиком заблокирован;
                                        // 2 - была коллизия и объект изменил траекторию;
                                        // В случае возврата 1 и 2 итоговый_offset будет отличаться от ожидаемого.
                                        // Если функция возвратила отрицательное значение значит ей неподходят аргументы.
    auto                                computeIterativeOffset( const uint32_t aabbCurrent, const math::vec2f_t& offset, const float deltaOffset ) ->int32_t;

                                        // If potential collision was found, the function will return true,
                                        // and in the 'found' vector will be written indexes of AABBs, which was found.
    static auto                         findPotentialCollision( const uint32_t current,
                                                                const std::vector< aabb2d_t >& all,
                                                                std::vector< uint32_t >& found,
                                                                const float moveLen ) noexcept ->bool;

    auto                                operator=( const TestGroup2D& a ) ->TestGroup2D&;

                                        TestGroup2D( void ) = default;
                                        TestGroup2D( const TestGroup2D& a );
                                        ~TestGroup2D( void ) = default;

private:
                                        // If a 'current' AABB might be moved with 'deltaOffset' value,
                                        // and collision will not occur, the function changes the AABB position,
                                        // and returns 'false'.
    static auto                         computeDeltaOffset( const uint32_t current,
                                                            std::vector< aabb2d_t >& all,
                                                            const std::vector< uint32_t >& found,
                                                            const math::vec2f_t& deltaOffset ) noexcept ->bool;
};

}/*namespace smd*/

#endif/*__TESTGROUP2D_HPP__*/
