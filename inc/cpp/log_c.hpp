﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2014-2017 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

namespace smd {

//==============================================================================================================================
enum class stream_t : uint32_t {
    file   = 1,
    cout   = 2,
    cerr   = 4,
    buffer = 8
};

//==============================================================================================================================
constexpr stream_t operator&( const stream_t a, const stream_t b ) noexcept {
    return static_cast< stream_t >( static_cast< uint32_t >( a ) & static_cast< uint32_t >( b ) );
}

//==============================================================================================================================
constexpr stream_t operator|( const stream_t a, const stream_t b ) noexcept {
    return static_cast< stream_t >( static_cast< uint32_t >( a ) | static_cast< uint32_t >( b ) );
}

//==============================================================================================================================
constexpr stream_t operator~( const stream_t a ) noexcept {
    return static_cast< stream_t >( ~static_cast< uint32_t >( a ) );
}

//==============================================================================================================================
template< typename charT > class log_c;

log_c< char >&    logInstA( void );
log_c< wchar_t >& logInstW( void );

#define logInst logInstA

//==============================================================================================================================
template< typename charT > class log_c final {
    using ios_t     = std::basic_ios< charT >;
    using ostream_t = std::basic_ostream< charT >;

public:
    stream_t mOutput { stream_t::buffer };

    constexpr const std::string& mcGetFileName( void ) const noexcept;
    constexpr void               mSetFileName( const std::string& str, const bool fileEnd );

    constexpr bool mcCheckOutput( const stream_t cOutput ) const noexcept;

    constexpr std::string mcGetBuff( void ) const;

    template< typename valT > constexpr log_c& operator<<( valT val );

    constexpr log_c& operator<<( ostream_t& ( *pf )( ostream_t& ) );

    log_c& operator=( const log_c& ) = delete;
    log_c& operator=( log_c&& )      noexcept = delete;
           log_c( void )             = default;
           log_c( const log_c& )     = delete;
           log_c( log_c&& )          noexcept = delete;
           ~log_c( void )            = default;

private:
    std::basic_ofstream< charT >     mOutFile;
    std::string                      mFileName { "unnamed.log" }; // FIXME Only char for filenames.
    std::basic_stringstream< charT > mBuffer;

    template< typename valT > constexpr void write( valT val );
};

//==============================================================================================================================
template< typename charT > constexpr const std::string& log_c< charT >::mcGetFileName( void ) const noexcept {
    return mFileName;
}

//==============================================================================================================================
template< typename charT > constexpr void log_c< charT >::mSetFileName( const std::string& str, const bool fileEnd ) {
    if ( mOutFile.is_open() ) {
        mOutFile.close();
    }
    if ( fileEnd ) {
        mOutFile.open( str, ios_t::out | ios_t::app );
    } else {
        mOutFile.open( str );
    }

    // TODO PYR_HASSERT( mOutFile.is_open(), "mOutFile.is_open()" );
    mFileName = str;
    if ( fileEnd ) {
        mOutFile.seekp( ios_t::end );
    } else {
        mOutFile.seekp( ios_t::beg );
    }
}

//==============================================================================================================================
template< typename charT > constexpr bool log_c< charT >::mcCheckOutput( const stream_t cOutput ) const noexcept {
    return static_cast< uint32_t >( mOutput & cOutput );
}

//==============================================================================================================================
template< typename charT > constexpr std::string log_c< charT >::mcGetBuff( void ) const {
    return mBuffer.str();
}

//==============================================================================================================================
template< typename charT > template< typename valT > constexpr log_c< charT >& log_c< charT >::operator<<( valT val ) {
    write( val );
    return *this;
}

//==============================================================================================================================
template< typename charT > constexpr log_c< charT >& log_c< charT >::operator<<( ostream_t& ( *pf )( ostream_t& ) ) {
    write( pf );
    return *this;
}

//==============================================================================================================================
template<> template< typename valT > constexpr void log_c< char >::write( valT val ) {
    if ( mcCheckOutput( stream_t::cout ) ) {
        std::cout << val;
    }
    if ( mcCheckOutput( stream_t::cerr ) ) {
        std::cerr << val;
    }
    if ( mcCheckOutput( stream_t::file ) ) {
        if ( !mOutFile.is_open() ) {
            mOutFile.open( mFileName, ios_t::out | ios_t::app );
            // TODO PYR_HASSERT( mOutFile.is_open(), "mOutFile.is_open()" );
            mOutFile.seekp( ios_t::end );
        }
        mOutFile << val;
    }
    if ( mcCheckOutput( stream_t::buffer ) ) {
        mBuffer << val;
    }
}

//==============================================================================================================================
template<> template< typename valT > constexpr void log_c< wchar_t >::write( valT val ) {
    if ( mcCheckOutput( stream_t::cout ) ) {
        std::wcout << val;
    }
    if ( mcCheckOutput( stream_t::cerr ) ) {
        std::wcerr << val;
    }
    if ( mcCheckOutput( stream_t::file ) ) {
        if ( !mOutFile.is_open() ) {
            mOutFile.open( mFileName, ios_t::out | ios_t::app );
            // TODO PYR_HASSERT( mOutFile.is_open(), "mOutFile.is_open()" );
            mOutFile.seekp( ios_t::end );
        }
        mOutFile << val;
    }
    if ( mcCheckOutput( stream_t::buffer ) ) {
        mBuffer << val;
    }
}

} // namespace smd

//==============================================================================================================================
#define SMD_FORCE_WARNING( condition, comment )          \
    if ( !( condition ) ) {                              \
        std::basic_stringstream< char > out;             \
        out << "WARNING: file=\"" << __FILE__            \
            << "\" line=\""       << __LINE__            \
            << "\" function=\""   << __PRETTY_FUNCTION__ \
            << "\" comment=\""    << comment             \
            << "\""               << std::endl;          \
        *smd::logInst() << out.str();                    \
    }

//==============================================================================================================================
#define SMD_FORCE_WARNING_EX( condition, comment, action ) \
    if ( !( condition ) ) {                                \
        std::basic_stringstream< char > out;               \
        out << "WARNING: file=\"" << __FILE__              \
            << "\" line=\""       << __LINE__              \
            << "\" function=\""   << __PRETTY_FUNCTION__   \
            << "\" comment=\""    << comment               \
            << "\""               << std::endl;            \
        *smd::logInst() << out.str();                      \
        action;                                            \
    }
