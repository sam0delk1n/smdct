﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

//==============================================================================================================================
//
// TODO list:
// * q_lerper
// * q_slerper
// * shortest_arc
// * сопряжение
// * инверсный
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include "vec3.hpp"
#include "mat4.hpp"

#include <cmath>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct quat_t final {
    T x, y, z, w;

    // TODO Quat     Inverse(void) const;
    // TODO Vector3f ToAngularVelocity(void) const;
    // TODO Quat&    Slerp(const Quat& from, const Quat& to, float t);
};

using quatf_t = quat_t< float >;
using quatd_t = quat_t< double >;

static_assert( std::is_pod< quatf_t >::value && sizeof( quatf_t ) == 16, "" );
static_assert( std::is_pod< quatd_t >::value && sizeof( quatd_t ) == 32, "" );

//==============================================================================================================================
template< typename T >
constexpr void set( quat_t< T >& out, T x, T y, T z, T w ) noexcept {
    out = { x, y, z, w };
}

//==============================================================================================================================
template< typename T >
inline void quatFromAxisAngle( quat_t< T >& out, const vec3_t< T >& cAxis, T angle ) noexcept {
    const T cHalfAngle { 0.5f * angle };
    const T cHalfSin   { sin( cHalfAngle ) };

    out = { cAxis.x * cHalfSin, cAxis.y * cHalfSin, cAxis.z * cHalfSin, cos( cHalfAngle ) };
}

//==============================================================================================================================
template< typename T >
inline quat_t< T > quatFromAxisAngle( const vec3_t< T >& cAxis, T angle ) noexcept {
    const T cHalfAngle { 0.5f * angle };
    const T cHalfSin   { sin( cHalfAngle ) };

    return { cAxis.x * cHalfSin, cAxis.y * cHalfSin, cAxis.z * cHalfSin, cos( cHalfAngle ) };
}

//==============================================================================================================================
template< typename T >
inline void quatYpr( quat_t< T >& out, const vec3_t< T >& cYpr ) noexcept {
    const T cHalfYaw   { cYpr.x * 0.5f };
    const T cHalfPitch { cYpr.y * 0.5f };
    const T cHalfRoll  { cYpr.z * 0.5f };

    const T cCosYaw   { cos( cHalfYaw ) };
    const T cSinYaw   { sin( cHalfYaw ) };
    const T cCosPitch { cos( cHalfPitch ) };
    const T cSinPitch { sin( cHalfPitch ) };
    const T cCosRoll  { cos( cHalfRoll ) };
    const T cSinRoll  { sin( cHalfRoll ) };

    out = { cCosRoll * cSinPitch * cCosYaw + cSinRoll * cCosPitch * cSinYaw,
            cCosRoll * cCosPitch * cSinYaw - cSinRoll * cSinPitch * cCosYaw,
            cSinRoll * cCosPitch * cCosYaw - cCosRoll * cSinPitch * cSinYaw,
            cCosRoll * cCosPitch * cCosYaw + cSinRoll * cSinPitch * cSinYaw };
}

//==============================================================================================================================
template< typename T >
inline quat_t< T > quatYpr( const vec3_t< T >& cYpr ) noexcept {
    const T cHalfYaw   { cYpr.x * 0.5f };
    const T cHalfPitch { cYpr.y * 0.5f };
    const T cHalfRoll  { cYpr.z * 0.5f };

    const T cCosYaw   { cos( cHalfYaw ) };
    const T cSinYaw   { sin( cHalfYaw ) };
    const T cCosPitch { cos( cHalfPitch ) };
    const T cSinPitch { sin( cHalfPitch ) };
    const T cCosRoll  { cos( cHalfRoll ) };
    const T cSinRoll  { sin( cHalfRoll ) };

    return { cCosRoll * cSinPitch * cCosYaw + cSinRoll * cCosPitch * cSinYaw,
             cCosRoll * cCosPitch * cSinYaw - cSinRoll * cSinPitch * cCosYaw,
             cSinRoll * cCosPitch * cCosYaw - cCosRoll * cSinPitch * cSinYaw,
             cCosRoll * cCosPitch * cCosYaw + cSinRoll * cSinPitch * cSinYaw };
}

//==============================================================================================================================
template< typename T >
constexpr void add( quat_t< T >& out, const quat_t< T >& in1, const quat_t< T >& in2 ) noexcept {
    out = { in1.x + in2.x, in1.y + in2.y, in1.z + in2.z, in1.w + in2.w };
}

//==============================================================================================================================
template< typename T >
constexpr void scale( quat_t< T >& out, const quat_t< T >& in, T scale ) noexcept {
    out = { in.x * scale, in.y * scale, in.z * scale, in.w * scale };
}

//==============================================================================================================================
template< typename T >
constexpr T dot( const quat_t< T >& in1, const quat_t< T >& in2 ) noexcept {
    return in1.x * in2.x + in1.y * in2.y + in1.z * in2.z + in1.w * in2.w;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool compare( const quat_t< T >& cQuat1, const quat_t< T >& cQuat2, T eps = cEpsilon ) noexcept {
    if ( fabs( cQuat1.x - cQuat2.x ) <= eps &&
         fabs( cQuat1.y - cQuat2.y ) <= eps &&
         fabs( cQuat1.z - cQuat2.z ) <= eps &&
         fabs( cQuat1.w - cQuat2.w ) <= eps ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
template< typename T >
constexpr T norm( const quat_t< T >& in ) noexcept {
    return in.x * in.x + in.y * in.y + in.z * in.z + in.w * in.w;
}

//==============================================================================================================================
template< typename T >
inline T magnitude( const quat_t< T >& in ) noexcept {
    return sqrt( norm( in ) );
}

//==============================================================================================================================
template< typename T >
inline void normalize( quat_t< T >& out, const quat_t< T >& in, T eps = cEpsilon ) noexcept {
    T len { magnitude( in ) };
    if ( len > eps ) {
        len = 1 / len;
        scale( out, in, len );
    } else {
        out = { 0, 0, 0, 0 };
    }
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( quat_t< T >& out, const quat_t< T >& in1, const quat_t< T >& in2 ) noexcept {
    vec3_t< T > vec0 {     0,     0,     0 },
                vec1 { in1.x, in1.y, in1.z },
                vec2 { in2.x, in2.y, in2.z };

    const T cDot { dot( vec1, vec2 ) };
    cross( vec0, vec1, vec2 );
    scale( vec1, vec1, in2.w );
    scale( vec2, vec2, in1.w );
    add( vec0, vec0, vec1 );
    add( vec0, vec0, vec2 );

    out = { vec0.x, vec0.y, vec0.z, in1.w * in2.w - cDot };
}

//==============================================================================================================================
template< typename T >
constexpr void quatToMatT( mat3_t< T >& out, const quat_t< T >& in, T s ) noexcept {
    const T x2 { in.x * s }, y2 { in.y * s }, z2 { in.z * s };

    const T xx { in.x * x2 }, xy { in.x * y2 }, xz { in.x * z2 };
    const T yy { in.y * y2 }, yz { in.y * z2 }, zz { in.z * z2 };
    const T wx { in.w * x2 }, wy { in.w * y2 }, wz { in.w * z2 };

    out = { 1 - ( yy + zz ), xy + wz,         xz - wy,
            xy - wz,         1 - ( xx + zz ), yz + wx,
            xz + wy,         yz - wx,         1 - ( xx + yy ) };
}

//==============================================================================================================================
template< typename T >
constexpr void unitQuatToMat( mat3_t< T >& out, const quat_t< T >& in ) noexcept {
    quatToMatT( out, in, 2 );
}

//==============================================================================================================================
template< typename T >
constexpr void unitQuatToMat( mat4_t< T >& out, const quat_t< T >& in ) noexcept {
    mat3_t< T > mat;

    //identity( mat );
    quatToMatT( mat, in, 2 );

    out = { mat.r1.x, mat.r1.y, mat.r1.z, 0,
            mat.r2.x, mat.r2.y, mat.r2.z, 0,
            mat.r3.x, mat.r3.y, mat.r3.z, 0,
            0,        0,        0,        1 };
}

//==============================================================================================================================
template< typename T >
constexpr void quatToMat( mat3_t< T >& out, const quat_t< T >& in ) noexcept {
    quatToMatT( out, in, 2 / norm( in ) );
}

//==============================================================================================================================
template< typename T >
constexpr void quatToMat( mat4_t< T >& out, const quat_t< T >& in ) noexcept {
    mat3_t< T > mat;

    //identity( mat );
    quatToMatT( mat, in, 2 / norm( in ) );

    out = { mat.r1.x, mat.r1.y, mat.r1.z, 0,
            mat.r2.x, mat.r2.y, mat.r2.z, 0,
            mat.r3.x, mat.r3.y, mat.r3.z, 0,
            0,        0,        0,        1 };
}

//==============================================================================================================================
template< typename T, template< typename > typename mat_t >
constexpr void matToUnitQuatT( quat_t< T >& out, const mat_t< T >& in ) noexcept {
    const T tr { in.r1.x + in.r2.y + in.r3.z };

    if ( tr > 0 ) {
        out = { in.r2.z - in.r3.y, in.r3.x - in.r1.z, in.r1.y - in.r2.x, tr + 1 };
        scale( out, out, 0.5f / sqrt( out.w ) );
    } else if ( in.r1.x > in.r2.y && in.r1.x > in.r3.z ) {
        out = { 1 + in.r1.x - in.r2.y - in.r3.z, in.r2.x + in.r1.y, in.r3.x + in.r1.z, in.r2.z - in.r3.y };
        scale( out, out, 0.5f / sqrt( out.x ) );
    } else if ( in.r2.y > in.r3.z ) {
        out = { in.r2.x + in.r1.y, 1 + in.r2.y - in.r1.x - in.r3.z, in.r3.y + in.r2.z, in.r3.x - in.r1.z };
        scale( out, out, 0.5f / sqrt( out.y ) );
    } else {
        out = { in.r3.x + in.r1.z, in.r3.y + in.r2.z, 1 + in.r3.z - in.r1.x - in.r2.y, in.r1.y - in.r2.x };
        scale( out, out, 0.5f / sqrt( out.z ) );
    }
}

//==============================================================================================================================
template< typename T >
constexpr void matToUnitQuat( quat_t< T >& out, const mat3_t< T >& in ) noexcept {
    matToUnitQuatT( out, in );
}

//==============================================================================================================================
template< typename T >
constexpr void matToUnitQuat( quat_t< T >& out, const mat4_t< T >& in ) noexcept {
    matToUnitQuatT( out, in );
}

//==============================================================================================================================
template< typename T, template< typename > typename mat_t >
constexpr void matToQuatT( quat_t< T >& out, const mat_t< T >& in ) noexcept {
    const T tr { in.r1.x + in.r2.y + in.r3.z };

    if ( tr > 0 ) {
        out = { in.r2.z - in.r3.y, in.r3.x - in.r1.z, in.r1.y - in.r2.x, tr + 1 };
    } else if ( in.r1.x > in.r2.y && in.r1.x > in.r3.z ) {
        out = { 1 + in.r1.x - in.r2.y - in.r3.z, in.r2.x + in.r1.y, in.r3.x + in.r1.z, in.r2.z - in.r3.y };
    } else if ( in.r2.y > in.r3.z ) {
        out = { in.r2.x + in.r1.y, 1 + in.r2.y - in.r1.x - in.r3.z, in.r3.y + in.r2.z, in.r3.x - in.r1.z };
    } else {
        out = { in.r3.x + in.r1.z, in.r3.y + in.r2.z, 1 + in.r3.z - in.r1.x - in.r2.y, in.r1.y - in.r2.x };
    }
}

//==============================================================================================================================
template< typename T >
constexpr void matToQuat( quat_t< T >& out, const mat3_t< T >& in ) noexcept {
    matToQuatT( out, in );
}

//==============================================================================================================================
template< typename T >
constexpr void matToQuat( quat_t< T >& out, const mat4_t< T >& in ) noexcept {
    matToQuatT( out, in );
}

//==============================================================================================================================
// quat_t::GetDirVecFast
//
// theta = 2 * arccos( q.w )
// axis.x = q.x / sin( theta / 2 )
// axis.y = q.y / sin( theta / 2 )
// axis.z = q.z / sin( theta / 2 )
//==============================================================================================================================
/*
auto smd::quat_t::GetDirVecFast( Vector3f& vec ) const noexcept ->void {
    const float theta = 2.0f * acosf( w );
    const float fs = sinf( theta / 2.0f );
    vec.x = x / fs;
    vec.y = y / fs;
    vec.z = z / fs;
}
*/

//==============================================================================================================================
// quat_t::SetShortArc
//
// quat_t q;
// vector a = crossproduct(v1, v2)
// q.xyz = a;
// q.w = sqrt((v1.Length ^ 2) * (v2.Length ^ 2)) + dotproduct(v1, v2)
//==============================================================================================================================
/*
auto smd::quat_t::SetShortArc( const Vector3f& to, const Vector3f& from ) noexcept ->void {
    Vector3f locFrom = from;
    Vector3f locTo = to;

    //locFrom.Normalize();
    //locTo.Normalize();
    float vecS = sqrt( locFrom.x * locFrom.x + locFrom.y * locFrom.y + locFrom.z * locFrom.z );
    locFrom.x /= vecS;
    locFrom.y /= vecS;
    locFrom.z /= vecS;

    vecS = sqrt( locTo.x * locTo.x + locTo.y * locTo.y + locTo.z * locTo.z );
    locTo.x /= vecS;
    locTo.y /= vecS;
    locTo.z /= vecS;

    Vector3f a;
    cross( &a, &locFrom, &locTo );
    float d;
    dot( &d, &locFrom, &locTo );
    if ( d < 0.0f || d > 1.0f ) {
        Vector3f locFromInverse;
        negate( &locFromInverse, &locFrom );
        dot( &d, &locFromInverse, &locTo );
    }
    //float s = Math::SqrRoot( ( 1.0f + d ) * 2.0f );
    float s = sqrtf( ( 1.0f + d ) * 2.0f );

    x = a.x / s;
    y = a.y / s;
    z = a.z / s;
    w = s / 2.0f;

    //FIXME Нормализация только портит результат, видимо она неправильная...
    //Normalize();
}
*/

} // namespace smd::math
