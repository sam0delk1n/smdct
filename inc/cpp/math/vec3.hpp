﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include <cmath>
#include <cstdint>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct vec3_t final {
    T x, y, z;
};

using vec3f_t = vec3_t< float >;
using vec3d_t = vec3_t< double >;

static_assert( std::is_pod< vec3f_t >::value && sizeof( vec3f_t ) == 12, "" );
static_assert( std::is_pod< vec3d_t >::value && sizeof( vec3d_t ) == 24, "" );

//==============================================================================================================================
template< typename T >
constexpr void set( vec3_t< T >& out, T x, T y, T z ) noexcept {
    out = { x, y, z };
}

//==============================================================================================================================
template< typename T >
constexpr void negate( vec3_t< T >& out, const vec3_t< T >& cVec ) noexcept {
    out = { -cVec.x, -cVec.y, -cVec.z };
}

//==============================================================================================================================
template< typename T >
constexpr void add( vec3_t< T >& out, const vec3_t< T >& cVec1, const vec3_t< T >& cVec2 ) noexcept {
    out = { cVec1.x + cVec2.x, cVec1.y + cVec2.y, cVec1.z + cVec2.z };
}

//==============================================================================================================================
template< typename T >
constexpr void subtract( vec3_t< T >& out, const vec3_t< T >& cVec1, const vec3_t< T >& cVec2 ) noexcept {
    out = { cVec1.x - cVec2.x, cVec1.y - cVec2.y, cVec1.z - cVec2.z };
}

//==============================================================================================================================
template< typename T >
constexpr void scale( vec3_t< T >& out, const vec3_t< T >& cVec, T scale ) noexcept {
    out = { cVec.x * scale, cVec.y * scale, cVec.z * scale };
}

//==============================================================================================================================
template< typename T >
constexpr T dot( const vec3_t< T >& cVec1, const vec3_t< T >& cVec2 ) noexcept {
    return cVec1.x * cVec2.x + cVec1.y * cVec2.y + cVec1.z * cVec2.z;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline bool compare( const vec3_t< T >& cVec1, const vec3_t< T >& cVec2, T eps = cEpsilon ) noexcept {
    if ( fabs( cVec1.x - cVec2.x ) <= eps && fabs( cVec1.y - cVec2.y ) <= eps && fabs( cVec1.z - cVec2.z ) <= eps ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'sqrtf' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline T length( const vec3_t< T >& cVec ) noexcept {
    return sqrt( dot( cVec, cVec ) );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'length' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void normalize( vec3_t< T >& out, const vec3_t< T >& cVec, T eps = cEpsilon ) noexcept {
    T len { length( cVec ) };

    if ( len > eps ) {
        len = 1 / len;
        scale( out, cVec, len );
    } else {
        out = { 0, 0, 0 };
    }
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'length' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline T distance( const vec3_t< T >& cVec1, const vec3_t< T >& cVec2 ) noexcept {
    vec3_t< T > subtracted;
    subtract( subtracted, cVec1, cVec2 );
    return length( subtracted );
}

//==============================================================================================================================
template< typename T >
constexpr uint32_t toDword( const vec3_t< T >& cVec ) noexcept {
    uint32_t out { 0 };

    out |= ( static_cast< uint32_t >( cVec.x * 255 ) << 16 ) & 0x00ff0000;
    out |= ( static_cast< uint32_t >( cVec.y * 255 ) << 8  ) & 0x0000ff00;
    out |=   static_cast< uint32_t >( cVec.z * 255 )         & 0x000000ff;

    return out;
}

//==============================================================================================================================
template< typename T >
constexpr void fromDword( vec3_t< T >& out, uint32_t dword ) noexcept {
    out = { static_cast< uint8_t >( dword >> 16 ) / static_cast< T >( 255 ),
            static_cast< uint8_t >( dword >> 8  ) / static_cast< T >( 255 ),
            static_cast< uint8_t >( dword       ) / static_cast< T >( 255 ) };
}

//==============================================================================================================================
template< typename T >
constexpr void clamp( vec3_t< T >& out, const vec3_t< T >& cVec, T low, T high ) noexcept {
    out = cVec;

    if ( out.x < low ) {
        out.x = low;
    } else if ( out.x > high ) {
        out.x = high;
    }
    if ( out.y < low ) {
        out.y = low;
    } else if ( out.y > high ) {
        out.y = high;
    }
    if ( out.z < low ) {
        out.z = low;
    } else if ( out.z > high ) {
        out.z = high;
    }
}

//==============================================================================================================================
template< typename T >
constexpr void saturate( vec3_t< T >& out, const vec3_t< T >& cVec ) noexcept {
    clamp( out, cVec, 0.0f, 1.0f );
}

//==============================================================================================================================
template< typename T >
constexpr void cross( vec3_t< T >& out, const vec3_t< T >& cVec1, const vec3_t< T >& cVec2 ) noexcept {
    out = { cVec1.y * cVec2.z - cVec1.z * cVec2.y,
            cVec1.z * cVec2.x - cVec1.x * cVec2.z,
            cVec1.x * cVec2.y - cVec1.y * cVec2.x };
}

} // namespace smd::math
