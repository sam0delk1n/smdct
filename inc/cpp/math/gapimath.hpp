//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2017-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include "vec3.hpp"
#include "mat3.hpp"
#include "mat4.hpp"

#include <cmath>

namespace smd::math {

//==============================================================================================================================
// FIXME: Using 'inline' because 'normalize' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void lookAtLh( mat4_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    vec3_t< T > xAxis { 0, 0, 0 },
                yAxis { 0, 0, 0 },
                zAxis { 0, 0, 0 };

    subtract( zAxis, cAt, cEye );
    normalize( zAxis, zAxis );
    cross( xAxis, cUp, zAxis );
    normalize( xAxis, xAxis );
    cross( yAxis, zAxis, xAxis );

    out = { xAxis.x,             yAxis.x,             zAxis.x,             0,
            xAxis.y,             yAxis.y,             zAxis.y,             0,
            xAxis.z,             yAxis.z,             zAxis.z,             0,
            -dot( xAxis, cEye ), -dot( yAxis, cEye ), -dot( zAxis, cEye ), 1 };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'normalize' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void lookAtRh( mat4_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    vec3_t< T > xAxis { 0, 0, 0 },
                yAxis { 0, 0, 0 },
                zAxis { 0, 0, 0 };

    subtract( zAxis, cEye, cAt );
    normalize( zAxis, zAxis );
    cross( xAxis, cUp, zAxis );
    normalize( xAxis, xAxis );
    cross( yAxis, zAxis, xAxis );

    out = { xAxis.x,            yAxis.x,            zAxis.x,            0,
            xAxis.y,            yAxis.y,            zAxis.y,            0,
            xAxis.z,            yAxis.z,            zAxis.z,            0,
            dot( xAxis, cEye ), dot( yAxis, cEye ), dot( zAxis, cEye ), 1 };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'normalize' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void pointToT( mat3_t< T >& out, const vec3_t< T >& in1, const vec3_t< T >& in2, const vec3_t< T >& cUp ) noexcept {
    vec3_t< T > xAxis { 0, 0, 0 },
                yAxis { 0, 0, 0 },
                zAxis { 0, 0, 0 };

    subtract( zAxis, in2, in1 ); // Swap 'in1' and 'in2' for Left Hand and Right Hand basis.
    normalize( zAxis, zAxis );
    cross( xAxis, cUp, zAxis );
    normalize( xAxis, xAxis );
    cross( yAxis, zAxis, xAxis );

    out = { xAxis.x, yAxis.x, zAxis.x,
            xAxis.y, yAxis.y, zAxis.y,
            xAxis.z, yAxis.z, zAxis.z };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'pointToT' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void pointToLh( mat3_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    mat3_t< T > mat;

    //identity( mat );
    pointToT( mat, cEye, cAt, cUp );
    inverse( out, mat );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'pointToT' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void pointToLh( mat4_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    mat3_t< T > mat1;

    //identity( mat1 );
    pointToT( mat1, cEye, cAt, cUp );
    const mat4_t mat2 { mat1.r1.x, mat1.r1.y, mat1.r1.z, 0,
                        mat1.r2.x, mat1.r2.y, mat1.r2.z, 0,
                        mat1.r3.x, mat1.r3.y, mat1.r3.z, 0,
                                0,         0,         0, 1 };
    inverse( out, mat2 );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'pointToT' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void pointToRh( mat3_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    mat3_t< T > mat;

    //identity( mat );
    pointToT( mat, cAt, cEye, cUp );
    inverse( out, mat );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'pointToT' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void pointToRh( mat4_t< T >& out, const vec3_t< T >& cEye, const vec3_t< T >& cAt, const vec3_t< T >& cUp ) noexcept {
    mat3_t< T > mat1;

    //identity( mat1 );
    pointToT( mat1, cAt, cEye, cUp );
    const mat4_t mat2 { mat1.r1.x, mat1.r1.y, mat1.r1.z, 0,
                        mat1.r2.x, mat1.r2.y, mat1.r2.z, 0,
                        mat1.r3.x, mat1.r3.y, mat1.r3.z, 0,
                                0,         0,         0, 1 };
    inverse( out, mat2 );
}

//==============================================================================================================================
template< typename T >
constexpr void orthoOffCenterLh( mat4_t< T >& out, T left, T right, T bottom, T top, T near, T far ) noexcept {
    out = { 2 / ( right - left ),                0,                                   0,                     0,
            0,                                   2 / ( top - bottom ),                0,                     0,
            0,                                   0,                                   1 / ( far - near ),    0,
            ( left + right ) / ( left - right ), ( top + bottom ) / ( bottom - top ), near / ( near - far ), 1 };
}

//==============================================================================================================================
template< typename T >
constexpr void orthoOffCenterRh( mat4_t< T >& out, T left, T right, T bottom, T top, T near, T far ) noexcept {
    out = { 2 / ( right - left ),                0,                                   0,                     0,
            0,                                   2 / ( top - bottom ),                0,                     0,
            0,                                   0,                                   1 / ( near - far ),    0,
            ( left + right ) / ( left - right ), ( top + bottom ) / ( bottom - top ), near / ( near - far ), 1 };
}

//==============================================================================================================================
template< typename T >
inline void perspectiveFovLh( mat4_t< T >& out, T fov, T aspect, T near, T far ) noexcept {
    const T cScaleY { 1 / tan( fov / 2 ) };
    const T cScaleX { cScaleY / aspect };

    out = { cScaleX, 0,       0,                            0,
            0,       cScaleY, 0,                            0,
            0,       0,       far / ( far - near ),         1,
            0,       0,       -near * far / ( far - near ), 0 };
}

//==============================================================================================================================
template< typename T >
inline void perspectiveFovRh( mat4_t< T >& out, T fov, T aspect, T near, T far ) noexcept {
    const T cScaleY { 1 / tan( fov / 2 ) };
    const T cScaleX { cScaleY / aspect };

    out = { cScaleX, 0,       0,                           0,
            0,       cScaleY, 0,                           0,
            0,       0,       far / ( near - far ),       -1,
            0,       0,       near * far / ( near - far ), 0 };
}

} // namespace smd::math
