﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include <cmath>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct vec2_t final {
    T x, y;
};

using vec2f_t = vec2_t< float >;
using vec2d_t = vec2_t< double >;

static_assert( std::is_pod< vec2f_t >::value && sizeof( vec2f_t ) == 8, "" );
static_assert( std::is_pod< vec2d_t >::value && sizeof( vec2d_t ) == 16, "" );

//==============================================================================================================================
template< typename T >
constexpr void set( vec2_t< T >& out, T x, T y ) noexcept {
    out = { x, y };
}

//==============================================================================================================================
template< typename T >
constexpr void negate( vec2_t< T >& out, const vec2_t< T >& cVec ) noexcept {
    out = { -cVec.x, -cVec.y };
}

//==============================================================================================================================
template< typename T >
constexpr void add( vec2_t< T >& out, const vec2_t< T >& cVec1, const vec2_t< T >& cVec2 ) noexcept {
    out = { cVec1.x + cVec2.x, cVec1.y + cVec2.y };
}

//==============================================================================================================================
template< typename T >
constexpr void subtract( vec2_t< T >& out, const vec2_t< T >& cVec1, const vec2_t< T >& cVec2 ) noexcept {
    out = { cVec1.x - cVec2.x, cVec1.y - cVec2.y };
}

//==============================================================================================================================
template< typename T >
constexpr void scale( vec2_t< T >& out, const vec2_t< T >& cVec, T scale ) noexcept {
    out = { cVec.x * scale, cVec.y * scale };
}

//==============================================================================================================================
template< typename T >
constexpr T dot( const vec2_t< T >& cVec1, const vec2_t< T >& cVec2 ) noexcept {
    return cVec1.x * cVec2.x + cVec1.y * cVec2.y;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline bool compare( const vec2_t< T >& cVec1, const vec2_t< T >& cVec2, T eps = cEpsilon ) noexcept {
    if ( fabs( cVec1.x - cVec2.x ) <= eps && fabs( cVec1.y - cVec2.y ) <= eps ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'sqrtf' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline T length( const vec2_t< T >& cVec ) noexcept {
    return sqrt( dot( cVec, cVec ) );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'length' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline void normalize( vec2_t< T >& out, const vec2_t< T >& cVec, T eps = cEpsilon ) noexcept {
    T len { length( cVec ) };

    if ( len > eps ) {
        len = 1 / len;
        scale( out, cVec, len );
    } else {
        out = { 0, 0 };
    }
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'length' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline T distance( const vec2_t< T >& cVec1, const vec2_t< T >& cVec2 ) noexcept {
    vec2_t< T > subtracted;
    subtract( subtracted, cVec1, cVec2 );
    return length( subtracted );
}

//==============================================================================================================================
template< typename T >
constexpr void clamp( vec2_t< T >& out, const vec2_t< T >& cVec, T low, T high ) noexcept {
    out = cVec;

    if ( out.x < low ) {
        out.x = low;
    } else if ( out.x > high ) {
        out.x = high;
    }
    if ( out.y < low ) {
        out.y = low;
    } else if ( out.y > high ) {
        out.y = high;
    }
}

} // namespace smd::math
