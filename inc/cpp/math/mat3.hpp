﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

//==============================================================================================================================
//
// TODO list:
// * multiply mat3f_t on mat3f_t
// * project vector on matrix
// * unproject vector on matrix
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include "vec3.hpp"
#include "mat2.hpp"

#include <cmath>
#include <cstddef>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct mat3_t final {
    vec3_t< T > r1, r2, r3;
};

using mat3f_t = mat3_t< float >;
using mat3d_t = mat3_t< double >;

static_assert( std::is_pod< mat3f_t >::value && sizeof( mat3f_t ) == 36, "" );
static_assert( std::is_pod< mat3d_t >::value && sizeof( mat3d_t ) == 72, "" );

//==============================================================================================================================
template< typename T >
constexpr void identity( mat3_t< T >& out ) noexcept {
    out = { 1, 0, 0,
            0, 1, 0,
            0, 0, 1 };
}

//==============================================================================================================================
template< typename T >
constexpr void set( mat3_t< T >& out, T m11, T m12, T m13,
                                      T m21, T m22, T m23,
                                      T m31, T m32, T m33 ) noexcept {
    out = { m11, m12, m13,
            m21, m22, m23,
            m31, m32, m33 };
}

//==============================================================================================================================
template< typename T >
constexpr void transpose( mat3_t< T >& out, const mat3_t< T >& in ) noexcept {
    out = { in.r1.x, in.r2.x, in.r3.x,
            in.r1.y, in.r2.y, in.r3.y,
            in.r1.z, in.r2.z, in.r3.z };
}

//==============================================================================================================================
template< typename T >
inline mat3_t< T > mat3_rotX( T angle ) noexcept {
    const T cSin { sin( angle ) }, cCos { cos( angle ) };
    return { 1,    0,     0,
             0, cCos, -cSin,
             0, cSin,  cCos };
}

//==============================================================================================================================
template< typename T >
inline mat3_t< T > mat3_rotY( T angle ) noexcept {
    const T cSin { sin( angle ) }, cCos { cos( angle ) };
    return { cCos, 0, cSin,
                0, 1,    0,
            -cSin, 0, cCos };
}

//==============================================================================================================================
template< typename T >
inline mat3_t< T > mat3_rotZ( T angle ) noexcept {
    const T cSin { sin( angle ) }, cCos { cos( angle ) };
    return { cCos, -cSin, 0,
             cSin,  cCos, 0,
                0,     0, 1 };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'compare' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline bool compare( const mat3_t< T >& cMat1, const mat3_t< T >& cMat2, T eps = cEpsilon ) noexcept {
    if ( compare( cMat1.r1, cMat2.r1, eps ) && compare( cMat1.r2, cMat2.r2, eps ) && compare( cMat1.r3, cMat2.r3, eps ) ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
template< typename T >
constexpr void minorMatrix( mat2_t< T >& out, const mat3_t< T >& in, size_t row, size_t col ) noexcept {
    switch ( ( row << 4 ) + col ) {
    case 0x11:
        out = { in.r2.y, in.r2.z,
                in.r3.y, in.r3.z };
        break;
    case 0x12:
        out = { in.r2.x, in.r2.z,
                in.r3.x, in.r3.z };
        break;
    case 0x13:
        out = { in.r2.x, in.r2.y,
                in.r3.x, in.r3.y };
        break;
    case 0x21:
        out = { in.r1.y, in.r1.z,
                in.r3.y, in.r3.z };
        break;
    case 0x22:
        out = { in.r1.x, in.r1.z,
                in.r3.x, in.r3.z };
        break;
    case 0x23:
        out = { in.r1.x, in.r1.y,
                in.r3.x, in.r3.y };
        break;
    case 0x31:
        out = { in.r1.y, in.r1.z,
                in.r2.y, in.r2.z };
        break;
    case 0x32:
        out = { in.r1.x, in.r1.z,
                in.r2.x, in.r2.z };
        break;
    case 0x33:
        out = { in.r1.x, in.r1.y,
                in.r2.x, in.r2.y };
        break;
    default:
        out = { 1, 0,
                0, 1 };
    }
}

//==============================================================================================================================
template< typename T >
constexpr T determinant( const mat3_t< T >& cMat ) noexcept {
    const mat2_t cMat11 { cMat.r2.y, cMat.r2.z,
                          cMat.r3.y, cMat.r3.z },
                 cMat12 { cMat.r2.x, cMat.r2.z,
                          cMat.r3.x, cMat.r3.z },
                 cMat13 { cMat.r2.x, cMat.r2.y,
                          cMat.r3.x, cMat.r3.y };
    const T cDetMat11 { determinant( cMat11 ) }, cDetMat12 { determinant( cMat12 ) }, cDetMat13 { determinant( cMat13 ) };
    return cMat.r1.x * cDetMat11 - cMat.r1.y * cDetMat12 + cMat.r1.z * cDetMat13;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool inverse( mat3_t< T >& out, const mat3_t< T >& in, T eps = cInverseMatEpsilon ) noexcept {
    mat2_t< T > minor;
    mat3_t< T > transposedMat;

    //identity( minor );
    //identity( transposedMat );

    T detMatInverse { determinant( in ) };
    if ( fabs( detMatInverse ) <= eps ) {
        return false;
    }

    detMatInverse = 1 / detMatInverse;
    transpose( transposedMat, in );

    minorMatrix( minor, transposedMat, 1, 1 ); out.r1.x =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 1, 2 ); out.r1.y = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 1, 3 ); out.r1.z =  determinant( minor ) * detMatInverse;

    minorMatrix( minor, transposedMat, 2, 1 ); out.r2.x = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 2, 2 ); out.r2.y =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 2, 3 ); out.r2.z = -determinant( minor ) * detMatInverse;

    minorMatrix( minor, transposedMat, 3, 1 ); out.r3.x =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 3, 2 ); out.r3.y = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 3, 3 ); out.r3.z =  determinant( minor ) * detMatInverse;

    return true;
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( vec3_t< T >& out, const mat3_t< T >& cMat, const vec3_t< T >& cVec ) noexcept {
    out = { cMat.r1.x * cVec.x + cMat.r1.y * cVec.y + cMat.r1.z * cVec.z,
            cMat.r2.x * cVec.x + cMat.r2.y * cVec.y + cMat.r2.z * cVec.z,
            cMat.r3.x * cVec.x + cMat.r3.y * cVec.y + cMat.r3.z * cVec.z };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( vec3_t< T >& out, const vec3_t< T >& cVec, const mat3_t< T >& cMat ) noexcept {
    out = { cMat.r1.x * cVec.x + cMat.r2.x * cVec.y + cMat.r3.x * cVec.z,
            cMat.r1.y * cVec.x + cMat.r2.y * cVec.y + cMat.r3.y * cVec.z,
            cMat.r1.z * cVec.x + cMat.r2.z * cVec.y + cMat.r3.z * cVec.z };
}

//==============================================================================================================================
/*
auto Matrix3f::sInverse( Matrix3f& output, const Matrix3f& cMat ) noexcept ->bool {
    Matrix3f temp;
    float det, invDet;

    temp[ 0 ].x = cMat[ 1 ].y * cMat[ 2 ].z - cMat[ 1 ].z * cMat[ 2 ].y;
    temp[ 1 ].x = cMat[ 1 ].z * cMat[ 2 ].x - cMat[ 1 ].x * cMat[ 2 ].z;
    temp[ 2 ].x = cMat[ 1 ].x * cMat[ 2 ].y - cMat[ 1 ].y * cMat[ 2 ].x;

    det = cMat[ 0 ].x * temp[ 0 ].x + cMat[ 0 ].y * temp[ 1 ].x + cMat[ 0 ].z * temp[ 2 ].x;

    if ( fabsf( det ) < MATRIX_INVERSE_EPSILON ) return false;

    invDet = 1.0f / det;

    temp[ 0 ].y = cMat[ 0 ].z * cMat[ 2 ].y - cMat[ 0 ].y * cMat[ 2 ].z;
    temp[ 0 ].z = cMat[ 0 ].y * cMat[ 1 ].z - cMat[ 0 ].z * cMat[ 1 ].y;
    temp[ 1 ].y = cMat[ 0 ].x * cMat[ 2 ].z - cMat[ 0 ].z * cMat[ 2 ].x;
    temp[ 1 ].z = cMat[ 0 ].z * cMat[ 1 ].x - cMat[ 0 ].x * cMat[ 1 ].z;
    temp[ 2 ].y = cMat[ 0 ].y * cMat[ 2 ].x - cMat[ 0 ].x * cMat[ 2 ].y;
    temp[ 2 ].z = cMat[ 0 ].x * cMat[ 1 ].y - cMat[ 0 ].y * cMat[ 1 ].x;

    output[ 0 ].x = temp[ 0 ].x * invDet;
    output[ 0 ].y = temp[ 0 ].y * invDet;
    output[ 0 ].z = temp[ 0 ].z * invDet;

    output[ 1 ].x = temp[ 1 ].x * invDet;
    output[ 1 ].y = temp[ 1 ].y * invDet;
    output[ 1 ].z = temp[ 1 ].z * invDet;

    output[ 2 ].x = temp[ 2 ].x * invDet;
    output[ 2 ].y = temp[ 2 ].y * invDet;
    output[ 2 ].z = temp[ 2 ].z * invDet;

    return true;
}
*/

//==============================================================================================================================
/*
auto Matrix3f::RotationYawPitchRoll( float yaw, float pitch, float roll ) noexcept ->void {
    //TODO Р¤СѓРЅРєС†РёСЏ РЅРµ СЂР°Р±РѕС‚Р°РµС‚, РјРѕР¶РµС‚ СЃС‚РѕРёС‚ РїРѕРїСЂРѕР±РѕРІР°С‚СЊ РµРµ РїРµСЂРµРїРёСЃР°С‚СЊ РІРѕС‚ С‚Р°Рє:
    // matRes = matRotX * matRotY * matRotZ;
    const float sinYaw { sinf( yaw ) }, sinPitch { sinf( pitch ) }, sinRoll { sinf( roll ) };
    const float cosYaw { cosf( yaw ) }, cosPitch { cosf( pitch ) }, cosRoll { cosf( roll ) };
    mat[ 0 ].x = cosYaw * cosRoll - sinYaw * cosPitch * sinRoll; mat[ 0 ].y = -cosYaw * sinRoll - sinYaw * cosPitch * cosRoll; mat[ 0 ].z = sinYaw * sinPitch;
    mat[ 1 ].x = sinYaw * cosRoll + cosYaw * cosPitch * sinRoll; mat[ 1 ].y = -sinYaw * sinRoll + cosYaw * cosPitch * cosRoll; mat[ 1 ].z = -cosYaw * sinPitch;
    mat[ 2 ].x = sinPitch * sinRoll;                             mat[ 2 ].y = sinPitch * cosRoll;                              mat[ 2 ].z = cosPitch;
}
*/

} // namespace smd::math
