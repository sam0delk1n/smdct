﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include <cmath>
#include <cstdint>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct vec4_t final {
    T x, y, z, w;
};

using vec4f_t = vec4_t< float >;
using vec4d_t = vec4_t< double >;

static_assert( std::is_pod< vec4f_t >::value && sizeof( vec4f_t ) == 16, "" );
static_assert( std::is_pod< vec4d_t >::value && sizeof( vec4d_t ) == 32, "" );

//==============================================================================================================================
template< typename T >
constexpr void set( vec4_t< T >& out, T x, T y, T z, T w ) noexcept {
    out = { x, y, z, w };
}

//==============================================================================================================================
template< typename T >
constexpr void negate( vec4_t< T >& out, const vec4_t< T >& cVec ) noexcept {
    out = { -cVec.x, -cVec.y, -cVec.z, -cVec.w };
}

//==============================================================================================================================
template< typename T >
constexpr void add( vec4_t< T >& out, const vec4_t< T >& cVec1, const vec4_t< T >& cVec2 ) noexcept {
    out = { cVec1.x + cVec2.x, cVec1.y + cVec2.y, cVec1.z + cVec2.z, cVec1.w + cVec2.w };
}

//==============================================================================================================================
template< typename T >
constexpr void subtract( vec4_t< T >& out, const vec4_t< T >& cVec1, const vec4_t< T >& cVec2 ) noexcept {
    out = { cVec1.x - cVec2.x, cVec1.y - cVec2.y, cVec1.z - cVec2.z, cVec1.w - cVec2.w };
}

//==============================================================================================================================
template< typename T >
constexpr void scale( vec4_t< T >& out, const vec4_t< T >& cVec, T scale ) noexcept {
    out = { cVec.x * scale, cVec.y * scale, cVec.z * scale, cVec.w * scale };
}

//==============================================================================================================================
template< typename T >
constexpr float dot( const vec4_t< T >& cVec1, const vec4_t< T >& cVec2 ) noexcept {
    return cVec1.x * cVec2.x + cVec1.y * cVec2.y + cVec1.z * cVec2.z + cVec1.w * cVec2.w;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool compare( const vec4_t< T >& cVec1, const vec4_t< T >& cVec2, T eps = cEpsilon ) noexcept {
    if ( fabs( cVec1.x - cVec2.x ) <= eps && fabs( cVec1.y - cVec2.y ) <= eps &&
         fabs( cVec1.z - cVec2.z ) <= eps && fabs( cVec1.w - cVec2.w ) <= eps ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
template< typename T >
inline T length( const vec4_t< T >& cVec ) noexcept {
    return sqrt( dot( cVec, cVec ) );
}

//==============================================================================================================================
template< typename T >
inline void normalize( vec4_t< T >& out, const vec4_t< T >& cVec, T eps = cEpsilon ) noexcept {
    T len { length( cVec ) };

    if ( len > eps ) {
        len = 1 / len;
        scale( out, cVec, len );
    } else {
        out = { 0, 0, 0, 0 };
    }
}

//==============================================================================================================================
template< typename T >
constexpr uint32_t toDword( const vec4_t< T >& cVec ) noexcept {
    uint32_t out { 0 };

    out |= ( static_cast< uint32_t >( cVec.x * 255 ) << 24 ) & 0xff000000;
    out |= ( static_cast< uint32_t >( cVec.y * 255 ) << 16 ) & 0x00ff0000;
    out |= ( static_cast< uint32_t >( cVec.z * 255 ) << 8  ) & 0x0000ff00;
    out |=   static_cast< uint32_t >( cVec.w * 255 )         & 0x000000ff;

    return out;
}

//==============================================================================================================================
template< typename T >
constexpr void fromDword( vec4_t< T >& out, uint32_t dword ) noexcept {
    out = { static_cast< uint8_t >( dword >> 24 ) / static_cast< T >( 255 ),
            static_cast< uint8_t >( dword >> 16 ) / static_cast< T >( 255 ),
            static_cast< uint8_t >( dword >> 8  ) / static_cast< T >( 255 ),
            static_cast< uint8_t >( dword       ) / static_cast< T >( 255 ) };
}

//==============================================================================================================================
template< typename T >
constexpr void clamp( vec4_t< T >& out, const vec4_t< T >& cVec, T low, T high ) noexcept {
    out = cVec;

    if ( out.x < low ) {
        out.x = low;
    } else if ( out.x > high ) {
        out.x = high;
    }
    if ( out.y < low ) {
        out.y = low;
    } else if ( out.y > high ) {
        out.y = high;
    }
    if ( out.z < low ) {
        out.z = low;
    } else if ( out.z > high ) {
        out.z = high;
    }
    if ( out.w < low ) {
        out.w = low;
    } else if ( out.w > high ) {
        out.w = high;
    }
}

//==============================================================================================================================
template< typename T >
constexpr void saturate( vec4_t< T >& out, const vec4_t< T >& cVec ) noexcept {
    clamp( out, cVec, 0.0f, 1.0f );
}

} // namespace smd::math
