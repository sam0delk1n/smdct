﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include "vec2.hpp"

#include <cmath>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct mat2_t final {
    vec2_t< T > r1, r2;
};

using mat2f_t = mat2_t< float >;
using mat2d_t = mat2_t< double >;

static_assert( std::is_pod< mat2f_t >::value && sizeof( mat2f_t ) == 16, "" );
static_assert( std::is_pod< mat2d_t >::value && sizeof( mat2d_t ) == 32, "" );

//==============================================================================================================================
template< typename T >
constexpr void identity( mat2_t< T >& out ) noexcept {
    out = { 1, 0,
            0, 1 };
}

//==============================================================================================================================
template< typename T >
constexpr void set( mat2_t< T >& out, T m11, T m12,
                                      T m21, T m22 ) noexcept {
    out = { m11, m12,
            m21, m22 };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'compare' is not declared as 'constexpr' in MSVS 2017.
template< typename T >
inline bool compare( const mat2_t< T >& cMat1, const mat2_t< T >& cMat2, T eps = cEpsilon ) noexcept {
    if ( compare( cMat1.r1, cMat2.r1, eps ) && compare( cMat1.r2, cMat2.r2, eps ) ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
template< typename T >
constexpr T determinant( const mat2_t< T >& cMat ) noexcept {
    return cMat.r1.x * cMat.r2.y - cMat.r2.x * cMat.r1.y;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool inverse( mat2_t< T >& out, const mat2_t< T >& in, T eps = cInverseMatEpsilon ) noexcept {
    const T cDet { determinant( in ) };
    if ( fabs( cDet ) <= eps ) {
        return false;
    }
    out = { in.r2.y / cDet, -in.r1.y / cDet,
           -in.r2.x / cDet,  in.r1.x / cDet };
    return true;
}

//==============================================================================================================================
template< typename T >
constexpr void transpose( mat2_t< T >& out, const mat2_t< T >& in ) noexcept {
    out = { in.r1.x, in.r2.x,
            in.r1.y, in.r2.y };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( vec2_t< T >& out, const vec2_t< T >& cVec, const mat2_t< T >& cMat ) noexcept {
    out = { cVec.x * cMat.r1.x + cVec.y * cMat.r2.x, cVec.x * cMat.r1.y + cVec.y * cMat.r2.y };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( mat2_t< T >& out, const mat2_t< T >& cMat1, const mat2_t< T >& cMat2 ) noexcept {
    out = { cMat1.r1.x * cMat2.r1.x + cMat1.r1.y * cMat2.r2.x, cMat1.r1.x * cMat2.r1.y + cMat1.r1.y * cMat2.r2.y,
            cMat1.r2.x * cMat2.r1.x + cMat1.r2.y * cMat2.r2.x, cMat1.r2.x * cMat2.r1.y + cMat1.r2.y * cMat2.r2.y };
}

//==============================================================================================================================
template< typename T >
inline void rotation( mat2_t< T >& out, T angle ) {
    const T cSin { sin( angle ) };
    const T cCos { cos( angle ) };
    out = { cCos, cSin,
           -cSin, cCos };
}

} // namespace smd::math
