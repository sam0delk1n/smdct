﻿//==============================================================================================================================
//
// This file is part of the sam0delk1n's Conventional Tools (smdCT).
//
// Copyright © 2015-2018 sam0delk1n <skipper05ster@gmail.com>.
// All rights reserved.
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.
//
//==============================================================================================================================

#pragma once

#include "common.hpp"
#include "vec4.hpp"
#include "mat3.hpp"

#include <cmath>
#include <cstddef>
#include <type_traits>

namespace smd::math {

//==============================================================================================================================
template< typename T >
struct mat4_t final {
    vec4_t< T > r1, r2, r3, r4;
};

using mat4f_t = mat4_t< float >;
using mat4d_t = mat4_t< double >;

static_assert( std::is_pod< mat4f_t >::value && sizeof( mat4f_t ) == 64, "" );
static_assert( std::is_pod< mat4d_t >::value && sizeof( mat4d_t ) == 128, "" );

//==============================================================================================================================
template< typename T >
constexpr void identity( mat4_t< T >& out ) noexcept {
    out = { 1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1 };
}

//==============================================================================================================================
template< typename T >
constexpr void set( mat4_t< T >& out, T m11, T m12, T m13, T m14,
                                      T m21, T m22, T m23, T m24,
                                      T m31, T m32, T m33, T m34,
                                      T m41, T m42, T m43, T m44 ) noexcept {
    out = { m11, m12, m13, m14,
            m21, m22, m23, m24,
            m31, m32, m33, m34,
            m41, m42, m43, m44 };
}

//==============================================================================================================================
template< typename T >
constexpr void transpose( mat4_t< T >& out, const mat4_t< T >& in ) noexcept {
    out = { in.r1.x, in.r2.x, in.r3.x, in.r4.x,
            in.r1.y, in.r2.y, in.r3.y, in.r4.y,
            in.r1.z, in.r2.z, in.r3.z, in.r4.z,
            in.r1.w, in.r2.w, in.r3.w, in.r4.w };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( mat4_t< T >& out, const mat4_t< T >& cMat, T val ) noexcept {
    scale( out.r1, cMat.r1, val );
    scale( out.r2, cMat.r2, val );
    scale( out.r3, cMat.r3, val );
    scale( out.r4, cMat.r4, val );
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( vec4_t< T >& out, const vec4_t< T >& cVec, const mat4_t< T >& cMat ) noexcept {
    mat4_t< T > tp;

    //identity( tp );
    transpose( tp, cMat );

    out = { dot( cVec, tp.r1 ),
            dot( cVec, tp.r2 ),
            dot( cVec, tp.r3 ),
            dot( cVec, tp.r4 ) };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( vec4_t< T >& out, const mat4_t< T >& cMat, const vec4_t< T >& cVec ) noexcept {
    out = { dot( cMat.r1, cVec ),
            dot( cMat.r2, cVec ),
            dot( cMat.r3, cVec ),
            dot( cMat.r4, cVec ) };
}

//==============================================================================================================================
template< typename T >
constexpr void multiply( mat4_t< T >& out, const mat4_t< T >& in1, const mat4_t< T >& in2 ) noexcept {
    mat4_t< T > tp;

    //identity( tp );
    transpose( tp, in2 );

    out = { { dot( in1.r1, tp.r1 ),
              dot( in1.r1, tp.r2 ),
              dot( in1.r1, tp.r3 ),
              dot( in1.r1, tp.r4 ) },
            { dot( in1.r2, tp.r1 ),
              dot( in1.r2, tp.r2 ),
              dot( in1.r2, tp.r3 ),
              dot( in1.r2, tp.r4 ) },
            { dot( in1.r3, tp.r1 ),
              dot( in1.r3, tp.r2 ),
              dot( in1.r3, tp.r3 ),
              dot( in1.r3, tp.r4 ) },
            { dot( in1.r4, tp.r1 ),
              dot( in1.r4, tp.r2 ),
              dot( in1.r4, tp.r3 ),
              dot( in1.r4, tp.r4 ) } };
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'compare' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool compare( const mat4_t< T >& cMat1, const mat4_t< T >& cMat2, T eps = cEpsilon ) noexcept {
    if ( compare( cMat1.r1, cMat2.r1, eps ) && compare( cMat1.r2, cMat2.r2, eps ) &&
         compare( cMat1.r3, cMat2.r3, eps ) && compare( cMat1.r4, cMat2.r4, eps ) ) {
        return true;
    }
    return false;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'compare' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool operator==( const mat4_t< T >& cMat1, const mat4_t< T >& cMat2 ) noexcept {
    return compare( cMat1, cMat2 );
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'compare' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool operator!=( const mat4_t< T >& cMat1, const mat4_t< T >& cMat2 ) noexcept {
    return !compare( cMat1, cMat2 );
}

//==============================================================================================================================
template< typename T >
constexpr void translate( mat4_t< T >& out, const vec3_t< T >& cVec ) noexcept {
    out = { 1,      0,      0,      0,
            0,      1,      0,      0,
            0,      0,      1,      0,
            cVec.x, cVec.y, cVec.z, 1 };
}

//==============================================================================================================================
template< typename T >
constexpr void scale( mat4_t< T >& out, const vec3_t< T >& cVec ) noexcept {
    out = { cVec.x, 0,      0,      0,
            0,      cVec.y, 0,      0,
            0,      0,      cVec.z, 0,
            0,      0,      0,      1 };
}

//==============================================================================================================================
template< typename T >
constexpr void minorMatrix( mat3_t< T >& out, const mat4_t< T >& in, size_t row, size_t col ) noexcept {
    switch ( ( row << 4 ) + col ) {
    case 0x11:
        out = { in.r2.y, in.r2.z, in.r2.w,
                in.r3.y, in.r3.z, in.r3.w,
                in.r4.y, in.r4.z, in.r4.w };
        break;
    case 0x12:
        out = { in.r2.x, in.r2.z, in.r2.w,
                in.r3.x, in.r3.z, in.r3.w,
                in.r4.x, in.r4.z, in.r4.w };
        break;
    case 0x13:
        out = { in.r2.x, in.r2.y, in.r2.w,
                in.r3.x, in.r3.y, in.r3.w,
                in.r4.x, in.r4.y, in.r4.w };
        break;
    case 0x14:
        out = { in.r2.x, in.r2.y, in.r2.z,
                in.r3.x, in.r3.y, in.r3.z,
                in.r4.x, in.r4.y, in.r4.z };
        break;
    case 0x21:
        out = { in.r1.y, in.r1.z, in.r1.w,
                in.r3.y, in.r3.z, in.r3.w,
                in.r4.y, in.r4.z, in.r4.w };
        break;
    case 0x22:
        out = { in.r1.x, in.r1.z, in.r1.w,
                in.r3.x, in.r3.z, in.r3.w,
                in.r4.x, in.r4.z, in.r4.w };
        break;
    case 0x23:
        out = { in.r1.x, in.r1.y, in.r1.w,
                in.r3.x, in.r3.y, in.r3.w,
                in.r4.x, in.r4.y, in.r4.w };
        break;
    case 0x24:
        out = { in.r1.x, in.r1.y, in.r1.z,
                in.r3.x, in.r3.y, in.r3.z,
                in.r4.x, in.r4.y, in.r4.z };
        break;
    case 0x31:
        out = { in.r1.y, in.r1.z, in.r1.w,
                in.r2.y, in.r2.z, in.r2.w,
                in.r4.y, in.r4.z, in.r4.w };
        break;
    case 0x32:
        out = { in.r1.x, in.r1.z, in.r1.w,
                in.r2.x, in.r2.z, in.r2.w,
                in.r4.x, in.r4.z, in.r4.w };
        break;
    case 0x33:
        out = { in.r1.x, in.r1.y, in.r1.w,
                in.r2.x, in.r2.y, in.r2.w,
                in.r4.x, in.r4.y, in.r4.w };
        break;
    case 0x34:
        out = { in.r1.x, in.r1.y, in.r1.z,
                in.r2.x, in.r2.y, in.r2.z,
                in.r4.x, in.r4.y, in.r4.z };
        break;
    case 0x41:
        out = { in.r1.y, in.r1.z, in.r1.w,
                in.r2.y, in.r2.z, in.r2.w,
                in.r3.y, in.r3.z, in.r3.w };
        break;
    case 0x42:
        out = { in.r1.x, in.r1.z, in.r1.w,
                in.r2.x, in.r2.z, in.r2.w,
                in.r3.x, in.r3.z, in.r3.w };
        break;
    case 0x43:
        out = { in.r1.x, in.r1.y, in.r1.w,
                in.r2.x, in.r2.y, in.r2.w,
                in.r3.x, in.r3.y, in.r3.w };
        break;
    case 0x44:
        out = { in.r1.x, in.r1.y, in.r1.z,
                in.r2.x, in.r2.y, in.r2.z,
                in.r3.x, in.r3.y, in.r3.z };
    default:
        out = { 1, 0, 0,
                0, 1, 0,
                0, 0, 1 };
    }
}

//==============================================================================================================================
template< typename T >
constexpr T determinant( const mat4_t< T >& cMat ) noexcept {
    mat3_t< T > minor;

    //identity( minor );

    minorMatrix( minor, cMat, 1, 1 );
    const T cDetMinor11 { determinant( minor ) };

    minorMatrix( minor, cMat, 1, 2 );
    const T cDetMinor12 { determinant( minor ) };

    minorMatrix( minor, cMat, 1, 3 );
    const T cDetMinor13 { determinant( minor ) };

    minorMatrix( minor, cMat, 1, 4 );
    const T cDetMinor14 { determinant( minor ) };

    return cMat.r1.x * cDetMinor11 - cMat.r1.y * cDetMinor12 + cMat.r1.z * cDetMinor13 - cMat.r1.w * cDetMinor14;
}

//==============================================================================================================================
// FIXME: Using 'inline' because 'fabsf' is not declared as 'constexpr' in MinGW-Release.
template< typename T >
inline bool inverse( mat4_t< T >& out, const mat4_t< T >& in, T eps = cInverseMatEpsilon ) noexcept {
    mat3_t< T > minor;
    mat4_t< T > transposedMat;

    //identity( minor );
    //identity( transposedMat );

    T detMatInverse { determinant( in ) };
    if ( fabs( detMatInverse ) <= eps ) {
        return false;
    }

    detMatInverse = 1 / detMatInverse;
    transpose( transposedMat, in );

    minorMatrix( minor, transposedMat, 1, 1 ); out.r1.x =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 1, 2 ); out.r1.y = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 1, 3 ); out.r1.z =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 1, 4 ); out.r1.w = -determinant( minor ) * detMatInverse;

    minorMatrix( minor, transposedMat, 2, 1 ); out.r2.x = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 2, 2 ); out.r2.y =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 2, 3 ); out.r2.z = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 2, 4 ); out.r2.w =  determinant( minor ) * detMatInverse;

    minorMatrix( minor, transposedMat, 3, 1 ); out.r3.x =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 3, 2 ); out.r3.y = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 3, 3 ); out.r3.z =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 3, 4 ); out.r3.w = -determinant( minor ) * detMatInverse;

    minorMatrix( minor, transposedMat, 4, 1 ); out.r4.x = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 4, 2 ); out.r4.y =  determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 4, 3 ); out.r4.z = -determinant( minor ) * detMatInverse;
    minorMatrix( minor, transposedMat, 4, 4 ); out.r4.w =  determinant( minor ) * detMatInverse;

    return true;
}

/*
//==============================================================================================================================
void inverseAlt( mat4f_t* const pcOut, const mat4f_t* const cpcMat ) noexcept {
    mat4f_t out;
    //const float* a = mat.f;
    //float* r = out.f;
    out.m11 =  cpcMat->m22 * cpcMat->m33 * cpcMat->m44 - a[5]*a[14]*a[11] - a[6]*a[9]*a[15] + a[6]*a[13]*a[11] + a[7]*a[9]*a[14] - a[7]*a[13]*a[10];
    out.m12 = -cpcMat->m12 * cpcMat->m33 * cpcMat->m44 + a[1]*a[14]*a[11] + a[2]*a[9]*a[15] - a[2]*a[13]*a[11] - a[3]*a[9]*a[14] + a[3]*a[13]*a[10];
    out.m13 =  cpcMat->m12 * cpcMat->m23 * cpcMat->m44 - a[1]*a[14]*a[7]  - a[2]*a[5]*a[15] + a[2]*a[13]*a[7]  + a[3]*a[5]*a[14] - a[3]*a[13]*a[6];
    out.m14 = -cpcMat->m12 * cpcMat->m23 * cpcMat->m34 + a[1]*a[10]*a[7]  + a[2]*a[5]*a[11] - a[2]*a[9]*a[7]   - a[3]*a[5]*a[10] + a[3]*a[9]*a[6];

    out.m21 = -a[4]*a[10]*a[15] + a[4]*a[14]*a[11] + a[6]*a[8]*a[15] - a[6]*a[12]*a[11] - a[7]*a[8]*a[14] + a[7]*a[12]*a[10];
    out.m22 =  a[0]*a[10]*a[15] - a[0]*a[14]*a[11] - a[2]*a[8]*a[15] + a[2]*a[12]*a[11] + a[3]*a[8]*a[14] - a[3]*a[12]*a[10];
    out.m23 = -a[0]*a[6]*a[15]  + a[0]*a[14]*a[7]  + a[2]*a[4]*a[15] - a[2]*a[12]*a[7]  - a[3]*a[4]*a[14] + a[3]*a[12]*a[6];
    out.m24 =  a[0]*a[6]*a[11]  - a[0]*a[10]*a[7]  - a[2]*a[4]*a[11] + a[2]*a[8]*a[7]   + a[3]*a[4]*a[10] - a[3]*a[8]*a[6];

    out.m31 =  a[4]*a[9]*a[15]  - a[4]*a[13]*a[11] - a[5]*a[8]*a[15] + a[5]*a[12]*a[11] + a[7]*a[8]*a[13] - a[7]*a[12]*a[9];
    out.m32 = -a[0]*a[9]*a[15]  + a[0]*a[13]*a[11] + a[1]*a[8]*a[15] - a[1]*a[12]*a[11] - a[3]*a[8]*a[13] + a[3]*a[12]*a[9];
    out.m33 =  a[0]*a[5]*a[15]  - a[0]*a[13]*a[7]  - a[1]*a[4]*a[15] + a[1]*a[12]*a[7]  + a[3]*a[4]*a[13] - a[3]*a[12]*a[5];
    out.m34 = -a[0]*a[5]*a[11]  + a[0]*a[9]*a[7]   + a[1]*a[4]*a[11] - a[1]*a[8]*a[7]   - a[3]*a[4]*a[9]  + a[3]*a[8]*a[5];

    out.m41 = -a[4]*a[9]*a[14]  + a[4]*a[13]*a[10] + a[5]*a[8]*a[14] - a[5]*a[12]*a[10] - a[6]*a[8]*a[13] + a[6]*a[12]*a[9];
    out.m42 =  a[0]*a[9]*a[14]  - a[0]*a[13]*a[10] - a[1]*a[8]*a[14] + a[1]*a[12]*a[10] + a[2]*a[8]*a[13] - a[2]*a[12]*a[9];
    out.m43 = -a[0]*a[5]*a[14]  + a[0]*a[13]*a[6]  + a[1]*a[4]*a[14] - a[1]*a[12]*a[6]  - a[2]*a[4]*a[13] + a[2]*a[12]*a[5];
    out.m44 =  a[0]*a[5]*a[10]  - a[0]*a[9]*a[6]   - a[1]*a[4]*a[10] + a[1]*a[8]*a[6]   + a[2]*a[4]*a[9]  - a[2]*a[8]*a[5];

    float det = a[0]*r[0] + a[4]*r[1] + a[8]*r[2] + a[12]*r[3];
    if( fabsf( det ) > 0.00001f ) {
        det = 1.0f / det;
    }
    for( int i = 0; i < 16; i++ ) {
        r[i] *= det;
    }
    return out;
}
*/

} // namespace smd::math
